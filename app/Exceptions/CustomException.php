<?php

namespace App\Exceptions;

use Exception;

class CustomException extends Exception
{
    public function render() {
        return response()->view('errors.' . '500', ['message' => 'El error es...'], 500);
    }


}
