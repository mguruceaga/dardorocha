<?php

/**
 * Se utiliza para devolver encriptado los datos recibidos si esta en true la variable de entorno APP_ENCRYPT
 *
 * @param $dato
 * @return string
 */
function (new \App\Tools\HelpersTool)->helper_getUrlHash($datos)
{
    if (config('milegajo.config.encriptacion.app_encrypt')) {
        $datos = \Illuminate\Support\Facades\Crypt::encryptString($datos);
    }
    return $datos;
}

/**
 * Se utiliza para Desncriptar los datos enviados desde los formularios, generalmente por Método GET
 * Recibe los datos y un valor de tipo que indica si es Simple o Compuesto
 * Simple es un dato encriptado
 * Compuesto es una secuencia del tipo clave=valor separados por & ('caja_id=1&caja_instancia_id=8' por ejemplo)
 *
 * @param $datos
 * @param string $tipo
 * @return array|mixed
 */
function helper_desencriptar($datos, $tipo = ENCRYPT_COMPUESTA) {
    /**
     * Si está actica la encriptación por la variable de entorno (APP_ENCRYPT) desencripto los datos recibidos
     */
    if (config('milegajo.config.encriptacion.app_encrypt')) {
        $datos = \Illuminate\Support\Facades\Crypt::decryptString($datos);
    }

    /**
     * Puede ser que la encriptación sea un dato simple y directo, o uno compuesto
     */
    switch ($tipo) {

        /**
         * Cuando el tipo de encriptación es simple se devuelve el dato directamente
         */
        case ENCRYPT_SIMPLE:

            return $datos;
            break;

        /**
         * Parseo los datos recibidos
         * Los datos vienen separados por & y clave=valor
         */
        case ENCRYPT_COMPUESTA:

            $datos = explode('&', $datos);
            $data = array();
            foreach($datos as $dato) {
                $dato = explode('=', $dato);
                $data[$dato[0]] = $dato[1];
            }
            return $data;
            break;

    }

}



