<?php

namespace App\Providers;

use App\Tools\ValidationTool;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Builder::macro('search', function ($field, $string) {
            return $string ? $this->where($field, 'like', '%'.$string.'%') : $this;
        });

        Validator::extend('dni_in_cuil_cuit', function ($attribute, $value, $parameters, $validator) {
            info($parameters);
            return (new ValidationTool)->dniIsInCuit($value, $parameters[0]);
        });

//        Validator::extend('dni_in_cuil_cuit', function ($attribute, $value, $parameters, $validator) {
//            return (new ValidationTool)->dniIsInCuit($value, $parameters[1]);
//        });

        Validator::extend('cbu_valid', function ($attribute, $value, $parameters, $validator) {
            return (new ValidationTool)->cbuIsValid($value);
        });

        Validator::extend('genero_cuit', function ($attribute, $value, $parameters, $validator) {
            return (new ValidationTool)->generoCuit($value, $parameters[0]);
        });

        Validator::extend('cuit_valid', function ($attribute, $value, $parameters, $validator) {
            return (new ValidationTool)->cuitIsValid($value);
        });
    }
}
