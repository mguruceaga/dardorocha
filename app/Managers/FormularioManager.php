<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Managers;

use App\Models\Banco;
use App\Models\Reparticion;
use App\Models\Formulario;

/**
 * Description of FormularioManager
 *
 * @author Gab
 */
class FormularioManager extends BaseManager {

    public function __construct($entity = null) {
        $this->entityClassName = '\App\Models\Formulario';
        parent::__construct($entity);
    }

    public function getItem($formularioId) {
        $formulario = Formulario::find($formularioId);
        return $formulario;
    }

    public function getItems($orderBy) {
        return Formulario::orderBy($orderBy)->get();
    }

    public function saveItem(Formulario $formulario) {
        $formulario->save();
    }

    public function delete($formularioId) {
        $formulario = $this->getItem($formularioId);
        $formulario->delete();
    }

}
