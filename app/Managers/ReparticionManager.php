<?php

namespace App\Managers;

use App\Models\Reparticion;

class ReparticionManager extends BaseManager {

    public function __construct($entity = null) {
        $this->entityClassName = '\App\Models\Reparticion';
        parent::__construct($entity);
    }
    
    public function getItem($reparticionId) {
        $reparticion = Reparticion::find($reparticionId);
        return $reparticion;
    }
    
    public function getItems($orderBy) {
        return Reparticion::orderBy($orderBy)->get();
    }
    
    public function saveItem(Reparticion $reparticion){
        $reparticion->save();
    }

}
