<?php

namespace App\Managers;

use Illuminate\Support\MessageBag;

class ExceptionManager extends BaseManager {

    /**
     * Se utiliza para devolver el error en una elemento MessageBag de acuerdo al error
     *
     * @param \Exception $e
     * @return MessageBag
     */
    public function messageBagError(\Exception $e) {

        /**
         * Creo una instancia de MessageBag para devolver los errores asociados en el arreglo errors de Laravel
         */
        $messageBag = new MessageBag();

        switch($e->getCode()) {
            case 1000:
                /**
                 * Las exepciones con código 1000 son disparadas por la programación
                 */
                $messageBag->add('debug_errors', $e->getMessage());
                break;
            case 1001:
                /**
                 * Las exepciones con código 1001 son errores de acceso por permisos
                 */
                $messageBag->add('permission_errors', $e->getMessage());
                break;
            case 1099:
                /**
                 * Las exepciones con código 1001 son errores de acceso por permisos
                 */
                $messageBag->add('errors', $e->getMessage());
                break;
            default:
                $messageBag->add('debug_errors', $e->getMessage());
                break;
        }

        return $messageBag;

    }

}
