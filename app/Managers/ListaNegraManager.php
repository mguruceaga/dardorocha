<?php

namespace App\Managers;

use App\Models\ListaNegra;
use App\Tools\FormatsTool;

class ListaNegraManager extends BaseManager {

    public function __construct($entity = null) {
        $this->entityClassName = '\App\Models\ListaNegra';
        parent::__construct($entity);
    }

    public function getItem($listaNegraId) {
        $listaNegra = ListaNegra::find($listaNegraId);
        //$listaNegra->fecha = Carbon::createFromDate($listaNegra->fecha)->format('d-m-Y');
        return $listaNegra;
    }

    public function getItems($orderBy) {
        return ListaNegra::orderBy($orderBy)->get();
    }

    public function saveItem(ListaNegra $listaNegra){
        $listaNegra->fecha = (new FormatsTool)->prepareToSave('date', $listaNegra->fecha);
        $listaNegra->save();
    }

    public function delete($listaNegraId){
        $listaNegra = $this->getItem($listaNegraId);
        $listaNegra->delete();

    }

}
