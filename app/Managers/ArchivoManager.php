<?php

namespace App\Managers;

use App\Models\Archivo;

class ArchivoManager extends BaseManager {

    public function __construct($entity = null) {
        $this->entityClassName = '\App\Models\Archivo';
        parent::__construct($entity);
    }

    public function getItem($archivoId) {
        $archivo = Archivo::find($archivoId);
        return $archivo;
    }

    public function getItems($orderBy) {
        return Archivo::orderBy($orderBy)->get();
    }

    public function saveItem(Archivo $archivo){
        $archivo->activo = 0;
        $archivo->save();
    }

    public function delete($archivoId){
        $archivo = $this->getItem($archivoId);
        $archivo->delete();

    }

}
