<?php

namespace App\Managers;

use App\Models\PrecargaEstado;

class PrecargaEstadoManager extends BaseManager {

    public function __construct($entity = null) {
        $this->entityClassName = '\App\Models\PrecargaEstado';
        parent::__construct($entity);
    }
    
    public function getItem($condicionId) {
        $condicion = PrecargaEstado::find($condicionId);
        return $condicion;
    }

    public function getBySlug($slug) {
        $condicion = PrecargaEstado::where('slug', $slug)
            ->first();
        return $condicion;
    }

}
