<?php

namespace App\Managers;

use App\Models\Condicion;
use App\Models\Precarga;
use Illuminate\Support\Facades\DB;

class CondicionManager extends BaseManager {

    public function __construct($entity = null) {
        $this->entityClassName = '\App\Models\Condicion';
        parent::__construct($entity);
    }
    
    public function getItem($condicionId) {
        $condicion = Condicion::find($condicionId);
        return $condicion;
    }

    public function getCondicion($precarga) {
        $condicion = DB::table('condiciones')
            ->join('condicion_por_reparticiones', 'condicion_por_reparticiones.condicion_id', 'condiciones.id')
            ->where('condicion_por_reparticiones.reparticion_id', $precarga->reparticion_id)
            ->where('condicion_por_reparticiones.zona_id', $precarga->zona_id)
            ->where('condicion_por_reparticiones.grupo_pago_id', $precarga->grupo_pago_id)
            ->select('condiciones.*')
            ->first();
        return $condicion;
    }
    
    public function saveItem(Condicion $condicion) {
        if(!isset($condicion->haber))
            $condicion->haber = 0;
        if(!isset($condicion->cbu))
            $condicion->cbu = 0;
        if(!isset($condicion->relacion_dependencia))
            $condicion->relacion_dependencia = 2;
        $condicion->save();
    }

    public function delete($condicionId) {
        $condicion = $this->getItem($condicionId);
        $condicion->delete();
    }
    
}
