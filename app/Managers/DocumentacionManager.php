<?php

namespace App\Managers;

use App\Models\Abogado;
use App\Models\DocumentacionAdjunta;
use App\Models\DocumentacionAdjuntasTipo;
use App\Models\Persona;
use App\Models\Precarga;
use Illuminate\Support\Facades\Storage;
use Livewire\TemporaryUploadedFile;

class DocumentacionManager extends BaseManager {

    public function __construct($entity = null) {
        $this->entityClassName = '\App\Models\DocumentacionAdjunta';
        parent::__construct($entity);
    }

    public function saveNewItem(Precarga $precarga, TemporaryUploadedFile $file, $fileName) {
        $metada = Storage::disk('documents')->getMetaData($fileName);
        $documentacionAdjunta = DocumentacionAdjunta::create([
            'precarga_id' => $precarga->id,
            'nombre' => $file->getClientOriginalName(),
            'path' => $fileName,
            'size' => $file->getSize()
        ]);
        return $documentacionAdjunta;
    }

    public function deleteDocumentacionAsociada(DocumentacionAdjunta $documentacionAdjunta) {
        DocumentacionAdjuntasTipo::where('documentacion_adjunta_id', $documentacionAdjunta->id)
            ->delete();
        return true;
    }

    public function addDocumentacionAsociada(DocumentacionAdjunta $documentacionAdjunta, $selected) {
        foreach($selected as $k => $v) {
            if ($v != 0) {
                $documentacionAdjuntaTipo = DocumentacionAdjuntasTipo::create([
                    'documentacion_adjunta_id' => $documentacionAdjunta->id,
                    'documentacion_id' => $v,
                ]);
            }
        }
        return true;
    }

}
