<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Managers;

use App\Models\Banco;

/**
 * Description of BancoManager
 *
 * @author Gab
 */
class BancoManager extends BaseManager {

    public function __construct($entity = null) {
        $this->entityClassName = '\App\Models\Banco';
        parent::__construct($entity);
    }
    
    public function getItem($bancoId) {
        $banco = Banco::find($bancoId);
        return $banco;
    }

    public function saveItem(Banco $banco) {
        $banco->save();
    }

    public function delete($bancoId) {
        $banco = $this->getItem($bancoId);
        $banco->delete();
    }
    
}
