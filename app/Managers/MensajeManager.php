<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Managers;

use App\Models\Mensaje;
use Illuminate\Support\Facades\Auth;

/**
 * Description of MensajeManager
 *
 * @author Gab
 */
class MensajeManager extends BaseManager {

    public function __construct($entity = null) {
        $this->entityClassName = '\App\Models\Mensaje';
        parent::__construct($entity);
    }
    
    public function getItem($mensajeId) {
        $mensaje = Mensaje::find($mensajeId);
        return $mensaje;
    }

    public function saveItem(Mensaje $mensaje, $precagaId, $mensajaId) {
        if (auth()->user()->rolPrincipal()->name == config('tools.constants.roles.comercial'))
            $mensaje->leido_comercial = true;
        else
            $mensaje->leido_empresa = true;
        $mensaje->mensaje_id = $mensajaId;
        $mensaje->precarga_id = $precagaId;
        $mensaje->user_id = auth()->user()->id;
        $mensaje->save();
    }

    public function delete($mensajeId) {
        $mensaje = $this->getItem($mensajeId);
        $mensaje->delete();
    }
    
}
