<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Managers;

use App\Models\Grilla;

/**
 * Description of GrillaManager
 *
 * @author Gab
 */
class GrillaManager extends BaseManager {

    public function __construct($entity = null) {
        $this->entityClassName = '\App\Models\Grilla';
        parent::__construct($entity);
    }

    public function getItem($grillaId) {
        $grilla = Grilla::find($grillaId);
        return $grilla;
    }

    public function getItems($orderBy) {
        return Grilla::orderBy($orderBy)->get();
    }

    public function saveItem(Grilla $grilla) {
        $grilla->save();
    }

    public function delete($grillaId) {
        $grilla = $this->getItem($grillaId);
        $grilla->delete();
    }

}
