<?php

namespace App\Managers;

use App\Http\Livewire\PrecargaEstados;
use App\Models\DocumentacionAdjunta;
use App\Models\ListaNegra;
use App\Models\MutualLineaGrilla;
use App\Models\Precarga;
use App\Models\PrecargaEstado;
use App\Models\PrecargaEstadoRelacion;
use App\Models\PrecargaUsuario;
use App\Models\Propuesta;
use App\Models\Rol;
use App\Tools\FormatsTool;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\Input;

class PrecargaManager extends BaseManager {

    public function __construct($entity = null) {
        $this->entityClassName = '\App\Models\Precarga';
        parent::__construct($entity);
    }

    public function newItem() {
        $precarga = new Precarga();
        $precarga->fecha_nacimiento = Carbon::now()->subYears(18)->format('d-m-Y');
        return $precarga;
    }

    public function getItem($precargaId) {
        try {
            $precarga = Precarga::find($precargaId);
            $precarga->cuota_maxima = ($precarga->cuota_maxima != '') ? number_format($precarga->cuota_maxima, 2, ',', '') : '';
            $precarga->monto_maximo = ($precarga->monto_maximo != '') ? number_format($precarga->monto_maximo, 2, ',', '') : '';
            $precarga->ingreso_laboral = ($precarga->ingreso_laboral != '') ? number_format($precarga->ingreso_laboral, 2, ',', '') : '';
            $precarga->fecha_nacimiento = Carbon::createFromDate($precarga->fecha_nacimiento)->format('d-m-Y');
        } catch (\Exception $e) {

        }
        return $precarga;
    }

    public function getItemsExcept(Precarga $precarga) {
        $precargas = Precarga::where('user_id', $precarga->user_id)
            ->where('numero_doc', $precarga->numero_doc)
            ->where('id', '<>', $precarga->id)
            ->get();
        return $precargas;
    }

    public function saveItem(Precarga $precarga, $repuestaAutomatica) {

        try {

            $esNueva = false;
            $precarga->contratacion_tipo_id = (new FormatsTool)->convertirANull($precarga->contratacion_tipo_id, [0]);
            $precarga->banco_id = (new FormatsTool)->convertirANull($precarga->banco_id, [0]);
            $precarga->codigo_postal_id = (new FormatsTool)->convertirANull($precarga->codigo_postal_id, [0]);
            $precarga->fecha_nacimiento = (new FormatsTool)->prepareToSave('date', $precarga->fecha_nacimiento);
            if ($precarga->pais_id == 0) {
                $precarga->pais_id = null;
            }
            $precarga->user_id = Auth::user()->id;

            $clienteEnListNegra = $this->estaEnListaNegra($precarga);
            if (!is_null($clienteEnListNegra)) {
                $repuestaAutomatica['success'] = 'rechazada';
                $repuestaAutomatica['message'] = $clienteEnListNegra->listaNegraMotivo->nombre;
            }

            if (is_null($precarga->precarga_estado_id)) {
                $esNueva = true;
                if ($repuestaAutomatica['success'] != 'none') {
                    $precarga->precarga_estado_id = (new PrecargaEstadoManager)->getBySlug($repuestaAutomatica['success'])->id;
                    $precarga->fecha_estado = Carbon::now()->format('Y-m-d h:i:s');
                    $precarga->save();
                    $this->changeState($precarga, $repuestaAutomatica['success'], (isset($repuestaAutomatica['message'])) ? $repuestaAutomatica['message'] : '');
                } else {
                    $precarga->precarga_estado_id = PrecargaEstado::where('slug', 'iniciada')->first()->id;
                    $precarga->fecha_estado = Carbon::now()->format('Y-m-d h:i:s');
                    $precarga->save();
                    $this->changeState($precarga, 'iniciada');
                }
                $repuestaAutomatica['success'] = 'none';
                $repuestaAutomatica['message'] = '';
            } else {
                $precarga->save();
            }

            if ($repuestaAutomatica['success'] != 'none') {
                $precarga->precarga_estado_id = (new PrecargaEstadoManager)->getBySlug($repuestaAutomatica['success'])->id;
                $precarga->fecha_estado = Carbon::now()->format('Y-m-d h:i:s');
                $this->changeState($precarga, $repuestaAutomatica['success'], (isset($repuestaAutomatica['message'])) ? $repuestaAutomatica['message'] : '');
            }
            $precarga->user_id = Auth::user()->id;
            $precarga->save();
            return $precarga;

        } catch (\Exception $e) {
            //dd($e);
        }

    }

    public function validarCondiciones(Precarga $precarga, $enviar_revision) {
        $respuesta = '';
        $respuestaAutomatica = [];
        $respuestaAutomatica['success'] = 'none';

        if ($enviar_revision) {

            $documentacionReciboSueldo = DocumentacionAdjunta::where('precarga_id', $precarga->id)
                ->where('documentacion_id', 2)
                ->first();

            if (is_null($documentacionReciboSueldo)) {
                $respuestaAutomatica = [];
                $respuestaAutomatica['success'] = 'error';
                $respuestaAutomatica['message'] = 'El Recibo de Sueldo es una documentación requerida.';
                return $respuestaAutomatica;
            }

            /* $documentacionResumen = DocumentacionAdjunta::where('precarga_id', $precarga->id)
                ->where('documentacion_id', 1)
                ->first();

            if (is_null($documentacionResumen)) {
                $respuestaAutomatica = [];
                $respuestaAutomatica['success'] = 'error';
                $respuestaAutomatica['message'] = 'El Resumen de Movimiento Bancario es una documentación requerida.';
                return $respuestaAutomatica;
            } */

            $condicion = (new CondicionManager)->getCondicion($precarga);

            if (!is_null($condicion)) {
                $fechaNacimiento = Carbon::createFromDate($precarga->fecha_nacimiento);
                if ($precarga->genero_id == 1) {
                    if (($fechaNacimiento < Carbon::now()->subYears($condicion->edad_max_mujer))) {
                        $respuesta .= 'Supera edad máxima.';
                    }
                } elseif (($fechaNacimiento < Carbon::now()->subYears($condicion->edad_max_hombre))) {
                    $respuesta .= 'Supera edad máxima.';
                }

                /* $respuesta = '';

                foreach($reparticion->condiciones as $condicion) {

                    //valida que el importe solicitado no supere el máximo y no sea inferior al mínimo permitidos
                    if (!(($precarga->importe_solicitado >= $condicion->monto_minimo) && ($precarga->importe_solicitado <= $condicion->monto_maximo))) {

                        $respuesta .= 'El monto no respeta mínimo y/o máximo.'."\n";
                    }

                    //valida que la edad de la persona no supere la máxima permitida
                    $fechaNacimiento = Carbon::createFromDate($precarga->fecha_nacimiento);

                    if($precarga->genero_id==1){

                        if (($fechaNacimiento < Carbon::now()->subYears($condicion->edad_max_mujer))) {

                            $respuesta .= 'Supera edad máxima.'."\n";
                        }

                    } elseif (($fechaNacimiento < Carbon::now()->subYears($condicion->edad_max_hombre))) {

                            $respuesta .= 'Supera edad máxima.'."\n";
                       }

                    //valida que el plazo no supere el máximo ni sea inferior al mínimo permitidos
                    if(($precarga->plazo < $condicion->cuotas_min_cbu) || ($precarga->plazo > $condicion->cuotas_max_cbu)){

                        $respuesta .= 'No cumple plazo mínimo y/o máximo'."\n";
                    }

                }
                info($respuesta); */
                if ($respuesta == '') {
                    $respuestaAutomatica = [];
                    $respuestaAutomatica['success'] = 'en_revision';
                } else {
                    $respuestaAutomatica = [];
                    $respuestaAutomatica['success'] = 'inconsistente';
                    $respuestaAutomatica['message'] = $respuesta;
                }
            } else {
                $respuestaAutomatica = [];
                $respuestaAutomatica['success'] = 'error';
                $respuestaAutomatica['message'] = 'La repartición seleccionada no tiene una condición asociada.';
            }
        }
        return $respuestaAutomatica;
    }

    public function changeState(Precarga $precarga, $estadoSlug, $mensaje = '') {
        $precargaEstado = PrecargaEstado::where('slug', $estadoSlug)
            ->first();
        $rol = Rol::where('name', config('tools.constants.roles.empleado'))
            ->first();
        if (($estadoSlug == 'en_revision') || ($estadoSlug == 'aceptada') || ($estadoSlug == 'firmada')) {
            if (($precarga->user_id == Auth::user()->id) && (Auth::user()->recibe_precargas)) {
                $empleado = Auth::user();
            } else {
                /**
                 * Busco a quien le asigné la última precarga
                 */
                $ultimoAsigado = 0;
                $precargaUsuario = PrecargaUsuario::orderBy('created_at', 'DESC')
                    ->first();
                if (!is_null($precargaUsuario))
                    $ultimoAsigado = $precargaUsuario->user_id;
                $empleado = DB::table('users')
                    ->join('model_has_roles', 'model_has_roles.model_id', 'users.id')
                    ->where('users.recibe_precargas', 1)
                    ->where('users.id', '>', $ultimoAsigado)
                    ->where('model_has_roles.role_id', $rol->id)
                    ->orderBy('users.id')
                    ->first();
                if (is_null($empleado))
                    $empleado = DB::table('users')
                        ->join('model_has_roles', 'model_has_roles.model_id', 'users.id')
                        ->where('users.recibe_precargas', true)
                        ->where('model_has_roles.role_id', $rol->id)
                        ->orderBy('users.id')
                        ->first();
            }
            $precargaUsuario = PrecargaUsuario::create([
                'precarga_id' => $precarga->id,
                'user_id' => $empleado->id,
            ]);
            $fechaNacimiento = explode('-', $precarga->fecha_nacimiento);
            if (strlen($fechaNacimiento[0]) == 2) {
                $precarga->fecha_nacimiento = (new FormatsTool)->prepareToSave('date', $precarga->fecha_nacimiento);
            }
            $precarga->cuota_maxima = (new FormatsTool)->prepareToSave('decimal', $precarga->cuota_maxima);
            $precarga->monto_maximo = (new FormatsTool)->prepareToSave('decimal', $precarga->monto_maximo);
            $precarga->ingreso_laboral = (new FormatsTool)->prepareToSave('decimal', $precarga->ingreso_laboral);
            $precarga->user_asignado_id = $empleado->id;
            $precarga->save();
        }
        $precargaEstadoRelacion = PrecargaEstadoRelacion::create([
            'precarga_id' => $precarga->id,
            'precarga_estado_id' => $precargaEstado->id,
            'user_id' => Auth::user()->id,
            'mensaje' => $mensaje
        ]);
        Precarga::where('id', $precarga->id)
            ->update(['precarga_estado_id' => $precargaEstado->id]);
        return $precargaEstadoRelacion;
    }

    public function getFilterItems($nombre, $email, $reparticion, $estado, $ordenarPor, $sentido, $comercialNombre = '', $selectColaborador) {

        if (is_null($selectColaborador))
            $selectColaborador = 'todos';

        $roles = auth()->user()->getRoleNames();
        if ($estado == 0)
            $estado = 'todos';
        switch ($roles[0]) {
            case config('tools.constants.roles.administrador'):
            case config('tools.constants.roles.tesorero'):
                $listado = DB::table('precargas')
                    ->join('reparticiones', 'reparticiones.id', 'precargas.reparticion_id')
                    ->join('precarga_estados', 'precarga_estados.id', 'precargas.precarga_estado_id')
                    ->join('users', 'users.id', 'precargas.user_id')
                    ->where(function($query) use ($nombre)
                        {
                            $query->where('precargas.nombre', 'LIKE', '%'.$nombre.'%')
                                ->orWhere('precargas.apellido', 'LIKE', '%'.$nombre.'%')
                                ->orWhere('precargas.numero_doc', 'LIKE', '%'.$nombre.'%');
                        })
                    ->where('users.name', ($comercialNombre != '' ? 'LIKE' : '<>'), ($comercialNombre != '' ? '%'.$comercialNombre.'%' : ''))
                    ->where('precargas.user_asignado_id', ($selectColaborador == 'todos' ? '<>' : '='), ($selectColaborador == 'todos' ? 0 : $selectColaborador))
                    ->where('precarga_estados.slug', ($estado != 'todos') ? '=' : '<>', $estado)
                    ->where('reparticiones.nombre', ($reparticion != '' ? 'LIKE' : '<>'), ($reparticion != '' ? '%'.$reparticion.'%' : ''))
                    ->orderBy('precargas.'.$ordenarPor, $sentido)
                    ->select('precargas.*')
                    ->paginate(env('ITEMS_PER_PAGE', 10));
                break;

            case config('tools.constants.roles.empleado'):
                $listado = DB::table('precargas')
                    ->join('reparticiones', 'reparticiones.id', 'precargas.reparticion_id')
                    ->join('precarga_estados', 'precarga_estados.id', 'precargas.precarga_estado_id')
                    ->join('users', 'users.id', 'precargas.user_id')
                    ->where(function($query) use ($nombre)
                        {
                            $query->where('precargas.nombre', 'LIKE', '%'.$nombre.'%')
                                ->orWhere('precargas.apellido', 'LIKE', '%'.$nombre.'%')
                                ->orWhere('precargas.numero_doc', 'LIKE', '%'.$nombre.'%');
                        })
                    ->where('users.name', ($comercialNombre != '' ? 'LIKE' : '<>'), ($comercialNombre != '' ? '%'.$comercialNombre.'%' : ''))
                    ->where('precarga_estados.slug', ($estado != 'todos') ? '=' : '<>', $estado)
                    ->where('reparticiones.nombre', ($reparticion != '' ? 'LIKE' : '<>'), ($reparticion != '' ? '%'.$reparticion.'%' : ''))
                    ->where(function($query)
                    {
                        $query->where('user_asignado_id', Auth::user()->id)
                            ->orWhere('user_id', Auth::user()->id);
                    })
                    ->orderBy('precargas.'.$ordenarPor, $sentido)
                    ->select('precargas.*')
                    ->paginate(env('ITEMS_PER_PAGE', 10));
                break;

            case config('tools.constants.roles.comercial'):
                $listado = DB::table('precargas')
                    ->join('reparticiones', 'reparticiones.id', 'precargas.reparticion_id')
                    ->join('precarga_estados', 'precarga_estados.id', 'precargas.precarga_estado_id')
                    ->where(function($query) use ($nombre)
                        {
                            $query->where('precargas.nombre', 'LIKE', '%'.$nombre.'%')
                                ->orWhere('precargas.apellido', 'LIKE', '%'.$nombre.'%')
                                ->orWhere('precargas.numero_doc', 'LIKE', '%'.$nombre.'%');
                        })
                    ->where('precarga_estados.slug', ($estado != 'todos') ? '=' : '<>', $estado)
                    ->where('reparticiones.nombre', ($reparticion != '' ? 'LIKE' : '<>'), ($reparticion != '' ? '%'.$reparticion.'%' : ''))
                    ->where('user_id', Auth::user()->id)
                    ->orderBy('precargas.'.$ordenarPor, $sentido)
                    ->select('precargas.*')
                    ->paginate(env('ITEMS_PER_PAGE', 10));
                break;
        }
        return $listado;
    }

    public function arrayPaginator($array, $request){
        $page = $request->get('page', 1);
        $perPage = env('ITEMS_PER_PAGE', 10);
        $offset = ($page * $perPage) - $perPage;
        return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
            ['path' => $request->url(), 'query' => $request->query()]);
    }

    public function getItemStates(Precarga $precarga) {
        return DB::table('precarga_estado')
            ->join('precarga_estados', 'precarga_estados.id', 'precarga_estado.precarga_estado_id')
            ->join('users', 'users.id', 'precarga_estado.user_id')
            ->where('precarga_estado.precarga_id', $precarga->id)
            ->select('precarga_estados.nombre', 'precarga_estados.color', 'precarga_estados.icono', 'precarga_estados.slug',
                DB::raw('DATE_FORMAT(precarga_estado.created_at, "%d-%m-%Y %H:%k:%S") as fecha'),
                'users.name', 'precarga_estado.mensaje')
            ->orderBy('precarga_estado.id', 'DESC')
            ->get();
    }

    public function estaEnListaNegra(Precarga $precarga) {
        $enListaNegra = ListaNegra::where('documento_tipo_id', $precarga->documento_tipo_id)
            ->where('numero_doc', $precarga->numero_doc)
            ->first();
        if (is_null($enListaNegra))
            return null;
        return $enListaNegra;
    }

    public function getPropuestas(Precarga $precarga, $estadoActual)
    {
        $propuestas = Propuesta::where('precarga_id', $precarga->id)
            ->where('aceptada', true)
            ->get();
        /* $listado = collect();
        if (($estadoActual->slug == 'aprobada') || ($estadoActual->slug == 'firmada')) {
            foreach ($propuestas as $propuesta) {
                $propuesta->formularios = $this->formulariosPorPropuesta($precarga, $propuesta, false);
                $propuesta->fondistas = $this->fondistasPorPropuesta($precarga, $propuesta);
                $propuesta->opcionales = $this->fondistasPorPropuesta($precarga, $propuesta);
                $propuesta->dependientes = $this->formulariosPorPropuesta($precarga, $propuesta, true);
                $listado->add($propuesta);
            }
        } */
        return $propuestas;
    }

    public function fondistasPorPropuesta(Precarga $precarga, Propuesta $propuesta, $extras = false)
    {
        $fondistas = new Collection();
        switch ($propuesta->linea_credito_id) {
            case 1:
                $fondistas->add([1 => 'DAP']);
                $fondistas->add([2 => 'VIDEGAR']);
                break;
        }
        return $fondistas;
    }

    public function formulariosPorPropuesta(Precarga $precarga, Propuesta $propuesta, $extras = false) {
        $condicion = DB::table('condiciones')
            ->join('condicion_por_reparticiones', 'condicion_por_reparticiones.condicion_id', 'condiciones.id')
            ->where('condicion_por_reparticiones.reparticion_id', $precarga->reparticion_id)
            ->where('condicion_por_reparticiones.zona_id', $precarga->zona_id)
            ->where('condicion_por_reparticiones.grupo_pago_id', $precarga->grupo_pago_id)
            ->select('condiciones.*')
            ->first();
        $mutualLineaGrilla = MutualLineaGrilla::where('grilla_id', $propuesta->grilla_id)
            ->first();
        $formularios = DB::table('mutual_linea_formularios')
            ->join('formularios', 'formularios.id', 'mutual_linea_formularios.formulario_id')
            ->join('archivos', 'archivos.formulario_id', 'formularios.id')
            ->where('grilla_id', $propuesta->grilla_id)
            ->where('formularios.extra', ($extras) ? 1 : 0)
            ->where('mutual_linea_id', $mutualLineaGrilla->mutual_linea_id)
            ->where('condicion_id', $condicion->id)
            ->orderBy('formularios.id')
            ->select('archivos.*', 'formularios.nombre as form', 'formularios.opcional')
            ->get();
        return $formularios;
    }

}
