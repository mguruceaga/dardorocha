<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Managers;

use App\Models\Fondista;

/**
 * Description of FondistaManager
 *
 * @author Gab
 */
class FondistaManager extends BaseManager {

    public function __construct($entity = null) {
        $this->entityClassName = '\App\Models\Fondista';
        parent::__construct($entity);
    }

    public function getItem($fondistaId) {
        $fondista = Fondista::find($fondistaId);
        return $fondista;
    }

    public function getItems($orderBy) {
        return Fondista::orderBy($orderBy)->get();
    }

    public function saveItem(Fondista $fondista) {
        $fondista->save();
    }

    public function delete($fondistaId) {
        $fondista = $this->getItem($fondistaId);
        $fondista->delete();
    }

}
