<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Managers;

use App\Models\Zona;

/**
 * Description of ZonaManager
 *
 * @author Gab
 */
class ZonaManager extends BaseManager{

    public function __construct($entity = null) {
        $this->entityClassName = '\App\Models\Zona';
        parent::__construct($entity);
    }

    public function getItem($zonaId) {
        $zona = Zona::find($zonaId);
        return $zona;
    }

    public function getItems($orderBy) {
        return Zona::orderBy($orderBy)->get();
    }

    public function saveItem(Zona $zona) {
        $zona->save();
    }

    public function delete($zonaId) {
        $zona = $this->getItem($zonaId);
        $zona->delete();
    }

}

