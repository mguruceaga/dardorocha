<?php

namespace App\Managers;

use Illuminate\Support\Facades\File;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Str;

class BaseManager {

    protected $entity;
    protected $request;
    protected $prefix;
    protected $uploadFolder = 'upload';

    public function __construct($entity = null) {
        $this->entity = $entity;
        $this->request = request();
    }

    public function entity() {
        return $this->entity;
    }

    public function save() {
        $this->entity->fill($this->request->all());
        return $this->entity->save();
    }

    public function find($id) {
        return $this->entityClassName::find($id);
    }

    public function findBySlug($slug) {
        return $this->entityClassName::where('slug', $slug)->first();
    }

    public function newEntity() {
        return new $this->entityClassName;
    }

    public function loadNewEntity() {
        $this->entity = new $this->entityClassName;
        return $this->entity;
    }

    public function loadEntityById($id) {
        $this->entity = $this->find($id);
        return $this->entity;
    }

}
