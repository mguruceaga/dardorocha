<?php

namespace App\Managers;

use App\Models\DocumentoTipo;

class DocumentoTipoManager extends BaseManager {

    public function __construct($entity = null) {
        $this->entityClassName = '\App\Models\DocumentoTipo';
        parent::__construct($entity);
    }

    public function getItem($documentoTipoId) {
        $documentoTipo = DocumentoTipo::find($documentoTipoId);
        return $documentoTipo;
    }

    public function getItems($orderBy) {
        return DocumentoTipo::orderBy($orderBy)->get();
    }

    public function saveItem(DocumentoTipo $documentoTipo){
        $documentoTipo->save();
    }

    public function delete($documentoTipoId){
        $documentoTipo = $this->getItem($documentoTipoId);
        $documentoTipo->delete();

    }

}
