<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Managers;

use App\Models\User;
use App\Models\Usuario;
use App\Notifications\NuevoUsuario;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

/**
 * Description of ZonaManager
 *
 * @author Gab
 */
class UsuarioManager extends BaseManager {

    public function __construct($entity = null) {
        $this->entityClassName = '\App\Models\Users';
        parent::__construct($entity);
    }

    public function getItem($usuarioId) {
        $usuario = User::withTrashed()
                    ->find($usuarioId);
        return $usuario;
    }

    public function getItems($orderBy) {
        return User::withTrashed()
            ->where('suspended', 1)
            ->orderBy($orderBy)->get();
    }

    public function saveItem(User $usuario, $usuarioRol) {
        $usuarioNuevo = is_null($usuario->id);
        $password = '';
        if ($usuarioNuevo) {
            $password = Str::random(8);
            $usuario->password = bcrypt($password);
            $usuario->primer_ingreso = 1;
        }
        $usuario->save();
        $roles = $usuario->getRoleNames();
        foreach ($roles as $rol) {
            $role = Role::where('name', $rol)->first();
            $usuario->removeRole($role);
        }
        if ($usuario->suspended)
            $usuario->delete();
        else
            $usuario->restore();
        if ($usuarioRol != 0) {
            $role = Role::find($usuarioRol);
            $usuario->assignRole($role);
        }
        if ($usuarioNuevo)
            Notification::send(User::where('id', $usuario->id)->get(), new NuevoUsuario($password));
    }

    public function delete($usuarioId) {
        $usuario = $this->getItem($usuarioId);
        $usuario = $this->getItem($usuarioId);
        $usuario->delete();
    }

}

