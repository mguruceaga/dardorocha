<?php

namespace App\Managers;

use App\Models\ListaNegraMotivo;

class ListaNegraMotivoManager extends BaseManager {

    public function __construct($entity = null) {
        $this->entityClassName = '\App\Models\ListaNegraMotivo';
        parent::__construct($entity);
    }

    public function getItem($listaNegraId) {
        $listaNegra = ListaNegraMotivo::find($listaNegraId);
        return $listaNegra;
    }

    public function getItems($orderBy) {
        return ListaNegraMotivo::orderBy($orderBy)->get();
    }

    public function saveItem(ListaNegraMotivo $listaNegra){
        $listaNegra->save();
    }

    public function delete($listaNegraId){
        $listaNegra = $this->getItem($listaNegraId);
        $listaNegra->delete();

    }

}
