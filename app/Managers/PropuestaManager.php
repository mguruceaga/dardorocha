<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Managers;

use App\Models\Debito;
use App\Models\Grilla;
use App\Models\LineaCredito;
use App\Models\Mutual;
use App\Models\MutualLinea;
use App\Models\Precarga;
use App\Models\Propuesta;
use App\Models\PropuestaFormulario;
use App\Tools\FormatsTool;
use Illuminate\Support\Facades\DB;

/**
 * Description of BancoManager
 *
 * @author Gab
 */
class PropuestaManager extends BaseManager {

    public function __construct($entity = null) {
        $this->entityClassName = '\App\Models\Propuesta';
        parent::__construct($entity);
    }

    public function newItem() {
        return new Propuesta();
    }

    public function getItem($propuestaId) {
        $propuesta = Propuesta::find($propuestaId);
        $propuesta->monto = ($propuesta->monto != '') ? number_format($propuesta->monto, 2, ',', '') : '';
        $propuesta->cuotas = ($propuesta->cuotas != '') ? number_format($propuesta->cuotas, 2, ',', '') : '';
        $propuesta->valor_cuota = ($propuesta->valor_cuota != '') ? number_format($propuesta->valor_cuota, 2, ',', '') : '';
        $propuesta->adicionales = ($propuesta->adicionales != '') ? number_format($propuesta->adicionales, 2, ',', '') : '';
        $propuesta->valor_cuota_social = ($propuesta->valor_cuota_social != '') ? number_format($propuesta->valor_cuota_social, 2, ',', '') : '';
        return $propuesta;
    }

    public function getItemFromPrecarga($precargaId, $aceptada = false) {
        return Propuesta::where('precarga_id', $precargaId)
            ->where('aceptada', $aceptada ? '=' : '<>', $aceptada ? 1 : 2)
            ->orderBy('id', 'DESC')
            ->get();
    }

    public function getLineasCredito() {
        return MutualLinea::all();
    }

    public function getGrillas() {
        return DB::table('mutual_lineas')
            ->join('mutual_linea_grillas', 'mutual_linea_grillas.mutual_linea_id', 'mutual_lineas.id')
            ->join('grillas', 'grillas.id', 'mutual_linea_grillas.grilla_id')
            ->select('grillas.id', 'grillas.nombre', 'mutual_linea_grillas.mutual_linea_id')
            ->get();
    }

    public function asociarFormularios (Propuesta $propuesta) {
        if ($propuesta->aceptada) {
            $fondista = $propuesta->fondista_id;
            $reparticion = $propuesta->precarga->reparticion_id;
            $formulariosLinea = DB::table('mutual_linea_formularios')
                ->join('formularios', 'formularios.id', 'mutual_linea_formularios.formulario_id')
                ->join('archivos', 'archivos.formulario_id', 'formularios.id')
                ->where('mutual_linea_formularios.mutual_linea_id', $propuesta->linea_credito_id)
                ->where(function ($result) use ($fondista) {
                    return $result->where('archivos.fondista_id', $fondista)
                        ->orWhereNull('archivos.fondista_id');
                })
                ->where(function ($result) use ($reparticion) {
                    return $result->where('archivos.reparticion_id', $reparticion)
                        ->orWhereNull('archivos.reparticion_id');
                })
                ->select('mutual_linea_formularios.*', 'formularios.nombre as nombreFormularios',
                    'archivos.id as archivo_id', 'archivos.nombre as nombreArchivo',
                    'archivos.fondista_id', 'archivos.reparticion_id')
                ->get();
            foreach ($formulariosLinea as $formularioLinea) {
                $propuestaFormulario = new PropuestaFormulario();
                $propuestaFormulario->propuesta_id = $propuesta->id;
                $propuestaFormulario->formulario_id = $formularioLinea->formulario_id;
                $propuestaFormulario->archivo_id = $formularioLinea->archivo_id;
                $propuestaFormulario->tipo_relacion_id = $formularioLinea->tipo_relacion_id;
                $propuestaFormulario->original_bloqueado = $formularioLinea->original_bloqueado;
                $propuestaFormulario->copias_identicas = $formularioLinea->copias_identicas;
                $propuestaFormulario->copias_blanco = $formularioLinea->copias_blanco;
                switch ($formularioLinea->tipo_relacion_id) {
                    case 1:
                        $propuestaFormulario->activo = true;
                        break;
                    case 2:
                        $propuestaFormulario->activo = false;
                        break;
                    case 3:
                        if ($propuesta->reparticion_id == $formularioLinea->reparticion_id) {
                            $propuestaFormulario->activo = true;
                        } else {
                            $propuestaFormulario->activo = false;
                        }
                        break;
                }
                $propuestaFormulario->save();
            }
        }
    }

    public function getDebitos() {
        return Debito::all();
    }

    public function saveItem(Propuesta $propuesta) {
        $propuesta->save();
    }

    public function delete($propuestaId) {
        $propuesta = $this->getItem($propuestaId);
        $propuesta->delete();
    }

}
