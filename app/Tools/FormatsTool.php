<?php

namespace App\Tools;

use App\Models\PrecargaEstado;
use Carbon\Carbon;

class FormatsTool {

    public function arregloEstados() {
        $estados = PrecargaEstado::all();
        $arrEstados = [];
        foreach($estados as $estado) {
            $arr = [];
            $arr['id'] = $estado->id;
            $arr['nombre'] = $estado->nombre;
            $arr['slug'] = $estado->slug;
            $arr['abreviatura'] = $estado->abreviatura;
            $arr['color'] = $estado->color;
            $arr['intensidad'] = $estado->intensidad;
            $arr['icono_ref'] = $estado->icono_ref;
            $arr['icono'] = $estado->icono;
            $arrEstados[$estado->slug] = $arr;
        }
        return $arrEstados;
    }

    public function prepareToSave($campo, $valor)
    {
        switch ($campo) {
            case 'date':
                $res = Carbon::createFromDate($valor)->format(config('tools.formats.date.database'));
                break;
            case 'decimal':
                if ($valor != '') {
                    if (strpos($valor, ',')) {
                        $res = str_replace(',', '.', str_replace('.', '', $valor));
                    } else {
                        $explode = explode( '.', $valor);
                        if (isset($explode[1])) {
                            if (strlen($explode[1]) == 3) {
                                $res = str_replace('.', '', $valor);
                            } else {
                                $res = $valor;
                            }
                        } else {
                            $res = $valor;
                        }
                    }
                } else {
                    $res = null;
                }
                break;
        }
        return $res;
    }

    public function convertirANull($valor, $arrOpcionesNulas)
    {
        if (!is_null($valor)) {
            foreach($arrOpcionesNulas as $k => $v) {
                if ($valor == $v) {
                    $valor = null;
                }
            }
        }
        return $valor;
    }

}
