<?php

namespace App\Tools;

use App\Models\EntidadFinanciera;
use Illuminate\Support\Facades\Crypt;

class HelpersTool {

    /**
     * Se utiliza para devolver encriptado los datos recibidos si esta en true la variable de entorno APP_ENCRYPT
     *
     * @param $dato
     * @return string
     */
    public function helper_getUrlHash($datos)
    {
        if (env('APP_ENCRYPT', false)) {
            $datos = Crypt::encryptString($datos);
        }
        return $datos;
    }

    /**
     * Se utiliza para Desncriptar los datos enviados desde los formularios, generalmente por Método GET
     * Recibe los datos y un valor de tipo que indica si es Simple o Compuesto
     * Simple es un dato encriptado
     * Compuesto es una secuencia del tipo clave=valor separados por & ('caja_id=1&caja_instancia_id=8' por ejemplo)
     *
     * @param $datos
     * @param string $tipo
     * @return array|mixed
     */
    public function helper_desencriptar($datos, $tipo = 'encrypt_simple') {
        /**
         * Si está actica la encriptación por la variable de entorno (APP_ENCRYPT) desencripto los datos recibidos
         */
        if (env('APP_ENCRYPT', false)) {
            $datos = Crypt::decryptString($datos);
        }

        /**
         * Puede ser que la encriptación sea un dato simple y directo, o uno compuesto
         */
        switch ($tipo) {

            /**
             * Cuando el tipo de encriptación es simple se devuelve el dato directamente
             */
            case 'encrypt_simple':

                return $datos;
                break;

            /**
             * Parseo los datos recibidos
             * Los datos vienen separados por & y clave=valor
             */
            case 'encrypt_compuesta':

                $datos = explode('&', $datos);
                $data = array();
                foreach($datos as $dato) {
                    $dato = explode('=', $dato);
                    $data[$dato[0]] = $dato[1];
                }
                return $data;
                break;

        }

    }

}
