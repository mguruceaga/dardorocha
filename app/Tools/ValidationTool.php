<?php

namespace App\Tools;

use App\Models\EntidadFinanciera;

class ValidationTool {

    /**
     * Devuelve true si el CBU $cbu es válido.
     * Controla longitud, caracteres y número verificador de acuerdo a lo
     * expuesto por Banco Central Argentino
     * Otras referencias: https://es.wikipedia.org/wiki/Clave_Bancaria_Uniforme
     *
     * @param string $cbu
     * @return boolean
     */
    public function cbuIsValid($cbu)
    {
        // Estrictamente sólo 22 números
        if (!preg_match('/[0-9]{22}/', $cbu))
            return false;
        /* $arr = str_split($cbu);
        if ($arr[7] <> self::getCbuDigitoVerificador($arr, 0, 6))
            return false;
        if ($arr[21] <> self::getCbuDigitoVerificador($arr, 8, 20))
            return false; */
        return true;
    }

    /**
     * Devuelve el dígito verificador para los dígitos de las posiciones "pos_inicial" a "pos_final"
     * de la cadena "$numero" usando clave 10 con ponderador 9713
     *
     * @param array $numero arreglo de digitos
     * @param integer $pos_inicial
     * @param integer $pos_final
     * @return integer digito verificador de la cadena $numero
     */
    private function getCbuDigitoVerificador($numero, $pos_inicial, $pos_final)
    {
        $ponderador = array(3,1,7,9);
        $suma = 0;
        $j = 0;
        for ($i = $pos_final; $i >= $pos_inicial; $i--)
        {
            $suma = $suma + ($numero[$i] * $ponderador[$j % 4]);
            $j++;
        }
        return (10 - $suma % 10) % 10;
    }

    /**
     * Recupero los 3 primeros números del CBU que son el código del banco o entidad financiera
     *
     * @param string $cbu
     * @return string
     */
    public function getCbuBankId($cbu)
    {
        return substr($cbu, 0, 3);
    }

    /**
     * Recupero el nombre del banco de acuerdo a los 3 primeros números del CBU que son el código del banco o entidad financiera
     *
     * @param string $cbu_or_id
     * @return string
     */
    public function getBankName($cbu)
    {
        $codigo = $this->getCbuBankId($cbu);
        $bank = EntidadFinanciera::wheere('codigo', $codigo)->first();
        if (is_null($bank))
            return 'No identificado';
        return $bank->nombre;
    }

    /**
     * Valido si el CUIT/CUIL es válido
     *
     * @param $cuit
     * @return bool
     */
    public function cuitIsValid($cuit) {
        if ($cuit == '00-00000000-0')
            return false;
        $digits = array();
        if (strlen($cuit) != 13) {
            $cuit = substr($cuit, 0, 2) . '-' . substr($cuit, 2, 8) . '-' . substr($cuit, 10, 1);
        }
        for ($i = 0; $i < strlen($cuit); $i++) {
            if ($i == 2 or $i == 11) {
                if ($cuit[$i] != '-') return false;
            } else {
                if (!ctype_digit($cuit[$i])) return false;
                if ($i < 12) {
                    $digits[] = $cuit[$i];
                }
            }
        }
        $acum = 0;
        foreach (array(5, 4, 3, 2, 7, 6, 5, 4, 3, 2) as $i => $multiplicador) {
            $acum += $digits[$i] * $multiplicador;
        }
        $cmp = 11 - ($acum % 11);
        if ($cmp == 11) $cmp = 0;
        if ($cmp == 10) $cmp = 9;
        return ($cuit[12] == $cmp);
    }

    /**
     * Valido si el DNI coincide con el numero que se encuentra entre la posicion 2 y la posición 9 del CUIT
     *
     * @param $cuit
     * @return bool
     */
    public function dniIsInCuit($cuit, $dni) {
        if (strlen($cuit) == 13)
            $dniInCuit = substr($cuit, 3, 8);
        else
            $dniInCuit = substr($cuit, 2, 8);
        if ($dni == $dniInCuit) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Devuelve true si el Género es correcto respecto del CUIT.
     * Femenino: CUIT empieza con 24 y 27
     * Femenino: CUIT empieza con 20 y 23
     *
     * @param string $cbu
     * @return boolean
     */
    public function generoCuit($genero, $cuit)
    {
        $inicio = explode('-', $cuit);
        $valido = false;
        switch ($genero) {
            case 1:
                if (($inicio[0] == 24) || ($inicio[0] == 27))
                    $valido = true;
                break;
            case 2:
                if (($inicio[0] == 20) || ($inicio[0] == 23))
                    $valido = true;
                break;
        }
        return $valido;
    }

}
