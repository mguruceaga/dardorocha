<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListaNegra extends Model
{
    use HasFactory;

    protected $table = 'lista_negra';
    protected $guarded = ['id'];

    public function listaNegraMotivo() {
        return $this->belongsTo('App\Models\ListaNegraMotivo');
    }

}
