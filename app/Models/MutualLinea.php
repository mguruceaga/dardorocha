<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MutualLinea extends Model
{
    use HasFactory;
    protected $table = 'mutual_lineas';
    protected $guarded = ['id'];
}
