<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reparticion extends Model
{
    use HasFactory;

    protected $table = 'reparticiones';
    protected $guarded = ['id'];

    public function condiciones() {
        return $this->belongsToMany('App\Models\CondicionPorReparticion', 'condicion_por_reparticiones');
    }

    public function zonas()
    {
        return $this->hasMany(Zona::class);
    }
}
