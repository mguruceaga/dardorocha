<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Zona extends Model
{
    use HasFactory;

    protected $table = 'zonas';
    protected $guarded = ['id'];

    public function gruposPago(){
        return $this->belongsToMany(GrupoPago::class)->withTimestamps();
    }

    public function reparticiones()
    {
        return $this->belongsTo(Reparticion::class);
    }

    public function origenes()
    {
        return DB::table('grupos_pagos')
            ->join('zona_por_grupo_pagos', 'zona_por_grupo_pagos.grupo_pago_id', 'grupos_pagos.id')
            ->where('zona_por_grupo_pagos.zona_id', $this->id)
            ->select('grupos_pagos.*')
            ->get();
    }

}
