<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReparticionRestriccion extends Model
{
    use HasFactory;

    protected $table = 'reparticion_restricciones';
    protected $guarded = ['id'];
}
