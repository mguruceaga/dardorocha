<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrestamoTipo extends Model
{
    use HasFactory;

    protected $table = 'prestamo_tipos';
    protected $guarded = ['id'];
}
