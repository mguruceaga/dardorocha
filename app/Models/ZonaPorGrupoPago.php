<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ZonaPorGrupoPago extends Model
{
    use HasFactory;
    
    protected $table = 'zona_por_grupo_pagos';
    protected $guarded = ['id'];
}
