<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mensaje extends Model
{
    use HasFactory;

    protected $table = 'mensajes';
    protected $guarded = ['id'];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function respuestas() {
        return Mensaje::where('mensaje_id', $this->id)
            ->get();
    }

    public function leido() {
        $rolPrincipal = auth()->user()->rolPrincipal();
        switch ($rolPrincipal->name) {
            case config('tools.constants.roles.comercial'):
                $leido = $this->leido_comercial;
                break;
            case config('tools.constants.roles.administrador'):
            case config('tools.constants.roles.tesorero'):
            case config('tools.constants.roles.empleado'):
                $leido = $this->leido_empresa;
                break;
        }
        if (!$leido)
            if ($this->user_id == auth()->user()->id)
                $leido = true;
        return $leido;
    }

}
