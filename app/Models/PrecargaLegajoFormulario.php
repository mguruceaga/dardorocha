<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrecargaLegajoFormulario extends Model
{
    use HasFactory;

    protected $table = 'precarga_legajo_formularios';
    protected $guarded = ['id'];

}
