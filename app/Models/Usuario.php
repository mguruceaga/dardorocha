<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

class Usuario extends Model
{
    use HasFactory;
    use HasRoles;
    use SoftDeletes;

    protected $table = 'users';
    protected $guarded = ['id'];

    public function role() {
        return ModelHasRole::where('model_id', $this->id)->orderBy('role_id', 'ASC')->first();
    }

}
