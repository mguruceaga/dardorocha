<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReparticionAnexo extends Model
{
    protected $table = 'reparticiones';
    protected $guarded = ['id'];
}
