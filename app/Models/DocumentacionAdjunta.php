<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocumentacionAdjunta extends Model
{
    use HasFactory;

    protected $table = 'documentacion_adjuntas';
    protected $guarded = ['id'];

    public function getSizeKbAttribute() {
        return round($this->size/1024, 2) . ' Kb';
    }
}
