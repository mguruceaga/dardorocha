<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrecargaUsuario extends Model
{
    use HasFactory;

    protected $table = 'precarga_usuario';
    protected $guarded = ['id'];
}
