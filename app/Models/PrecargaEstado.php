<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrecargaEstado extends Model
{
    use HasFactory;

    protected $table = 'precarga_estados';
    protected $guarded = ['id'];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

}
