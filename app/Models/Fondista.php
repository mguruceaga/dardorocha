<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fondista extends Model
{
    use HasFactory;

    protected $table = 'fondistas';
    protected $guarded = ['id'];
}
