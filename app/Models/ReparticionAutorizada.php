<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReparticionAutorizada extends Model
{
    use HasFactory;

    protected $table = 'reparticiones_autorizadas';
    protected $guarded = ['id'];

}
