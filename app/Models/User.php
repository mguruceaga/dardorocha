<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Jetstream\HasTeams;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use HasTeams;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use HasRoles;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    public function getProfilePhotoUrlAttribute()
    {
        return $this->profile_photo_path
            ? url('../laravel/storage/app/public/'.$this->profile_photo_path)
            : $this->defaultProfilePhotoUrl();
    }

    public function iniciales() {
        $names = explode(' ', $this->name);
        if (count($names) > 1) {
            $iniciales = strtoupper(substr($names[0], 0, 1)) . strtoupper(substr($names[1], 0, 1));
        } else {
            $iniciales = strtoupper(substr($names[0], 0, 2));
        }
        return $iniciales;
    }

    public function rolPrincipal()
    {
        $roles = $this->getRoleNames();
        if (!isset($roles[0])) {
            $rol = new Rol();
            $rol->name = 'No tiene Rol';
        } else {
            $rol = Rol::where('name', $roles[0])->first();
        }
        return $rol;
    }

    public function perfil() {
        $perfil = '';
        $rolPrincipal = auth()->user()->rolPrincipal();
        switch ($rolPrincipal->name) {
            case config('tools.constants.roles.comercial'):
                $perfil = 'comercial';
                break;
            case config('tools.constants.roles.administrador'):
            case config('tools.constants.roles.tesorero'):
            case config('tools.constants.roles.empleado'):
                $perfil = 'empresa';
                break;
        }
        return $perfil;
    }

    public function color()
    {
        $color = 'yellow';
        if ($this->hasRole(config('tools.constants.roles.administrador'))) {
            $color = 'purple';
        } else if ($this->hasRole(config('tools.constants.roles.empleado'))) {
            $color = 'green';
        } else if ($this->hasRole(config('tools.constants.roles.comercial'))) {
            $color = 'yellow';
        }
        return $color;
    }



}
