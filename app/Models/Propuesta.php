<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Propuesta extends Model
{
    use HasFactory;

    protected $table = 'propuestas';
    protected $guarded = ['id'];
    protected $attributes = [
        'linea_credito_id' => 0,
        'monto' => '',
        'cuotas' => '',
        'valor_cuota' => '',
        'debito_id' => 0,
        'adicionales' => '',
        'valor_cuota_social' => '',
        'precarga_id' => '',
    ];

    public function lineaCredito() {
        $lineaDeCredito = MutualLinea::find($this->linea_credito_id);
        return $lineaDeCredito;
    }

    public function precarga() {
        return $this->belongsTo('App\Models\Precarga');
    }

    public function grilla() {
        return $this->belongsTo('App\Models\Grilla');
    }

    public function debito() {
        return $this->belongsTo('App\Models\Debito');
    }

    public function fondista() {
        return $this->belongsTo('App\Models\Fondista');
    }

    public function formularios($tipoRelacionId) {
        $listado = PropuestaFormulario::where('propuesta_id', $this->id)
            ->where('tipo_relacion_id', $tipoRelacionId)
            ->get();
        return $listado;
    }

}
