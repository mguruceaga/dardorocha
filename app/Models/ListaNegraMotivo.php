<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListaNegraMotivo extends Model
{
    protected $table = 'lista_negra_motivos';
    protected $guarded = ['id'];
}
