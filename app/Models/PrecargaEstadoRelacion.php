<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrecargaEstadoRelacion extends Model
{
    use HasFactory;

    protected $table = 'precarga_estado';
    protected $guarded = ['id'];

}
