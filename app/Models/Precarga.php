<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Precarga extends Model
{
    use HasFactory;

    protected $table = 'precargas';
    protected $guarded = ['id'];
    protected $attributes = [
        'documento_tipo_id' => 0,
        'numero_doc' => '',
        'cuil_cuit' => '',
        'nombre' => '',
        'apellido' => '',
        'fecha_nacimiento' => null,
        'genero_id' => 0,

        'sector_id' => 1,
        'grupo_pago_id' => 0,
        'zona_id' => 0,
        'reparticion_id' => 0,
        'dependencia' => '',

        'cuota_maxima' => '',
        'plazo_maximo' => '',
        'monto_maximo' => '',

        'contratacion_tipo_id' => 0,
        'ingreso_laboral' => null,
        'antiguedad' => null,
        'banco_id' => 0,
        'cbu' => '',

        'email' => '',
        'telefono_movil' => '',
        'telefono_fijo' => '',
        'calle' => '',
        'numero' => '',
        'piso' => '',
        'departamento' => '',
        'codigo_postal_id' => 0,

        'precarga_estado_id' => null,

        'propuesta_multiple' => 0,

        'created_at' => '',
        'updated_at' => '',
    ];

    public function documentoTipo() {
        return $this->belongsTo('App\Models\DocumentoTipo');
    }

    public function reparticion() {
        return $this->belongsTo('App\Models\Reparticion');
    }

    public function contratacionTipo() {
        return $this->belongsTo('App\Models\ContratacionTipo');
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function userAsignado() {
        return $this->belongsTo('App\Models\User', 'user_asignado_id');
    }

    public function precargaEstadoActual() {
        return $this->belongsToMany('App\Models\PrecargaEstado', 'precarga_estado')
            ->as('pivot')
            ->withTimestamps()
            ->orderBy('precarga_estado.id', 'DESC')
            ->first();
    }

    public function precargaEstados() {
        return $this->belongsToMany('App\Models\PrecargaEstado', 'precarga_estado');
    }

    public function mensajes() {
        return $this->hasMany('App\Models\Mensaje')
            ->where('mensaje_id', 0)
            ->orderBy('created_at', 'DESC');
    }

    public function mensajesNoLeidos($perfil) {
        return Mensaje::where('leido_'.$perfil, 0)
            ->where('precarga_id', $this->id)
            ->get()
            ->count();
    }

    public function marcarMensajesComoLeidos($perfil) {
        return Mensaje::where('precarga_id', $this->id)
            ->update(['leido_'.$perfil => true]);
    }

    public function getCreatedAtAttribute($value) {
        return Carbon::createFromDate($value)->format('d-m-Y');
    }

    public function documentos() {
        $documentos = DocumentacionAdjunta::where('precarga_id', $this->id)->get();
        return $documentos;
    }

}
