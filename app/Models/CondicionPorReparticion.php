<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CondicionPorReparticion extends Model
{
    use HasFactory;
    
    protected $table = 'condicion_por_reparticiones';
    protected $guarded = ['id'];
}
