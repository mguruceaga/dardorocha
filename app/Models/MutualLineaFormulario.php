<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MutualLineaFormulario extends Model
{
    
    protected $table = 'mutual_linea_formularios';
    protected $guarded = ['id'];
}
