<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReparticionZonaGrupo extends Model
{
    use HasFactory;

    protected $table = 'reparticion_zona_grupos';
    protected $guarded = ['id'];

}
