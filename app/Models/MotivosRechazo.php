<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MotivosRechazo extends Model
{
    use HasFactory;
    protected $table = 'MotivosRechazo';
    protected $guarded = ['id'];
}
