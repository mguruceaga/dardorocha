<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GrupoPago extends Model
{
    use HasFactory;

    protected $table = 'grupos_pagos';
    protected $guarded = ['id'];
    
    public function zonas(){
        return $this->belongsToMany(Zona::class)->withTimestamps();
    }
}
