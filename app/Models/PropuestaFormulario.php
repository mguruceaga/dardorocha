<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropuestaFormulario extends Model
{
    use HasFactory;

    protected $table = 'propuesta_formularios';
    protected $guarded = ['id'];

    public function propuesta() {
        return $this->belongsTo('App\Models\Propuesta');
    }

    public function formulario() {
        return $this->belongsTo('App\Models\Formulario');
    }

    public function archivo() {
        return $this->belongsTo('App\Models\Archivo');
    }

}
