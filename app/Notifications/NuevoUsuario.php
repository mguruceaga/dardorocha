<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Crypt;

class NuevoUsuario extends Notification
{
    use Queueable;
    public $password = '';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($password)
    {
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('login');
        return (new MailMessage)
                    ->line('Te damos la bienvenida a DardoRocha.')
                    ->line('Este correo electrónico es para darte acceso a nuestro sistema de gestión. Para hacerlo tenés que hacer click en el siguiente enlace, ingresar con tu correo electrónico y la siguiente contraseña: ')
                    ->line($this->password)
                    ->action('Acceder a Dardo Rocha', $url)
                    ->line('¡Gracias por usar nuestro sistema!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
