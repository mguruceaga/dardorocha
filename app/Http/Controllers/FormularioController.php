<?php

namespace App\Http\Controllers;

use App\Models\Fondista;
use App\Models\Formulario;
use App\Models\MutualLinea;
use App\Models\Reparticion;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class FormularioController extends Controller
{

    public function test($lineaId, $reparticionId, $fondistaId) {
        $linea = MutualLinea::find($lineaId);
        if (is_null($linea))
            return '<p>Debe elegir una línea</p>';
        else
            echo '<h1>Línea: ' . $linea->nombre . '</h1>';

        $reparticion = Reparticion::find($reparticionId);
        if (!is_null($reparticion))
            echo '<h2>Repartición: ' . $reparticion->nombre . '</h2>';

        $fondista = Fondista::find($fondistaId);
        if (!is_null($fondista))
            echo '<h3>Fondista: ' . $fondista->nombre . '</h3>';

        echo '<p>Formularios Obligatorios</p>';

        $resultado = DB::table('mutual_lineas')
            ->join('mutual_linea_formularios', 'mutual_linea_formularios.mutual_linea_id', 'mutual_lineas.id')
            ->join('tipo_relaciones', 'tipo_relaciones.id', 'mutual_linea_formularios.tipo_relacion_id')
            ->join('formularios', 'formularios.id', 'mutual_linea_formularios.formulario_id')
            ->join('archivos', 'archivos.formulario_id', 'formularios.id')
            ->where(function($result) use ($fondista) {
                return $result->where('archivos.fondista_id', $fondista->id)
                    ->orWhereNull('archivos.fondista_id');
            })
            ->where('mutual_lineas.id', $lineaId)
            ->where('mutual_linea_formularios.tipo_relacion_id', 1)
            ->select('formularios.id', 'formularios.nombre',
                'archivos.nombre as nombreArchivo',
                'tipo_relaciones.nombre as nombreTipoRelacion')
            ->get();

        echo '<ul>';
        foreach($resultado as $form) {
            echo '<li>' . $form->id . ' - ' . $form->nombre;
            echo '<ul><li>' . $form->nombreArchivo . ' - ' . $form->nombreTipoRelacion . '</li></ul>';
            echo '</li>';
        }
        echo '</ul>';

        echo '<p>Formularios Opcionales</p>';

        $resultado = DB::table('mutual_lineas')
            ->join('mutual_linea_formularios', 'mutual_linea_formularios.mutual_linea_id', 'mutual_lineas.id')
            ->join('tipo_relaciones', 'tipo_relaciones.id', 'mutual_linea_formularios.tipo_relacion_id')
            ->join('formularios', 'formularios.id', 'mutual_linea_formularios.formulario_id')
            ->join('archivos', 'archivos.formulario_id', 'formularios.id')
            ->where('mutual_lineas.id', $lineaId)
            ->where('mutual_linea_formularios.tipo_relacion_id', 2)
            ->where(function($result) use ($fondista) {
                return $result->where('archivos.fondista_id', $fondista->id)
                    ->orWhereNull('archivos.fondista_id');
            })
            ->select('formularios.id', 'formularios.nombre',
                'archivos.nombre as nombreArchivo',
                'tipo_relaciones.nombre as nombreTipoRelacion')
            ->get();

        echo '<ul>';
        foreach($resultado as $form) {
            echo '<li>' . $form->id . ' - ' . $form->nombre;
            echo '<ul><li>' . $form->nombreArchivo . ' - ' . $form->nombreTipoRelacion . '</li></ul>';
            echo '</li>';
        }
        echo '</ul>';

        echo '<p>Formularios Extras</p>';

        $resultado = DB::table('mutual_lineas')
            ->join('mutual_linea_formularios', 'mutual_linea_formularios.mutual_linea_id', 'mutual_lineas.id')
            ->join('tipo_relaciones', 'tipo_relaciones.id', 'mutual_linea_formularios.tipo_relacion_id')
            ->join('formularios', 'formularios.id', 'mutual_linea_formularios.formulario_id')
            ->join('archivos', 'archivos.formulario_id', 'formularios.id')
            ->where('mutual_lineas.id', $lineaId)
            ->where('mutual_linea_formularios.tipo_relacion_id', 3)
            ->where('archivos.reparticion_id', $reparticion->id)
            ->select('formularios.id', 'formularios.nombre',
                'archivos.nombre as nombreArchivo',
                'tipo_relaciones.nombre as nombreTipoRelacion')
            ->get();

        echo '<ul>';
        foreach($resultado as $form) {
            echo '<li>' . $form->id . ' - ' . $form->nombre;
            echo '<ul><li>' . $form->nombreArchivo . ' - ' . $form->nombreTipoRelacion . '</li></ul>';
            echo '</li>';
        }
        echo '</ul>';

    }

    public function procesarFormulario() {

        $pdf = \PDF::loadView('forms.form')->save(storage_path('app/legajos/DR.pdf'));
        return true;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Formulario  $formulario
     * @return \Illuminate\Http\Response
     */
    public function show(Formulario $formulario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Formulario  $formulario
     * @return \Illuminate\Http\Response
     */
    public function edit(Formulario $formulario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Formulario  $formulario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Formulario $formulario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Formulario  $formulario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Formulario $formulario)
    {
        //
    }
}
