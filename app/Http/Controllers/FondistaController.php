<?php

namespace App\Http\Controllers;

class FondistaController extends Controller
{
    public function index() {
        return view('fondistas.index');
    }

    public function create() {
        return view('fondistas.edit');
    }

    public function edit($fondistaId) {
        return view('fondistas.edit', ['fondistaId' => $fondistaId]);
    }
}
