<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReparticionController extends Controller
{
    public function index() {
        return view('reparticiones.index');
    }

    public function create() {
        return view('reparticiones.edit');
    }

    public function edit($reparticionId) {
        return view('reparticiones.edit', ['reparticionId' => $reparticionId]);
    }
}
