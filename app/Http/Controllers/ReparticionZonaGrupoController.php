<?php

namespace App\Http\Controllers;

use App\Models\ReparticionZonaGrupo;
use Illuminate\Http\Request;

class ReparticionZonaGrupoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ReparticionZonaGrupo  $reparticionZonaGrupo
     * @return \Illuminate\Http\Response
     */
    public function show(ReparticionZonaGrupo $reparticionZonaGrupo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ReparticionZonaGrupo  $reparticionZonaGrupo
     * @return \Illuminate\Http\Response
     */
    public function edit(ReparticionZonaGrupo $reparticionZonaGrupo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ReparticionZonaGrupo  $reparticionZonaGrupo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReparticionZonaGrupo $reparticionZonaGrupo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReparticionZonaGrupo  $reparticionZonaGrupo
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReparticionZonaGrupo $reparticionZonaGrupo)
    {
        //
    }
}
