<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUsuarioRequest;
use App\Http\Requests\UpdateUsuarioRequest;
use App\Models\Usuario;

class UsuarioController extends Controller
{
    public function index() {
        return view('usuarios.index');
    }

    public function create() {
        return view('usuarios.edit');
    }

    public function edit($usuarioId) {
        return view('usuarios.edit', ['usuarioId' => $usuarioId]);
    }

    public function new($token) {
        return view('usuarios.new', ['token' => $token]);
    }
}
