<?php

namespace App\Http\Controllers;

use App\Models\PrecargaEstado;
use Illuminate\Http\Request;

class PrecargaEstadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PrecargaEstado  $precargaEstado
     * @return \Illuminate\Http\Response
     */
    public function show(PrecargaEstado $precargaEstado)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PrecargaEstado  $precargaEstado
     * @return \Illuminate\Http\Response
     */
    public function edit(PrecargaEstado $precargaEstado)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PrecargaEstado  $precargaEstado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PrecargaEstado $precargaEstado)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PrecargaEstado  $precargaEstado
     * @return \Illuminate\Http\Response
     */
    public function destroy(PrecargaEstado $precargaEstado)
    {
        //
    }
}
