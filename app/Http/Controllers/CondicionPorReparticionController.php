<?php

namespace App\Http\Controllers;

use App\Models\CondicionPorReparticion;
use Illuminate\Http\Request;

class CondicionPorReparticionController extends Controller
{
    public function index() {
        return view('condiciones.index');
    }

    public function create() {
        return view('condiciones.edit');
    }

    public function edit($condicionId) {
        return view('condiciones.edit', ['condicionId' => $condicionId]);
    }

}
