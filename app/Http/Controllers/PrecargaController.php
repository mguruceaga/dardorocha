<?php

namespace App\Http\Controllers;

use App\Managers\PrecargaManager;
use App\Models\Condicion;
use App\Models\MutualLinea;
use App\Models\MutualLineaFormulario;
use App\Models\MutualLineaGrilla;
use App\Models\Precarga;
use App\Models\Propuesta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PrecargaController extends Controller
{
    public function index() {
        return view('precargas.index');
    }

    public function create() {
        return view('precargas.edit', ['tab' => 'precarga']);
    }

    public function edit($precargaId, $tab = 'precarga') {
        $precargaId = (new \App\Tools\HelpersTool)->helper_desencriptar($precargaId, 'encrypt_simple');
        $precarga = (new PrecargaManager)->getItem($precargaId);
        return view('precargas.edit', ['precarga' => $precarga, 'tab' => $tab]);
    }

    public function formularios($precargaId) {
        $precarga = Precarga::find($precargaId);
        $propuesta = Propuesta::where('precarga_id', $precarga->id)
            ->where('aceptada', 1)
            ->first();
        $condicion = DB::table('condiciones')
            ->join('condicion_por_reparticiones', 'condicion_por_reparticiones.condicion_id', 'condiciones.id')
            ->where('condicion_por_reparticiones.reparticion_id', $precarga->reparticion_id)
            ->where('condicion_por_reparticiones.zona_id', $precarga->zona_id)
            ->where('condicion_por_reparticiones.grupo_pago_id', $precarga->grupo_pago_id)
            ->select('condiciones.*')
            ->first();
        $mutualLineaGrilla = MutualLineaGrilla::where('grilla_id', $propuesta->grilla_id)
            ->first();
        $formularios = DB::table('mutual_linea_formularios')
            ->join('formularios', 'formularios.id', 'mutual_linea_formularios.formulario_id')
            ->join('archivos', 'archivos.formulario_id', 'formularios.id')
            ->where('grilla_id', $propuesta->grilla_id)
            ->where('mutual_linea_id', $mutualLineaGrilla->mutual_linea_id)
            ->where('condicion_id', $condicion->id)
            ->select('formularios.nombre as form', 'archivos.*')
            ->get();
    }

}
