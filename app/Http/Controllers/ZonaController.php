<?php

namespace App\Http\Controllers;

use App\Models\Zona;
use Illuminate\Http\Request;

class ZonaController extends Controller
{
    public function index() {
        return view('zonas.index');
    }

    public function create() {
        return view('zonas.edit');
    }

    public function edit($zonaId) {
        return view('zonas.edit', ['zonaId' => $zonaId]);
    }
}
