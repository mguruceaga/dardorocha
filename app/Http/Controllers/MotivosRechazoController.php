<?php

namespace App\Http\Controllers;

use App\Models\MotivosRechazo;
use Illuminate\Http\Request;

class MotivosRechazoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MotivosRechazo  $motivosRechazo
     * @return \Illuminate\Http\Response
     */
    public function show(MotivosRechazo $motivosRechazo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MotivosRechazo  $motivosRechazo
     * @return \Illuminate\Http\Response
     */
    public function edit(MotivosRechazo $motivosRechazo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MotivosRechazo  $motivosRechazo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MotivosRechazo $motivosRechazo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MotivosRechazo  $motivosRechazo
     * @return \Illuminate\Http\Response
     */
    public function destroy(MotivosRechazo $motivosRechazo)
    {
        //
    }
}
