<?php

namespace App\Http\Controllers;

use App\Models\ContratacionTipo;
use Illuminate\Http\Request;

class ContratacionTipoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ContratacionTipo  $contratacionTipo
     * @return \Illuminate\Http\Response
     */
    public function show(ContratacionTipo $contratacionTipo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ContratacionTipo  $contratacionTipo
     * @return \Illuminate\Http\Response
     */
    public function edit(ContratacionTipo $contratacionTipo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ContratacionTipo  $contratacionTipo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContratacionTipo $contratacionTipo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ContratacionTipo  $contratacionTipo
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContratacionTipo $contratacionTipo)
    {
        //
    }
}
