<?php

namespace App\Http\Controllers;

class GrillaController extends Controller
{
    public function index() {
        return view('grillas.index');
    }

    public function create() {
        return view('grillas.edit');
    }

    public function edit($grillaId) {
        return view('grillas.edit', ['grillaId' => $grillaId]);
    }
}
