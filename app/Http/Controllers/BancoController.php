<?php

namespace App\Http\Controllers;

use App\Models\Banco;
use Illuminate\Http\Request;

class BancoController extends Controller
{
    public function index() {
        return view('bancos.index');
    }

    public function create() {
        return view('bancos.edit');
    }

    public function edit($bancoId) {
        return view('bancos.edit', ['bancoId' => $bancoId]);
    }
}
