<?php

namespace App\Http\Controllers;

use App\Models\Archivo;
use Illuminate\Http\Request;

class ArchivoController extends Controller
{
    public function index() {
        return view('archivos.index');
    }

    public function create() {
        return view('archivos.edit');
    }

    public function edit($archivoId) {
        return view('archivos.edit', ['archivoId' => $archivoId]);
    }

}
