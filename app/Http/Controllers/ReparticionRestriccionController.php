<?php

namespace App\Http\Controllers;

use App\Models\ReparticionRestriccion;
use Illuminate\Http\Request;

class ReparticionRestriccionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ReparticionRestriccion  $reparticionRestriccion
     * @return \Illuminate\Http\Response
     */
    public function show(ReparticionRestriccion $reparticionRestriccion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ReparticionRestriccion  $reparticionRestriccion
     * @return \Illuminate\Http\Response
     */
    public function edit(ReparticionRestriccion $reparticionRestriccion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ReparticionRestriccion  $reparticionRestriccion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReparticionRestriccion $reparticionRestriccion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReparticionRestriccion  $reparticionRestriccion
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReparticionRestriccion $reparticionRestriccion)
    {
        //
    }
}
