<?php

namespace App\Http\Controllers;

class ListaNegraController extends Controller
{
    public function index() {
        return view('listaNegra.index');
    }

    public function create() {
        return view('listaNegra.edit');
    }

    public function edit($listaNegraId) {
        return view('listaNegra.edit', ['listaNegraId' => $listaNegraId]);
    }

}
