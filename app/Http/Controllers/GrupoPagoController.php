<?php

namespace App\Http\Controllers;

use App\Models\GrupoPago;
use Illuminate\Http\Request;

class GrupoPagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GrupoPago  $grupoPago
     * @return \Illuminate\Http\Response
     */
    public function show(GrupoPago $grupoPago)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GrupoPago  $grupoPago
     * @return \Illuminate\Http\Response
     */
    public function edit(GrupoPago $grupoPago)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GrupoPago  $grupoPago
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GrupoPago $grupoPago)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GrupoPago  $grupoPago
     * @return \Illuminate\Http\Response
     */
    public function destroy(GrupoPago $grupoPago)
    {
        //
    }
}
