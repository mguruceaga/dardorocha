<?php

namespace App\Http\Controllers;

use App\Models\ProvinciaPorGrupoPago;
use Illuminate\Http\Request;

class ProvinciaPorGrupoPagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProvinciaPorGrupoPago  $provinciaPorGrupoPago
     * @return \Illuminate\Http\Response
     */
    public function show(ProvinciaPorGrupoPago $provinciaPorGrupoPago)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProvinciaPorGrupoPago  $provinciaPorGrupoPago
     * @return \Illuminate\Http\Response
     */
    public function edit(ProvinciaPorGrupoPago $provinciaPorGrupoPago)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProvinciaPorGrupoPago  $provinciaPorGrupoPago
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProvinciaPorGrupoPago $provinciaPorGrupoPago)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProvinciaPorGrupoPago  $provinciaPorGrupoPago
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProvinciaPorGrupoPago $provinciaPorGrupoPago)
    {
        //
    }
}
