<?php

namespace App\Http\Controllers;

use App\Models\MutualLineaFormulario;
use Illuminate\Http\Request;

class MutualLineaFormularioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MutualLineaFormulario  $mutualLineaFormulario
     * @return \Illuminate\Http\Response
     */
    public function show(MutualLineaFormulario $mutualLineaFormulario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MutualLineaFormulario  $mutualLineaFormulario
     * @return \Illuminate\Http\Response
     */
    public function edit(MutualLineaFormulario $mutualLineaFormulario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MutualLineaFormulario  $mutualLineaFormulario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MutualLineaFormulario $mutualLineaFormulario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MutualLineaFormulario  $mutualLineaFormulario
     * @return \Illuminate\Http\Response
     */
    public function destroy(MutualLineaFormulario $mutualLineaFormulario)
    {
        //
    }
}
