<?php

namespace App\Http\Controllers;

use App\Models\MutualLineaGrilla;
use Illuminate\Http\Request;

class MutualLineaGrillaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MutualLineaGrilla  $mutualLineaGrilla
     * @return \Illuminate\Http\Response
     */
    public function show(MutualLineaGrilla $mutualLineaGrilla)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MutualLineaGrilla  $mutualLineaGrilla
     * @return \Illuminate\Http\Response
     */
    public function edit(MutualLineaGrilla $mutualLineaGrilla)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MutualLineaGrilla  $mutualLineaGrilla
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MutualLineaGrilla $mutualLineaGrilla)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MutualLineaGrilla  $mutualLineaGrilla
     * @return \Illuminate\Http\Response
     */
    public function destroy(MutualLineaGrilla $mutualLineaGrilla)
    {
        //
    }
}
