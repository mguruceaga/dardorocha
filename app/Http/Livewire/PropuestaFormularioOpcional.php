<?php

namespace App\Http\Livewire;

use Livewire\Component;

class PropuestaFormularioOpcional extends Component {

    public $propuesta;
    public $formulario;

    protected $rules = [
        'formulario.activo' => 'nullable',
    ];

    protected $messages = [
        'formulario.activo.nullable' => '',
    ];

    public function mount($propuesta, $formulario) {
        $this->propuesta = $propuesta;
        $this->formulario = $formulario;
    }

    public function updatedFormularioActivo($value) {
        $this->formulario->activo = $value;
        $this->formulario->save();
    }

    public function render()
    {
        return view('livewire.propuesta-formulario-opcional');
    }
}
