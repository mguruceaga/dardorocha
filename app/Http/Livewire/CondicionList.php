<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Condicion;
use App\Managers\CondicionManager;

use Livewire\WithPagination;

class CondicionList extends Component
{
    use WithPagination;

    public $nombre = '';
    public $ordenarPor = 'nombre';
    public $sentido = 'ASC';

    protected $queryString = ['ordenarPor', 'sentido'];

    public function sortBy($field) {
        $this->sentido = $this->ordenarPor === $field
            ? $this->sentido === 'ASC' ? 'DESC' : 'ASC'
            : 'ASC';
    }
    
    public function delete($condicionId){
        (new CondicionManager)->delete($condicionId);
    }
    
    public function updatingNombre()
    {
        $this->resetPage();
        $this->gotoPage(1);
    }

    public function cleanSort() {
        $this->nombre = '';
    }

    public function render()
    {
        return view('livewire.condicion-list', [
            'condiciones' => Condicion::where('nombre', 'like', '%'.$this->nombre.'%')
                ->orderBy($this->ordenarPor, $this->sentido)
                ->paginate(10),

        ]);
    }
}
