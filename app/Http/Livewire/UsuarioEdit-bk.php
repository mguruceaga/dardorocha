<?php

namespace App\Http\Livewire;

use App\Managers\UsuarioManager;
use App\Models\Usuario;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;

class UsuarioEdit extends Component
{

    use HasRoles;

    public $usuarioId = 0;
    public $usuario = '';
    public $name = '';
    public $email = '';
    public $usuarioRol = 0;
    public $roles = [];

    protected $rules = [
        'usuario.name' => 'required',
        'usuario.email' => 'required|unique',
    ];

    protected $messages = [
        'usuario.name.required' => 'El nombre del Usuario es requerido.',
        'usuario.email.required' => 'El correo electrónico del Usuario es requerido.',
        'usuario.email.unique' => 'Ya existe un usuario con este correo electrónico.',
    ];

    /**
     * Funciones relacionadas al Front
     */
    public function mount($usuarioId = 0)
    {
        if ($usuarioId != 0) {
            $this->usuarioId = $usuarioId;
            $this->edit($usuarioId);
            $this->usuarios = (new UsuarioManager)->getItems('name');
        }else{
            $this->create();
        }
        $this->roles = Role::orderBy('name')->get()->pluck('name', 'id')->toArray();
    }

    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function updatedElemNombre() {
        $this->validateOnly('usuario.name');
    }

    /**
     * Funciones relacionadas con ABM
     */
    public function create() {
        $this->usuario = new Usuario();
    }

    public function edit($usuarioId) {
        $this->usuario = (new UsuarioManager)->getItem($usuarioId);
        $this->name = $this->usuario->name;
        $this->email = $this->usuario->email;
    }

    public function save()
    {
        //$this->validate();
        $this->validate([
            'usuario.email' => [
                'required',
                Rule::unique('users','email')->ignore($this->usuario->id),
            ],
        ]);
        (new UsuarioManager)->saveItem($this->usuario);
        return redirect()->to('/usuarios');
    }

    public function render()
    {
        return view('livewire.usuario-edit');
    }

}
