<?php

namespace App\Http\Livewire;

use App\Managers\ReparticionManager;
use App\Managers\ArchivoManager;
use App\Managers\FondistaManager;
use App\Managers\FormularioManager;
use App\Models\Archivo;
use Illuminate\Validation\Rule;
use Livewire\Component;

class ArchivoEdit extends Component
{
    public $archivo = '';
    public $nombre = '';
    public $formularios = [];
    public $fondistas = [];
    public $reparticiones = [];
    public $archivos = [];

    protected $rules = [
        'archivo.nombre' => 'required',
        'archivo.formulario_id' => 'required',
        'archivo.fondista_id' => 'required|gt:0',
        'archivo.reparticion_id' => 'required|gt:0',
    ];

    protected $messages = [
        'archivo.nombre.required' => 'El nombre del archivo es requerido.',
        'archivo.nombre.unique' => 'Ya existe un Archivo con este nombre',
        'archivo.formulario_id.required' => 'El formulario es requerido.',
        'archivo.fondista_id.required' => 'El fondista es requerido.',
        'archivo.reparticion_id.gt' => 'La repartición es requerida.',
    ];

    public function mount($archivoId = 0)
    {
        if ($archivoId != 0) {
            $this->edit($archivoId);
        }else{
            $this->create();
        }

        $this->formularios = (new FormularioManager)->getItems('nombre')->pluck('nombre', 'id')->toArray();
        $this->fondistas = (new FondistaManager)->getItems('nombre')->pluck('nombre', 'id')->toArray();
        $this->reparticiones = (new ReparticionManager)->getItems('nombre')->pluck('nombre', 'id')->toArray();
    }

    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }


    public function updatedElemNombre() {
        $this->validateOnly('archivo.nombre');
    }

    public function save()
    {
        $this->validate([
            'archivo.nombre' => [
                'required',
                Rule::unique('archivos','nombre')->ignore($this->archivo->id),
            ],
        ]);

        (new ArchivoManager)->saveItem($this->archivo);

        return redirect()->to('/archivos');
        // $this->emit('notify-saved');

    }

    public function render()
    {
        return view('livewire.archivo-edit');
    }

    /**
     * Funciones relacionadas con ABM
     */
    public function create(){
        $this->archivo = new Archivo();
    }
    public function edit($archivoId){
        $this->archivo = (new ArchivoManager)->getItem($archivoId);
        $this->nombre = $this->archivo->nombre;
    }

}
