<?php

namespace App\Http\Livewire;

use App\Managers\BancoManager;
use App\Models\Banco;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Illuminate\Support\Facades\Validator;

class BancoEdit extends Component
{

    public $banco = '';
    public $codigo = '';
    public $nombre = '';
    public $bancos = [];

    protected $rules = [
        'banco.codigo' => 'required',
        'banco.nombre' => 'required',
    ];

    protected $messages = [
        'banco.codigo.required' => 'El codigo es requerido.',
        'banco.nombre.required' => 'El nombre es requerido.',
        'banco.nombre.unique' => 'Ya existe un banco con este nombre',
    ];

    /**
     * Funciones relacionadas al Front
     */
    public function mount($bancoId = 0)
    {
        if ($bancoId != 0) {
            $this->edit($bancoId);
        }else{
            $this->create();
        }

    }

    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function updatedBancoNombre() {
        $this->validateOnly('banco.nombre');
    }

    public function render()
    {
        return view('livewire.banco-edit');
    }

    /**
     * Funciones relacionadas con ABM
     */
    public function create() {
        $this->banco = new Banco();
    }

    public function edit($bancoId) {
        $this->banco = (new BancoManager)->getItem($bancoId);
        $this->codigo = $this->banco->codigo;
        $this->nombre = $this->banco->nombre;
    }

    public function save()
    {
        //$this->validate();
        $this->validate([
            'banco.codigo' => [
                'required',
                Rule::unique('bancos','codigo')->ignore($this->banco->id),
            ],
            'banco.nombre' => [
                'required',
                Rule::unique('bancos','nombre')->ignore($this->banco->id),
            ],
        ]);

        (new BancoManager)->saveItem($this->banco);

        return redirect()->to('/bancos');
        // $this->emit('notify-saved');

    }
}