<?php

namespace App\Http\Livewire;

use App\Managers\PrecargaEstadoManager;
use App\Models\Documentacion;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;

class PrecargaDocumentoAdjunto extends Component
{
    public $precarga = null;
    public $documento = '';
    public $mimetype = '';
    public $muestroPreview = 0;
    public $documentaciones = '';
    public $verDocumento = '';
    public $estadoActualSlug = '';
    public $documentacionId = 0;

    protected $rules = [
        'documento.documentacion_id' => 'required|gt:0',
    ];

    protected $messages = [
        'documento.documentacion_id.required' => 'Es requerida.',
    ];

    public function mount($precarga, $documento, $estadoActualSlug)
    {
        $this->precarga = $precarga;
        $this->estadoActualSlug = $estadoActualSlug;
        $this->documento = $documento;
        $this->documentacionId = $documento->documentacion_id;
        $this->mimetype = Storage::disk('documents')->mimeType($documento->path);
        info($this->mimetype);
        switch($this->mimetype) {
            case 'application/pdf':
            case 'image/png':
            case 'image/jpeg':
                $this->muestroPreview = true;
                break;
        }
        $this->documentaciones = Documentacion::orderBy('nombre')->get()->pluck('nombre', 'id')->toArray();
    }

    public function render()
    {
        return view('livewire.precarga-documento-adjunto');
    }

    public function asociateDocumentacion()
    {
        try {
            $roles = auth()->user()->getRoleNames();
            $estadoActual = $this->precarga->precargaEstadoActual();
            if (is_null($estadoActual)) {
                $estadoActual = (new PrecargaEstadoManager)->getBySlug('iniciada');
            }
            $documentacionId = $this->documentacionId != 0 ? $this->documentacionId : null;
            switch ($roles[0]) {
                case config('tools.constants.roles.administrador'):
                    switch ($estadoActual->slug) {
                        case 'iniciada':
                        case 'observada':
                        case 'preaprobada':
                        case 'aceptada':
                            $this->documento->documentacion_id = $documentacionId;
                            $this->documento->save();
                            break;
                    }
                    break;
                case config('tools.constants.roles.tesorero'):
                case config('tools.constants.roles.empleado'):
                    break;
                case config('tools.constants.roles.comercial'):
                    switch ($estadoActual->slug) {
                        case 'iniciada':
                        case 'observada':
                        case 'preaprobada':
                            $this->documento->documentacion_id = $documentacionId;
                            $this->documento->save();
                            break;
                    }
                    break;
            }
        } catch (\Exception $e) {
            //dd($e);
        }
    }

    public function deleteDocumentacion()
    {
        $roles = auth()->user()->getRoleNames();
        $estadoActual = $this->precarga->precargaEstadoActual();
        if (is_null($estadoActual)) {
            $estadoActual = (new PrecargaEstadoManager)->getBySlug('iniciada');
        }
        switch ($roles[0]) {
            case config('tools.constants.roles.administrador'):
                switch ($estadoActual->slug) {
                    case 'iniciada':
                    case 'observada':
                    case 'preaprobada':
                    case 'aceptada':
                        $documentoId = $this->documento->id;
                        $precargaId = $this->documento->precarga_id;
                        Storage::disk('documents')->delete($this->documento->path);
                        $this->documento->delete();
                        return redirect()->to('/precargas/editar/' . (new \App\Tools\HelpersTool)->helper_getUrlHash($precargaId) . '/documentos');
                        break;
                }
                break;
            case config('tools.constants.roles.tesorero'):
            case config('tools.constants.roles.empleado'):
                break;
            case config('tools.constants.roles.comercial'):
                switch ($estadoActual->slug) {
                    case 'iniciada':
                    case 'observada':
                    case 'preaprobada':
                        $documentoId = $this->documento->id;
                        $precargaId = $this->documento->precarga_id;
                        Storage::disk('documents')->delete($this->documento->path);
                        $this->documento->delete();
                        return redirect()->to('/precargas/editar/' . (new \App\Tools\HelpersTool)->helper_getUrlHash($precargaId) . '/documentos');
                        break;
                }
                break;
        }

    }

}
