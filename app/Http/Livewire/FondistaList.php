<?php

namespace App\Http\Livewire;

use App\Models\Fondista;
use Livewire\Component;
use Livewire\WithPagination;

class FondistaList extends Component
{
    use WithPagination;

    public $nombre = '';
    public $abreviatura = '';
    public $ordenarPor = 'nombre';
    public $sentido = 'ASC';

    protected $queryString = ['ordenarPor', 'sentido'];

    public function sortBy($field) {
        $this->sentido = $this->ordenarPor === $field
            ? $this->sentido === 'ASC' ? 'DESC' : 'ASC'
            : 'ASC';
    }

    public function updatingNombre()
    {
        $this->setPage(1);
    }

    public function updatingAbreviatura()
    {
        $this->setPage(1);
    }

    public function cleanSort() {
        $this->nombre = '';
        $this->abreviatura = '';
    }

    public function render()
    {
        return view('livewire.fondista-list', [
            'fondistas' => Fondista::search('nombre', $this->nombre)
                ->search('abreviatura', $this->abreviatura)
                ->orderBy ($this->ordenarPor, $this->sentido)
                ->paginate(10)
        ]);
    }
}
