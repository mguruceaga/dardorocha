<?php

namespace App\Http\Livewire;

use App\Models\PropuestaFormulario;
use Livewire\Component;

class Propuesta extends Component
{
    public $precarga = '';
    public $propuesta = '';
    public $estadoActualSlug = '';
    public $gestionador = '';
    public $fondistas = [];

    protected $rules = [
        'propuesta.fondista_id' => 'nullable',
        'propuesta.aceptada' => 'nullable',
    ];

    protected $messages = [
        'propuesta.fondista_id.nullable' => '',
        'propuesta.aceptada.nullable' => '',
    ];

    public function mount($precarga, $propuesta, $estadoActualSlug, $gestionador, $fondistas) {
        $this->precarga = $precarga;
        $this->propuesta = $propuesta;
        $this->estadoActualSlug = $estadoActualSlug;
        $this->gestionador = $gestionador;
        $this->fondistas = $fondistas;
    }

    public function updatedPropuestaFondistaId($value) {
        if ($value == 0) {
            $this->propuesta->fondista_id = null;
        } else {
            $this->propuesta->fondista_id = $value;
        }
        $this->propuesta->save();
    }

    public function updatedPropuestaAceptada($value) {
        $this->propuesta->save();
        if (!$this->precarga->propuesta_multiple) {
            \App\Models\Propuesta::where('precarga_id', $this->precarga->id)
                ->where('id', '<>', $this->propuesta->id)
                ->update(['aceptada' => 0]);
        }
    }

    public function delete($id) {
        \App\Models\Propuesta::find($id)
            ->delete();
        $this->emit('deletePropuesta');
    }

    public function render()
    {
        return view('livewire.propuesta');
    }
}
