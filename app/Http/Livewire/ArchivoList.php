<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Archivo;
use App\Managers\ArchivoManager;

class ArchivoList extends Component
{

    use WithPagination;

    public $nombre = '';
    public $ordenarPor = 'nombre';
    public $sentido = 'ASC';

    protected $queryString = ['ordenarPor', 'sentido'];

    public function sortBy($field) {
        $this->sentido = $this->ordenarPor === $field
            ? $this->sentido === 'ASC' ? 'DESC' : 'ASC'
            : 'ASC';
    }

    public function delete($archivoId){
        (new ArchivoManager)->delete($archivoId);
    }

    public function updatingNombre()
    {
        $this->setPage(1);
    }

    public function cleanSort() {
        $this->nombre = '';
    }

    public function render()
    {
        return view('livewire.archivo-list', [
            'archivos' => Archivo::from('archivos')
                ->join('formularios', 'archivos.formulario_id', 'formularios.id')
                ->leftjoin('fondistas', 'archivos.fondista_id', 'fondistas.id')
                ->leftjoin('reparticiones', 'archivos.reparticion_id', 'reparticiones.id')
                ->search('archivos.nombre', $this->nombre)
                ->addSelect(['archivos.id','archivos.nombre as archivoNombre', 'formularios.nombre as formularioNombre', 'fondistas.nombre as fondistaNombre', 'reparticiones.nombre as reparticionNombre'])
                ->orderBy('archivoNombre', $this->sentido)
                ->paginate(10),
        ]);
    }
}
