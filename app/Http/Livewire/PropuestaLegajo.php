<?php

namespace App\Http\Livewire;

use App\Managers\DocumentacionManager;
use App\Managers\PrecargaManager;
use App\Managers\PropuestaManager;
use App\Models\Documentacion;
use App\Models\DocumentacionAdjunta;
use App\Models\Precarga;
use App\Models\PrecargaLegajoFormulario;
use App\Tools\FormatsTool;
use App\Tools\NumbersTool;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Livewire\Component;
use Barryvdh\DomPDF\PDF;
use Livewire\WithFileUploads;
use setasign\Fpdi\Fpdi;

class PropuestaLegajo extends Component
{

    use WithFileUploads;

    public $legajoUpload = [];
    public $precarga = '';
    public $propuesta = '';
    public $obligatorios = [];
    public $opcionales = [];
    public $extras = [];
    public $estadoActualSlug = '';
    public $gestionador = '';
    public $errorLegajo = false;
    public $mensajeErrorLegajo = '';
    public $seguroDeEliminar = false;
    public $error = 0;
    public $mensajeError = '';

    public function mount($precarga, $propuesta, $estadoActualSlug, $gestionador) {
        $this->precarga = $precarga;
        $this->propuesta = $propuesta;
        $this->obligatorios = $propuesta->formularios(1);
        $this->opcionales = $propuesta->formularios(2);
        $this->extras = $propuesta->formularios(3);
        $this->estadoActualSlug = $estadoActualSlug;
        $this->gestionador = $gestionador;
    }

    public function confirmarLegajo($propuestaId)
    {
        if (!Storage::disk('local')->exists('legajos'))
            Storage::disk('local')->makeDirectory('legajos');
        $numberTool = new NumbersTool();
        $propuesta = (new PropuestaManager)->getItem($propuestaId);
        PrecargaLegajoFormulario::where('propuesta_id', $propuesta->id)
            ->forceDelete();
        $precarga = Precarga::find($propuesta->precarga_id);
        if ($propuesta->aceptada && !is_null($propuesta->fondista_id)) {
            $i = 1;
            $pathPrecarga = $numberTool->completeWithZero($propuesta->precarga_id, 6);
            $pathPropuesta = $numberTool->completeWithZero($propuesta->id, 6);
            $pathBase = 'legajos/' . $pathPrecarga . '/' . $pathPropuesta;
            if (!Storage::disk('local')->exists('legajos/' . $pathPrecarga))
                Storage::disk('local')->makeDirectory('legajos/' . $pathPrecarga);
            if (Storage::disk('local')->exists('legajos/' . $pathPrecarga . '/' . $pathPropuesta))
                Storage::disk('local')->deleteDirectory('legajos/' . $pathPrecarga . '/' . $pathPropuesta);
            Storage::disk('local')->makeDirectory('legajos/' . $pathPrecarga . '/' . $pathPropuesta);
            for($f=1; $f<=3; $f++) {
                foreach ($propuesta->formularios($f) as $formulario) {
                    $nombreArchivo = $numberTool->completeWithZero($i, 2) . '0' . '_' . Str::slug($formulario->archivo->nombre) . '.pdf';
                    $pdf = \PDF::loadView('forms.form', compact('nombreArchivo'))->save(Storage::disk('local')->path($pathBase) . '/' . $nombreArchivo);
                    $legajoFormulario = new PrecargaLegajoFormulario();
                    $legajoFormulario->propuesta_id = $propuesta->id;
                    $legajoFormulario->propuesta_formulario_id = $formulario->id;
                    $legajoFormulario->nombre = $nombreArchivo;
                    $legajoFormulario->path = 'app/' . $pathBase . '/' . $nombreArchivo;
                    $legajoFormulario->save();
                    $copias = 0;
                    if ($formulario->copias_identicas > 0) {
                        for ($j = 1; $j <= $formulario->copias_identicas; $j++) {
                            $nombreArchivo = $numberTool->completeWithZero($i, 2) . $j . '_' . Str::slug($formulario->archivo->nombre) . '_copia.pdf';
                            $pdf = \PDF::loadView('forms.form', compact('nombreArchivo'))->save(Storage::disk('local')->path($pathBase) . '/' . $nombreArchivo);
                            $legajoFormulario = new PrecargaLegajoFormulario();
                            $legajoFormulario->propuesta_id = $propuesta->id;
                            $legajoFormulario->propuesta_formulario_id = $formulario->id;
                            $legajoFormulario->nombre = $nombreArchivo;
                            $legajoFormulario->path = 'app/' . $pathBase . '/' . $nombreArchivo;
                            $legajoFormulario->save();
                            $copias += 1;
                        }
                    }
                    if ($formulario->copias_blanco > 0) {
                        for ($h = 1; $h <= $formulario->copias_blanco; $h++) {
                            $nombreArchivo = $numberTool->completeWithZero($i, 2) . ($copias + $h) . '_' . Str::slug($formulario->archivo->nombre) . '_copiaenblanco.pdf';
                            $pdf = \PDF::loadView('forms.form', compact('nombreArchivo'))->save(Storage::disk('local')->path($pathBase) . '/' . $nombreArchivo);
                            $legajoFormulario = new PrecargaLegajoFormulario();
                            $legajoFormulario->propuesta_id = $propuesta->id;
                            $legajoFormulario->propuesta_formulario_id = $formulario->id;
                            $legajoFormulario->nombre = $nombreArchivo;
                            $legajoFormulario->path = 'app/' . $pathBase . '/' . $nombreArchivo;
                            $legajoFormulario->save();
                        }
                    }
                    $i += 1;
                }
            }
            $legajoFormularios = PrecargaLegajoFormulario::where('propuesta_id', $propuesta->id)
                ->get();
            $fpdi = new Fpdi();
            foreach ($legajoFormularios as $legajoFormulario) {
                $filename  = storage_path($legajoFormulario->path);
                $count = $fpdi->setSourceFile($filename);
                for ($i=1; $i<=$count; $i++) {
                    $template = $fpdi->importPage($i);
                    $size = $fpdi->getTemplateSize($template);
                    $fpdi->AddPage($size['orientation'], array($size['width'], $size['height']));
                    $fpdi->useTemplate($template);
                }
            }
            $pathLegajo = 'legajos/' . $pathPrecarga . '/' . $pathPropuesta . '/legajo_' . $pathPrecarga . '_' . $pathPropuesta . '.pdf';
            $pathLegajoStorage = Storage::disk('local')->path('legajos/' . $pathPrecarga . '/' . $pathPropuesta) . '/legajo_' . $pathPrecarga . '_' . $pathPropuesta . '.pdf';
            $fpdi->Output($pathLegajoStorage, 'F');

            $propuesta->monto = (new FormatsTool)->prepareToSave('decimal', $propuesta->monto);
            $propuesta->cuotas = (new FormatsTool)->prepareToSave('decimal', $propuesta->cuotas);
            $propuesta->valor_cuota = (new FormatsTool)->prepareToSave('decimal', $propuesta->valor_cuota);
            $propuesta->adicionales = (new FormatsTool)->prepareToSave('decimal', $propuesta->adicionales);
            $propuesta->valor_cuota_social = (new FormatsTool)->prepareToSave('decimal', $propuesta->valor_cuota_social);
            $propuesta->legajo_confirmado = true;
            $propuesta->path_documento = $pathLegajo;
            $propuesta->save();
        }
        return redirect()->to(url('/precargas/editar/'.(new \App\Tools\HelpersTool)->helper_getUrlHash($propuesta->precarga_id).'/legajo'));
    }

    public function descargarLegajo($propuestaId)
    {
        $propuesta = (new PropuestaManager)->getItem($propuestaId);
        return Storage::disk('local')->download($propuesta->path_documento);
    }

    public function descargarLegajoFirmado($propuestaId)
    {
        $propuesta = (new PropuestaManager)->getItem($propuestaId);
        return Storage::disk('local')->download($propuesta->path_documento_firmado);
    }

    public function deshacerLegajo($propuestaId)
    {
        $propuesta = (new PropuestaManager)->getItem($propuestaId);
        $precarga = Precarga::find($propuesta->precarga_id);
        $numberTool = new NumbersTool();
        $pathPrecarga = $numberTool->completeWithZero($propuesta->precarga_id, 6);
        $pathPropuesta = $numberTool->completeWithZero($propuesta->id, 6);
        Storage::disk('local')->deleteDirectory('legajos/' . $pathPrecarga . '/' . $pathPropuesta);

        $propuesta->monto = (new FormatsTool)->prepareToSave('decimal', $propuesta->monto);
        $propuesta->cuotas = (new FormatsTool)->prepareToSave('decimal', $propuesta->cuotas);
        $propuesta->valor_cuota = (new FormatsTool)->prepareToSave('decimal', $propuesta->valor_cuota);
        $propuesta->adicionales = (new FormatsTool)->prepareToSave('decimal', $propuesta->adicionales);
        $propuesta->valor_cuota_social = (new FormatsTool)->prepareToSave('decimal', $propuesta->valor_cuota_social);
        $propuesta->legajo_confirmado = false;
        $propuesta->path_documento = null;
        $propuesta->save();

        return redirect()->to(url('/precargas/editar/'.(new \App\Tools\HelpersTool)->helper_getUrlHash($precarga->id).'/legajo'));
    }

    public function guardarLegajoFirmado($propuestaId)
    {

        $this->errorLegajo = false;
        $this->mensajeErrorLegajo = '';
        info($this->legajoUpload);
        if (count($this->legajoUpload) == 0) {
            $this->errorLegajo = true;
            $this->mensajeErrorLegajo = 'Debe subir el legajo firmado correspondiente a esta propuesta.';
        } else {
            $numberTool = new NumbersTool();
            $propuesta = (new PropuestaManager)->getItem($propuestaId);
            $precarga = Precarga::find($propuesta->precarga_id);
            $pathPrecarga = $numberTool->completeWithZero($precarga->id, 6);
            $pathPropuesta = $numberTool->completeWithZero($propuesta->id, 6);
            $pathLegajo = 'legajos/' . $pathPrecarga . '/' . $pathPropuesta . '/legajo_' . $pathPrecarga . '_' . $pathPropuesta . '_firmado.pdf';

            Storage::disk('local')->delete($pathLegajo);
            Storage::disk('local')->move('livewire-tmp/' . $this->legajoUpload[0]->getFileName(), $pathLegajo);

            $propuesta->monto = (new FormatsTool)->prepareToSave('decimal', $propuesta->monto);
            $propuesta->cuotas = (new FormatsTool)->prepareToSave('decimal', $propuesta->cuotas);
            $propuesta->valor_cuota = (new FormatsTool)->prepareToSave('decimal', $propuesta->valor_cuota);
            $propuesta->adicionales = (new FormatsTool)->prepareToSave('decimal', $propuesta->adicionales);
            $propuesta->valor_cuota_social = (new FormatsTool)->prepareToSave('decimal', $propuesta->valor_cuota_social);
            $propuesta->legajo_firmado = true;
            $propuesta->path_documento_firmado = $pathLegajo;
            $propuesta->nombre_original_documento_firmado = $this->legajoUpload[0]->getClientOriginalName();
            $propuesta->save();
            return redirect()->to('/precargas/editar/'.(new \App\Tools\HelpersTool)->helper_getUrlHash($precarga->id).'/legajo');
        }
    }

    public function borrarLegajoFirmado()
    {
        $this->seguroDeEliminar = true;
    }

    public function cancelarBorrarLegajoFirmado()
    {
        $this->seguroDeEliminar = false;
    }

    public function confirmarBorrarLegajoFirmado($propuestaId)
    {

        $numberTool = new NumbersTool();
        $propuesta = (new PropuestaManager)->getItem($propuestaId);
        $precarga = Precarga::find($propuesta->precarga_id);
        $pathPrecarga = $numberTool->completeWithZero($precarga->id, 6);
        $pathPropuesta = $numberTool->completeWithZero($propuesta->id, 6);
        $pathLegajo = 'legajos/' . $pathPrecarga . '/' . $pathPropuesta . '/legajo_' . $pathPrecarga . '_' . $pathPropuesta . '_firmado.pdf';

        Storage::disk('local')->delete($pathLegajo);

        $propuesta->monto = (new FormatsTool)->prepareToSave('decimal', $propuesta->monto);
        $propuesta->cuotas = (new FormatsTool)->prepareToSave('decimal', $propuesta->cuotas);
        $propuesta->valor_cuota = (new FormatsTool)->prepareToSave('decimal', $propuesta->valor_cuota);
        $propuesta->adicionales = (new FormatsTool)->prepareToSave('decimal', $propuesta->adicionales);
        $propuesta->valor_cuota_social = (new FormatsTool)->prepareToSave('decimal', $propuesta->valor_cuota_social);
        $propuesta->legajo_firmado = false;
        $propuesta->path_documento_firmado = null;
        $propuesta->nombre_original_documento_firmado = null;
        $propuesta->save();
        return redirect()->to('/precargas/editar/'.(new \App\Tools\HelpersTool)->helper_getUrlHash($precarga->id).'/legajo');

    }

    public function changeState($estadoSlug)
    {
        if ($estadoSlug == 'autorizada') {
            $cantidadDocumentacion = DocumentacionAdjunta::where('precarga_id', $this->precarga->id)
                ->whereNotNull('documentacion_id')
                ->select('documentacion_id')
                ->groupBy('documentacion_id')
                ->get()
                ->count();
            $documentaciones = Documentacion::all()->count();
            if ($cantidadDocumentacion != $documentaciones) {
                $this->error = 1;
                $this->mensajeError = 'La Documentación debe estar completa para continuar.';
            }
        }
        if ($this->error != 1) {
            $precargaEstadoRelacion = (new PrecargaManager)->changeState($this->elem, $estadoSlug);
            return redirect()->to(url('/precargas/editar/' . (new \App\Tools\HelpersTool)->helper_getUrlHash($this->elem->id) . '/precarga'));
        }
    }

    public function render()
    {
        return view('livewire.propuesta-legajo');
    }
}
