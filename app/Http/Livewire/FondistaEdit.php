<?php

namespace App\Http\Livewire;

use App\Managers\FondistaManager;
use App\Models\Fondista;
use Illuminate\Validation\Rule;
use Livewire\Component;

class FondistaEdit extends Component
{
    public $fondista = '';
    public $nombre = '';
    public $abreviatura = '';
    public $fondistas = [];

    protected $rules = [
        'fondista.nombre' => 'required',
        'fondista.abreviatura' => 'required',
    ];

    protected $messages = [
        'fondista.nombre.required' => 'El fondista es requerido.',
        'fondista.abreviatura.required' => 'La abreviatura es requerida.',
        'fondista.nombre.unique' => 'Ya existe un fondista con este nombre',
    ];

    /**
     * Funciones relacionadas al Front
     */
    public function mount($fondistaId = 0)
    {
        if ($fondistaId != 0) {
            $this->edit($fondistaId);
        }else{
            $this->create();
        }

    }

    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function updatedFondistaNombre() {
        $this->validateOnly('fondista.nombre');
    }

    public function render()
    {
        return view('livewire.fondista-edit');
    }

    /**
     * Funciones relacionadas con ABM
     */
    public function create() {
        $this->fondista = new Fondista();
    }

    public function edit($fondistaId) {
        $this->fondista = (new FondistaManager())->getItem($fondistaId);
        $this->nombre = $this->fondista->nombre;
        $this->abreviatura = $this->fondista->abreviatura;
    }

    public function save()
    {
        $this->validate([
            'fondista.nombre' => [
                'required',
                Rule::unique('fondistas','nombre')->ignore($this->fondista->id),
            ],
            'fondista.abreviatura' => [
                'required',
                Rule::unique('fondistas','abreviatura')->ignore($this->fondista->id),
            ],
        ]);

        (new FondistaManager)->saveItem($this->fondista);

        return redirect()->to('/fondistas');
        // $this->emit('notify-saved');

    }
}
