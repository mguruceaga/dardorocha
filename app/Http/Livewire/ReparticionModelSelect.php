<?php

namespace App\Http\Livewire;

use App\Models\Reparticion;
use Asantibanez\LivewireSelect\LivewireSelect;
use Illuminate\Support\Collection;

class ReparticionModelSelect extends LivewireSelect
{

    public $reparticiones = [];

    public function options($searchTerm = null) : Collection
    {
        $listado = Reparticion::search('nombre', $searchTerm)
            ->get();
        info($listado);
        foreach($listado as $reparticion) {
            $this->reparticiones[] = ['value' => $reparticion->id, 'description' => $reparticion->nombre];
        }
        return collect($this->reparticiones);
    }

    public function selectedOption($value)
    {
        $reparticion = Reparticion::find($value);
        return ['value'=> $value, 'description' => $reparticion->nombre];
    }

    public function styles()
    {
        return [
            'default' => 'p-2 rounded border w-full appearance-none',

            'searchSelectedOption' => 'rounded-md flex-1 form-input w-full bg-white flex items-center',
            'searchSelectedOptionTitle' => 'w-full text-gray-900 text-left',
            'searchSelectedOptionReset' => 'h-4 w-4 text-gray-500',

            'search' => 'relative',
            'searchInput' => 'rounded-md flex-1 form-input block w-full',
            'searchOptionsContainer' => 'absolute top-0 left-0 mt-12 w-full z-10 border rounded-b-md',

            'searchOptionItem' => 'p-3 hover:bg-gray-100 hover:text-gray-800 cursor-pointer text-sm',
            'searchOptionItemActive' => 'bg-indigo-600 text-white font-medium',
            'searchOptionItemInactive' => 'bg-white text-gray-900',

            'searchNoResults' => 'p-8 w-full bg-white border text-center text-xs text-gray-600',
        ];
    }

}
