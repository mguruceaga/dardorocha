<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;


class UsuarioList extends Component
{
    use WithPagination;

    public $name = '';
    public $ordenarPor = 'name';
    public $sentido = 'ASC';

    protected $queryString = ['ordenarPor', 'sentido'];

    public function sortBy($field) {
        $this->sentido = $this->ordenarPor === $field
            ? $this->sentido === 'ASC' ? 'DESC' : 'ASC'
            : 'ASC';
    }

    public function updatingNombre()
    {
        $this->resetPage();
        $this->gotoPage(1);
    }

    public function updatingPartido()
    {
        $this->resetPage();
        $this->gotoPage(1);
    }

    public function cleanSort() {
        $this->name = '';
    }

    public function render()
    {
        return view('livewire.usuario-list', [
            'usuarios' => User::withTrashed()
                ->search('name', $this->name)
                ->where('deleted', 0)
                ->orderBy($this->ordenarPor, $this->sentido)
                ->paginate(10),
        ]);
    }

}
