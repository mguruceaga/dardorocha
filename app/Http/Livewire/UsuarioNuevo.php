<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Crypt;
use Livewire\Component;

class UsuarioNuevo extends Component
{
    public $usuario = '';
    public $token = '';
    public $email = '';
    public $password = '';
    public $password_confirmation = '';

    public function mount($token) {
        $this->token = $token;
        $this->usuario = User::find(Crypt::decrypt($token));
        $this->email = $this->usuario->email;
    }

    public function save() {
        $this->password = 'dfasdfasfd';
    }

    public function render() {
        return view('livewire.usuario-nuevo');
    }
}
