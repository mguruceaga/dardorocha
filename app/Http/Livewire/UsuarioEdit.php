<?php

namespace App\Http\Livewire;

use App\Managers\UsuarioManager;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Validator;

class UsuarioEdit extends Component
{

    public $usuarioId = 0;
    public $usuario = '';
    public $name = '';
    public $email = '';
    public $suspended = '';
    public $recibePrecargas = '';
    public $usuarioRol = 0;
    public $roles = [];
    public $deleteUser = 0;
    public $userDeleted = 0;

    protected $rules = [
        'usuario.name' => 'required',
        'usuario.email' => 'required',
        'usuario.suspended' => 'nullable',
    ];

    protected $messages = [
        'usuario.name.required' => 'El nombre del Usuario es requerido.',
        'usuario.email.required' => 'El correo electrónico del Usuario es requerido.',
        'usuario.email.unique' => 'Ya existe un usuario con este correo electrónico.',
    ];

    /**
     * Funciones relacionadas al Front
     */
    public function mount($usuarioId = 0)
    {
        if ($usuarioId != 0) {
            $this->edit($usuarioId);
        }else{
            $this->create();
        }
        $this->roles = Role::orderBy('name')->get()->pluck('name', 'id')->toArray();
    }

    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    /**
     * Funciones relacionadas con ABM
     */
    public function create() {
        $this->usuario = new User();
        $this->name = '';
        $this->email = '';
        $this->suspended = 0;
        $this->recibePrecargas = 0;
    }

    public function edit($usuarioId) {
        $this->usuario = (new UsuarioManager)->getItem($usuarioId);
        $this->name = $this->usuario->name;
        $this->email = $this->usuario->email;
        $this->suspended = $this->usuario->suspended;
        $this->recibePrecargas = $this->usuario->recibe_precargas;
        if (count($this->usuario->getRoleNames()) != 0)
            $this->usuarioRol = Role::where('name', $this->usuario->getRoleNames()[0])->first()->id;
    }

    public function save()
    {
        if (!isset($this->usuario->id)) {
            $usuario = User::withTrashed()
                ->where('email', $this->usuario->email)
                ->where('deleted', 1)
                ->first();
            if (!is_null($usuario)) {
                $this->userDeleted = true;
            }
        }

        if ($this->userDeleted) {
            $this->edit($usuario->id);
        } else {
            $this->validate();

            $this->validate([
                'usuario.email' => [
                    Rule::unique('users', 'email')->ignore($this->usuario->id),
                ],
            ]);

            $this->usuario->suspended = $this->suspended;
            $this->usuario->recibe_precargas = $this->recibePrecargas;
            (new UsuarioManager)->saveItem($this->usuario, $this->usuarioRol);
            return redirect()->to('/usuarios');
        }
    }

    public function consultDeleteUser() {
        $this->deleteUser = true;
    }

    public function cancelDeleteUser() {
        $this->deleteUser = false;
    }

    public function deleting() {
        $this->usuario->deleted = 1;
        $this->usuario->save();
        $this->usuario->delete();
        return redirect()->to('/usuarios');
    }

    public function restoring() {
        $this->usuario->forceFill([
            'deleted' => 0,
            'suspended' => 0,
        ])->save();
        $this->usuario->restore();
        return redirect()->to('/usuarios');
    }

    public function cancelRestoreUser() {
        $this->userDeleted = false;
        $this->create();
    }

    public function render()
    {
        return view('livewire.usuario-edit');
    }

}
