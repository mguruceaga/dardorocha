<?php

namespace App\Http\Livewire;

use App\Models\Banco;
use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use Livewire\Component;
use Livewire\WithPagination;

class BancoList extends Component
{
    use WithPagination;

    public $nombre = '';
    public $codigo = '';
    public $ordenarPor = 'nombre';
    public $sentido = 'ASC';

    protected $queryString = ['ordenarPor', 'sentido'];

    public function sortBy($field) {
        $this->sentido = $this->ordenarPor === $field
            ? $this->sentido === 'ASC' ? 'DESC' : 'ASC'
            : 'ASC';
    }

    public function updatingNombre()
    {
        $this->setPage(1);
    }

    public function updatingCodigo()
    {
        $this->setPage(1);
    }

    public function cleanSort() {
        $this->nombre = '';
        $this->codigo = '';
    }

    public function render()
    {
        return view('livewire.banco-list', [
            'bancos' => Banco::search('nombre', $this->nombre)
                ->search('codigo', $this->codigo)
                ->orderBy ($this->ordenarPor, $this->sentido)
                ->paginate(10)
        ]);
    }
}
