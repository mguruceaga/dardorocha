<?php

namespace App\Http\Livewire;

use App\Managers\PrecargaManager;
use App\Models\Banco;
use App\Models\CodigoPostal;
use App\Models\ContratacionTipo;
use App\Models\DocumentoTipo;
use App\Models\Genero;
use App\Models\Precarga;
use App\Models\PrecargaEstado;
use App\Models\PrecargaEstadoRelacion;
use App\Models\PrestamoTipo;
use App\Models\Reparticion;
use App\Models\Sector;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class PrecargaEstados extends Component
{
    public $elem = '';
    public $estados = '';
    public $userPerfil = '';

    public function mount($precargaId = 0)
    {
        $this->userPerfil = auth()->user()->perfil();
        if ($precargaId != 0) {
            $this->elem = (new PrecargaManager)->getItem($precargaId);
            $this->estados = (new PrecargaManager)->getItemStates($this->elem);
        } else {
            $this->estados = new Collection();
        }
    }

    public function render()
    {
        return view('livewire.precarga-estados', [
            'userRol' => auth()->user()->rolPrincipal(),
        ]);
    }

    public function changeState($estadoSlug)
    {
        $precargaEstadoRelacion = (new PrecargaManager)->changeState($this->elem, $estadoSlug);
        return redirect()->to(url('/precargas/editar/'.(new \App\Tools\HelpersTool)->helper_getUrlHash($this->elem->id).'/precarga'));
    }

}
