<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Password;
use Livewire\Component;

class UsuarioNew extends Component
{
    public $usuario = '';
    public $token = '';
    public $email = '';
    public $password = '';
    public $password_confirmation = '';

    protected $rules = [
        'password' => 'required|confirmed',
        'password_confirmation' => 'required',
    ];

    protected $messages = [
        'password.required' => 'La Contraseña es requerida.',
        'password_confirmation.required' => 'La Confirmación de Contraseña es requerida.',
    ];

    /**
     * Funciones relacionadas al Front
     */
    public function mount($token)
    {
        $this->token = $token;
        $this->usuario = User::find(Crypt::decrypt($token));
        $this->email = $this->usuario->email;
    }

    public function save() {
        dd('aca');
        $this->validate();
        $this->validate([
            'password' => [
                Password::min(8)
            ],
        ]);
    }

    public function render()
    {
        return view('livewire.usuario-new');
    }
}
