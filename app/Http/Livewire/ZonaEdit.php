<?php

namespace App\Http\Livewire;

use App\Managers\ZonaManager;
use App\Models\GrupoPago;
use App\Models\ReparticionZonaGrupo;
use App\Models\Zona;
use App\Models\ZonaPorGrupoPago;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Livewire\Component;
use function Livewire\str;

class ZonaEdit extends Component
{
    public $zona = '';
    public $nombre = '';
    public $origen = [];
    public $origenes = [];
    public $origenesAsociados = [];
    public $origenesSeteados = [];
    public $reparticionesPorOrigenes = [];

    protected $rules = [
        'zona.nombre' => 'required',
        //'zona.partido' => 'required',
    ];

    protected $messages = [
        'zona.nombre.required' => 'El nombre de la Zona es requerida.',
        'zona.nombre.unique' => 'Ya existe una zona con este nombre',
        //'zona.partido.required' => 'El partido es requerido.',
        //'zona.partido.unique' => 'Ya existe un partido con este nombre',
    ];

    /**
     * Funciones relacionadas al Front
     */
    public function mount($zonaId = 0)
    {
        $this->origenes = GrupoPago::all();
        foreach($this->origenes as $origen) {
            $this->origenesSeteados[$origen->id] = false;
            $this->reparticionesPorOrigenes[$origen->id] = 0;
        }
        if ($zonaId != 0) {
            $this->edit($zonaId);
            $this->origenesAsociados = $this->zona->origenes()->pluck('descripcion', 'id')->toArray();
            $query = 'SELECT grupo_pago_id as id, COUNT(id) as total FROM reparticion_zona_grupos WHERE zona_id = '.$zonaId.' GROUP BY grupo_pago_id';
            $reparticiones = DB::select($query);
            foreach($reparticiones as $k => $v) {
                $this->reparticionesPorOrigenes[$v->id] = $v->total;
            }
            foreach($this->origenesAsociados as $k => $v) {
                $this->origenesSeteados[$k] = 1;
            }
        } else {
            $this->create();
        }
    }

    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function updatedElemNombre() {
        $this->validateOnly('zona.nombre');
    }

    /**
     * Funciones relacionadas con ABM
     */
    public function create() {
        $this->zona = new Zona();
    }

    public function edit($zonaId) {
        $this->zona = (new ZonaManager)->getItem($zonaId);
        $this->nombre = $this->zona->nombre;
        //$this->partido = $this->zona->partido;
    }

    public function save()
    {
        //$this->validate();
        $this->validate([
            'zona.nombre' => [
                'required',
                Rule::unique('zonas','nombre')->ignore($this->zona->id),
            ],
        ]);

        (new ZonaManager)->saveItem($this->zona);

        foreach ($this->origenesSeteados as $k => $v) {
            $zonaPorGrupoPagoExiste = ZonaPorGrupoPago::where('zona_id', $this->zona->id)
                ->where('grupo_pago_id', $k)
                ->first();
            if ($v) {
                if (is_null($zonaPorGrupoPagoExiste))
                    ZonaPorGrupoPago::create([
                        'zona_id' => $this->zona->id,
                        'grupo_pago_id' => $k
                        ]);
            } else {
                if (!is_null($zonaPorGrupoPagoExiste))
                    ZonaPorGrupoPago::where('zona_id', $this->zona->id)
                        ->where('grupo_pago_id', $k)
                        ->delete();
            }
        }

        return redirect()->to('/origenes');
        // $this->emit('notify-saved');

    }

    public function render()
    {
        return view('livewire.zona-edit');
    }
}
