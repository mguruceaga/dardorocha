<?php

namespace App\Http\Livewire;

use App\Managers\PrecargaManager;
use App\Managers\PropuestaManager;
use App\Models\Documentacion;
use App\Models\DocumentacionAdjunta;
use App\Models\Fondista;
use App\Models\PrecargaEstado;
use App\Models\Propuesta;
use App\Tools\FormatsTool;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Livewire\Component;
use function PHPUnit\Framework\isNan;

class PropuestasList extends Component
{

    public $elem = '';
    public $precargaId = '';
    public $precarga = '';
    public $estadoActualSlug = '';
    public $error = 0;
    public $mensajeError = '';
    public $multiplesPropuestas = 0;
    public $opciones = [];
    public $fondista = [];
    public $fondistas = [];
    public $gestionador = 0;
    public $propuestas = [];
    public $lineasCredito = [];
    public $grillas = [];
    public $debitos = [];
    public $editable = true;
    public $errorMonto = false;
    public $errorCuotas = false;
    public $errorValorCuota = false;
    public $errorAdicionales = false;
    public $errorCuotaSocial = false;

    protected $rules = [
        'elem.linea_credito_id' => 'required|gt:0',
        'elem.grilla_id' => 'required|gt:0',
        'elem.monto' => 'required|max:100000000',
        'elem.cuotas' => 'required|max:100000000',
        'elem.valor_cuota' => 'required|max:100000000',
        'elem.debito_id' => 'required|gt:0',
        'elem.adicionales' => 'required|max:100000000',
        'elem.valor_cuota_social' => 'required|max:100000000',
    ];

    protected $messages = [
        'elem.linea_credito_id.required' => 'Es requerido.',
        'elem.linea_credito_id.gt' => 'Es requerido.',
        'elem.grilla_id.required' => 'Es requerido.',
        'elem.grilla_id.gt' => 'Es requerido.',
        'elem.monto.required' => 'Es requerido.',
        'elem.monto.lt' => 'Valor inválido.',
        'elem.cuotas.required' => 'Es requerido.',
        'elem.cuotas.lt' => 'Valor inválido.',
        'elem.valor_cuota.required' => 'Es requerido.',
        'elem.valor_cuota.lt' => 'Valor inválido.',
        'elem.debito.required' => 'Es requerido.',
        'elem.debito_id.gt' => 'Es requerido.',
        'elem.debito_id.required' => 'Es requerido.',
        'elem.adicionales.required' => 'Es requerido.',
        'elem.adicionales.lt' => 'Valor inválido.',
        'elem.valor_cuota_social.required' => 'Es requerido.',
        'elem.valor_cuota_social.lt' => 'Valor inválido.',
    ];

    protected $listeners = [
        'deletePropuesta'
    ];

    public function mount($precargaId = 0, $estadoActualSlug = 'iniciada', $gestionador = false)
    {
        $this->elem = (new PropuestaManager)->newItem();
        $this->precargaId = $precargaId;
        $this->precarga = (new PrecargaManager)->getItem($precargaId);
        $this->estadoActualSlug = $estadoActualSlug;
        $this->gestionador = $gestionador;
        $this->lineasCredito = (new PropuestaManager)->getLineasCredito();
        $this->grillas = (new PropuestaManager)->getGrillas();
        $this->debitos = (new PropuestaManager)->getDebitos()->pluck('nombre', 'id')->toArray();
        $this->editable = true;
        $this->fondistas = Fondista::orderBy('nombre')->get();
    }

    public function hydrate()
    {
        $this->errorMonto = false;
        $this->errorCuotas = false;
        $this->errorValorCuota = false;
        $this->errorAdicionales = false;
        $this->errorCuotaSocial = false;
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function save()
    {
        /**
         * Convertir números con decimales con , en número con decimales con .
         */
        $this->elem->monto = (new FormatsTool)->prepareToSave('decimal', $this->elem->monto);
        $this->elem->cuotas = (new FormatsTool)->prepareToSave('decimal', $this->elem->cuotas);
        $this->elem->valor_cuota = (new FormatsTool)->prepareToSave('decimal', $this->elem->valor_cuota);
        $this->elem->adicionales = (new FormatsTool)->prepareToSave('decimal', $this->elem->adicionales);
        $this->elem->valor_cuota_social = (new FormatsTool)->prepareToSave('decimal', $this->elem->valor_cuota_social);

        $validator = Validator::make(
            [
                'elem' => $this->elem,
            ],
            $this->rules,
            $this->messages
        );

        if ($validator->fails()) {
            $this->mensajeError = 'Se produjeron errores en el guardado';
            $this->dispatchBrowserEvent('notify');
            $this->elem->precarga_id = $this->precargaId;
            $data = $this->validate();
            (new PropuestaManager)->saveItem($this->elem);
            return redirect()->to('/precargas/editar/'.(new \App\Tools\HelpersTool)->helper_getUrlHash($this->elem->precarga_id).'/propuesta');
        } else {
            $errores = false;
            if ($this->elem->monto >= 100000000) {
                $this->errorMonto = true;
                $this->mensajeError = 'Se produjeron errores en el guardado';
                $this->dispatchBrowserEvent('notify');
                $errores = true;
            }
            if ($this->elem->cuotas >= 100000000) {
                $this->errorCuotas = true;
                $this->mensajeError = 'Se produjeron errores en el guardado';
                $this->dispatchBrowserEvent('notify');
                $errores = true;
            }
            if ($this->elem->valor_cuota >= 100000000) {
                $this->errorValorCuota = true;
                $this->mensajeError = 'Se produjeron errores en el guardado';
                $this->dispatchBrowserEvent('notify');
                $errores = true;
            }
            if ($this->elem->adicionales >= 100000000) {
                $this->errorAdicionales = true;
                $this->mensajeError = 'Se produjeron errores en el guardado';
                $this->dispatchBrowserEvent('notify');
                $errores = true;
            }
            if ($this->elem->valor_cuota_social >= 100000000) {
                $this->errorCuotaSocial = true;
                $this->mensajeError = 'Se produjeron errores en el guardado';
                $this->dispatchBrowserEvent('notify');
            }
            if (!$errores) {
                $this->elem->precarga_id = $this->precargaId;
                $data = $this->validate();
                (new PropuestaManager)->saveItem($this->elem);
                return redirect()->to('/precargas/editar/'.(new \App\Tools\HelpersTool)->helper_getUrlHash($this->elem->precarga_id).'/propuesta');
            }
        }

    }

    public function deletePropuesta() {
        $this->propuestas = null;
    }

    public function render()
    {
        if ($this->precargaId != 0) {
            $this->propuestas = (new PropuestaManager)->getItemFromPrecarga($this->precargaId);
        } else {
            $this->propuestas = new \Illuminate\Support\Collection();
        }
        return view('livewire.propuestas-list');
    }

    public function delete($propuestaId) {
        $propuesta = (new PropuestaManager)->getItem($propuestaId);
        (new PropuestaManager)->delete($propuestaId);
        return redirect()->to('/precargas/editar/'.(new \App\Tools\HelpersTool)->helper_getUrlHash($propuesta->precarga_id).'/propuesta');
    }

    public function cleanForm() {
        $this->elem = (new PropuestaManager)->newItem();
    }

    public function changeLineaId($value) {
        $this->elem->linea_credito_id = $value;
        $this->elem->grilla_id = 0;
        $this->emit('changeLinea', $value);
    }

    public function changeState($estadoSlug)
    {
        try {
            $precarga = (new PrecargaManager)->getItem($this->precargaId);
            $this->error = 0;
            $this->errorAlAprobar = '';
            if ($estadoSlug == 'aceptada') {
                $cantidadDocumentacion = DocumentacionAdjunta::where('precarga_id', $precarga->id)
                    ->whereNotNull('documentacion_id')
                    ->select('documentacion_id')
                    ->groupBy('documentacion_id')
                    ->get()
                    ->count();
                $documentaciones = Documentacion::all()->count();
                if ($cantidadDocumentacion != $documentaciones) {
                    $this->error = 1;
                    $this->mensajeError = 'La Documentación debe estar completa para continuar.';
                }
            }
            if ($this->error != 1) {
                switch ($estadoSlug) {
                    case 'en_revision':
                    case 'preaprobada':
                    case 'aceptada':
                        $precarga->propuesta_multiple = $this->multiplesPropuestas;
                        $precarga->fecha_nacimiento = Carbon::createFromDate($precarga->fecha_nacimiento)->format('Y-m-d');
                        $precarga->cuota_maxima = (new FormatsTool)->prepareToSave('decimal', $precarga->cuota_maxima);
                        if ($precarga->plazo_maximo == '') {
                            $precarga->plazo_maximo = null;
                        }
                        $precarga->monto_maximo = (new FormatsTool)->prepareToSave('decimal', $precarga->monto_maximo);
                        if ($precarga->ingreso_laboral == '')
                            $precarga->ingreso_laboral = null;
                        else
                            $precarga->ingreso_laboral = (new FormatsTool)->prepareToSave('decimal', $precarga->ingreso_laboral);
                        $precarga->save();
                        $precargaEstadoActual = PrecargaEstado::find($precarga->precarga_estado_id)->first();
                        if ($precargaEstadoActual->slug == $estadoSlug) {
                            $this->error = 1;
                            $this->mensajeError = 'El estado actual de la Precarga ya es: ' . $precargaEstadoActual->nombre . '.';
                        } else {
                            $precargaEstadoRelacion = (new PrecargaManager)->changeState($precarga, $estadoSlug);
                            return redirect()->to(url('/precargas/editar/' . (new \App\Tools\HelpersTool)->helper_getUrlHash($this->precargaId) . '/precarga'));
                        }
                        break;
                }
            }
        } catch (\Exception $e) {
            $this->error = 1;
            $this->mensajeError = $e->getMessage();
        }
    }

    public function agregarPropuesta()
    {
        $precarga = (new PrecargaManager)->getItem($this->precargaId);
        $precarga->propuesta_multiple = $this->multiplesPropuestas;
        $precarga->fecha_nacimiento = Carbon::createFromDate($precarga->fecha_nacimiento)->format('Y-m-d');
        $precarga->cuota_maxima = (new FormatsTool)->prepareToSave('decimal', $precarga->cuota_maxima);
        if ($precarga->plazo_maximo == '') {
            $precarga->plazo_maximo = null;
        }
        $precarga->monto_maximo = (new FormatsTool)->prepareToSave('decimal', $precarga->monto_maximo);
        if ($precarga->ingreso_laboral == '')
            $precarga->ingreso_laboral = null;
        else
            $precarga->ingreso_laboral = (new FormatsTool)->prepareToSave('decimal', $precarga->ingreso_laboral);
        $precarga->seleccionada = false;
        $precarga->save();
        return redirect()->to(url('/precargas/editar/'.(new \App\Tools\HelpersTool)->helper_getUrlHash($this->precargaId).'/precarga'));
    }

    public function propuestaAceptada() {

        $this->error = 0;
        $this->mensajeError = '';

        $aceptadas = 0;
        foreach($this->propuestas as $propuesta) {
            if ($propuesta->aceptada) {
                $aceptadas += 1;
            }
        }
        if ($aceptadas == 0) {
            $this->error = 1;
            $this->mensajeError = 'Debe seleccionar al menos una propuesta.';
        } else {
            $propuestas = Propuesta::where('precarga_id', $this->elem->id)
                ->get();
            foreach ($propuestas as $propuesta) {
                $propuesta->aceptada = 0;
                $propuesta->save();
            }
            if (is_array($this->opciones)) {
                foreach ($this->opciones as $k => $v) {
                    $propuesta = Propuesta::find($v)
                        ->update(['aceptada' => 1]);
                }
            } else {
                $propuesta = Propuesta::find($this->opciones)
                    ->update(['aceptada' => 1]);
            }
            $this->changeState('aceptada');
        }
    }

    public function fondearPropuestas() {

        $this->error = 0;
        $this->mensajeError = '';

        $todosValidados = true;
        foreach($this->propuestas as $propuesta) {
            if ($propuesta->aceptada) {
                if (is_null($propuesta->fondista_id)) {
                    $todosValidados = false;
                }
            }
        }
        if (!$todosValidados) {
            $this->error = 1;
            $this->mensajeError = 'Cada propuesta debe tener seleccionado su Fondista.';
        } else {
            foreach ($this->propuestas as $propuesta) {
                (new PropuestaManager)->asociarFormularios($propuesta);
            }
            $this->precarga->fondeada = true;
            $this->precarga->save();
            return redirect()->to(url('/precargas/editar/' . (new \App\Tools\HelpersTool)->helper_getUrlHash($this->precargaId) . '/legajo'));
        }
    }

}
