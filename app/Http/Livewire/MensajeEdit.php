<?php

namespace App\Http\Livewire;

use App\Models\Mensaje;
use App\Managers\MensajeManager;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class MensajeEdit extends Component
{
    public $mensaje = '';
    public $precargaId = 0;
    public $mensajeId = 0;
    public $userId = '';

    protected $rules = [
        'mensaje.texto' => 'required',
    ];

    protected $messages = [
        'mensaje.texto.required' => 'Tenés que ingresar un mensaje.',
    ];

    /**
     * Funciones relacionadas al Front
     */
    public function mount($precarga, $mensajeId)
    {
        $this->create($precarga, $mensajeId);
    }

    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function updatedElemNombre() {
        $this->validateOnly('mensaje.texto');
    }

    public function save()
    {
        $data = $this->validate();
        (new MensajeManager)->saveItem($this->mensaje, $this->precargaId, $this->mensajeId);
        return redirect()->to('/precargas/editar/'.(new \App\Tools\HelpersTool)->helper_getUrlHash($this->mensaje->precarga_id).'/mensajes');
    }

    public function render()
    {
        return view('livewire.mensaje-edit');
    }

    public function clearMessage()
    {
        $this->mensaje->texto = '';
    }

    /**
     * Funciones relacionadas con ABM
     */
    public function create($precarga, $mensajeId) {
        $this->mensaje = new Mensaje();
        $this->precargaId = $precarga->id;
        $this->mensajeId = $mensajeId;
        $this->userId = Auth::user()->id;
        $this->mensaje->texto = '';

    }
}
