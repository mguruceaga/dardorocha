<?php

namespace App\Http\Livewire;

use App\Models\Mensaje;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class MensajeShow extends Component
{
    public $mensaje = '';
    public $padding = false;

    public function mount($mensaje)
    {
        $this->mensaje = $mensaje;
        if ($mensaje->mensaje_id != 0)
            $this->padding = true;

    }
    public function render()
    {
        return view('livewire.mensaje-show');
    }
}
