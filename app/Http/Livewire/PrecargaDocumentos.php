<?php

namespace App\Http\Livewire;

use App\Managers\DocumentacionManager;
use Livewire\Component;
use Livewire\WithFileUploads;

class PrecargaDocumentos extends Component
{
    use WithFileUploads;

    public $elem = '';
    public $files = [];
    public $estadoActualSlug = '';

    public function mount($precarga)
    {
        $this->elem = $precarga;
        $this->estadoActualSlug = $precarga->precargaEstadoActual()->slug;
    }

    public function update() {
    }

    public function save()
    {
        foreach($this->files as $file) {
            $fileName = $file->store('/', 'documents');
            $documentacionAdjunta = (new DocumentacionManager)->saveNewItem($this->elem, $file, $fileName);
        }
        return redirect()->to('/precargas/editar/'.(new \App\Tools\HelpersTool)->helper_getUrlHash($this->elem->id).'/documentos');
    }

    public function removeUploads()
    {
        $this->dispatchBrowserEvent('removeall');
    }

    public function render()
    {
        return view('livewire.precarga-documentos');
    }
}
