<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Reparticion;

class ReparticionList extends Component
{
    use WithPagination;

    public $nombre = '';
    public $cuit = '';
    public $ordenarPor = 'nombre';
    public $sentido = 'ASC';

    protected $queryString = ['ordenarPor', 'sentido'];

    public function sortBy($field) {
        $this->sentido = $this->ordenarPor === $field
            ? $this->sentido === 'ASC' ? 'DESC' : 'ASC'
            : 'ASC';
    }

    public function updatingNombre()
    {
        $this->resetPage();
        $this->gotoPage(1);
    }

    public function updatingPartido()
    {
        $this->resetPage();
        $this->gotoPage(1);
    }


    public function cleanSort() {
        $this->nombre = '';
        $this->cuit = '';
    }

    public function render()
    {           
        return view('livewire.reparticion-list', [            
            'reparticiones' => Reparticion::from('reparticiones')
                ->join('zonas', 'reparticiones.zona_id', 'zonas.id')
                ->search('reparticiones.nombre', $this->nombre)
                ->search('reparticiones.cuit', $this->cuit)
                ->addSelect(['reparticiones.id', 'reparticiones.nombre as reparticionNombre', 'reparticiones.cuit', 'zonas.nombre as zonaNombre'])
                ->orderBy('reparticionNombre', $this->sentido)
                ->paginate(10),
            ]);
    }
}
