<?php

namespace App\Http\Livewire;

use App\Models\ReparticionZonaGrupo;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use App\Models\Reparticion;
use App\Managers\ReparticionManager;
use Illuminate\Validation\Rule;

class ReparticionEdit extends Component
{
    public $reparticion = '';
    public $nombre = '';
    public $cuit = '';
    public $zonas = [];
    public $origenes = [];
    public $origenId = '';

    protected $rules = [
        'reparticion.nombre' => 'required',
        'reparticion.cuit' => 'required',
        'reparticion.zona_id' => 'required|gt:0',
    ];

    protected $messages = [
        'reparticion.nombre.required' => 'El nombre de la Repartición es requerido.',
        'reparticion.nombre.unique' => 'Ya existe una Repartición con este nombre',
        'reparticion.cuit.required' => 'El cuit es requerido.',
        //'reparticion.cuit.unique' => 'Ya existe una Repartición con este cuit',
        'reparticion.zona_id.required' => 'Es requerido.',
        'reparticion.zona_id.gt' => 'Es requerido.',
    ];

    public function mount($reparticionId = 0)
    {

        if ($reparticionId != 0) {
            $this->edit($reparticionId);
        }else{
            $this->create();
        }
        $this->zonas = DB::table('zona_por_grupo_pagos')
            ->join('grupos_pagos', 'grupos_pagos.id', 'zona_por_grupo_pagos.grupo_pago_id')
            ->join('zonas', 'zonas.id', 'zona_por_grupo_pagos.zona_id')
            ->select('grupos_pagos.id as grupoPagoId', 'grupos_pagos.descripcion as grupoPagoNombre',
                            'zonas.id as zonaId', 'zonas.nombre as zonaNombre')
            ->orderBy('zonas.nombre', 'ASC')
            ->orderBy('grupos_pagos.descripcion', 'ASC')
            ->get();
        foreach ($this->zonas as $zona) {
            $this->origenes[$zona->zonaId . '_' . $zona->grupoPagoId] = $zona->zonaNombre . ' - ' . $zona->grupoPagoNombre;
        }
    }

    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }


    public function updatedElemNombre() {
        $this->validateOnly('reparticion.nombre');
    }

    public function save()
    {
        //$this->validate();
        $this->validate([
            'reparticion.nombre' => [
                'required',
                Rule::unique('reparticiones','nombre')->ignore($this->reparticion->id),
            ],
//            'reparticion.cuit' => [
//                'required',
//                Rule::unique('reparticiones','cuit')->ignore($this->reparticion->id),
//            ],
        ]);

        $arrOrigen = explode('_', $this->origenId);

        if (count($arrOrigen) == 2) {
            $this->reparticion->zona_id = $arrOrigen[0];
        }
        (new ReparticionManager)->saveItem($this->reparticion);

        ReparticionZonaGrupo::where('reparticion_id', $this->reparticion->id)
            ->delete();

        if (count($arrOrigen) == 2) {
            ReparticionZonaGrupo::create([
                'alias' => $this->reparticion->nombre,
                'reparticion_id' => $this->reparticion->id,
                'zona_id' => $arrOrigen[0],
                'grupo_pago_id' => $arrOrigen[1]
            ]);
        }

        return redirect()->to('/reparticiones');
        // $this->emit('notify-saved');

    }

    public function render()
    {
        return view('livewire.reparticion-edit');
    }

    /**
     * Funciones relacionadas con ABM
     */
    public function create(){
        $this->reparticion = new Reparticion();
    }
    public function edit($reparticionId){
        $this->reparticion = (new ReparticionManager)->getItem($reparticionId);
        $this->nombre = $this->reparticion->nombre;
        $reparticionZonaGrupo = ReparticionZonaGrupo::where('reparticion_id', $reparticionId)
            ->first();
        if (!is_null($reparticionZonaGrupo))  {
            $this->origenId = $reparticionZonaGrupo->zona_id . '_' . $reparticionZonaGrupo->grupo_pago_id;
        }
    }

}
