<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Zona;

class ZonaList extends Component
{
    use WithPagination;

    public $nombre = '';
    //public $partido = '';
    public $ordenarPor = 'nombre';
    public $sentido = 'ASC';

    protected $queryString = ['ordenarPor', 'sentido'];

    public function sortBy($field) {
        $this->sentido = $this->ordenarPor === $field
            ? $this->sentido === 'ASC' ? 'DESC' : 'ASC'
            : 'ASC';
    }

    public function updatingNombre()
    {
        $this->resetPage();
        $this->gotoPage(1);
    }

    public function updatingPartido()
    {
        $this->resetPage();
        $this->gotoPage(1);
    }

    public function cleanSort() {
        $this->nombre = '';
        //$this->partido = '';
    }

    public function render()
    {
        return view('livewire.zona-list', [
            'zonas' => Zona::search('nombre', $this->nombre)
                //->search('partido', $this->partido)
                ->orderBy($this->ordenarPor, $this->sentido)
                ->paginate(10),
        ]);
    }

}
