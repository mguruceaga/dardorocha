<?php

namespace App\Http\Livewire;

use App\Models\Precarga;
use App\Models\PrecargaEstado;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class PrecargaListItem extends Component
{
    public $precarga = null;
    public $userRol = null;
    public $userPerfil = null;
    public $estadosPrecarga = [];
    public $users = [];
    public $selectUser = null;

    public function mount($precargaId, $userRol, $userPerfil) {
        $this->precarga = Precarga::find($precargaId);
        $this->selectUser = $this->precarga->user_asignado_id;
        $this->userRol = $userRol;
        $this->userPerfil = $userPerfil;
        $this->estadosPrecarga = PrecargaEstado::orderBy('nombre')
            ->get()
            ->pluck('nombre', 'slug')
            ->toArray();
        $this->users = DB::table('users')
            ->join('model_has_roles', 'model_has_roles.model_id', 'users.id')
            ->where('model_has_roles.role_id', '<=', 2)
            ->orderBy('name')
            ->select('users.id', 'users.name')
            ->pluck('name', 'id')
            ->toArray();
    }

    public function changeUser() {
        $this->precarga->user_asignado_id = $this->selectUser;
        $this->precarga->save();
    }

    public function render()
    {
        info('Apellido: ' . $this->precarga->apellido);
        return view('livewire.precarga-list-item');
    }
}
