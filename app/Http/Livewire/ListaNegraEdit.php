<?php

namespace App\Http\Livewire;

use App\Managers\DocumentoTipoManager;
use App\Models\ListaNegra;
use Illuminate\Validation\Rule;
use Livewire\Component;
use App\Managers\ListaNegraManager;
use App\Managers\ListaNegraMotivoManager;

class ListaNegraEdit extends Component
{
    public $listaNegra = '';
    public $tipoDocs = [];
    public $numDoc = '';
    public $nombreYapellido = '';
    public $fecha = '';
    public $motivo = [];
    public $listasNegras = [];

    protected $rules = [
        'listaNegra.nombre' => 'required',
        'listaNegra.apellido' => 'required',
        'listaNegra.documento_tipo_id' => 'required',
        'listaNegra.numero_doc' => 'required|gt:0',
        'listaNegra.fecha' => 'required',
        'listaNegra.observacion' => '',
        'listaNegra.lista_negra_motivo_id' => '',

    ];

    protected $messages = [
        'listaNegra.nombre.required' => 'El nombre es requerido.',
        'listaNegra.apellido.required' => 'El apellido es requerido.',
        'listaNegra.documento_tipo_id.required' => 'El tipo de documento es requerido.',
        'listaNegra.numero_doc.required' => 'El número de documento es requerido.',
        'listaNegra.fecha.required' => 'Es requerido.',
        'listaNegra.fecha.date' => 'Debe ser una fecha válida.',
    ];

    public function mount($listaNegraId = 0)
    {
        if ($listaNegraId != 0) {
            $this->edit($listaNegraId);
        }else{
            $this->create();
        }

        $this->tipoDocs = (new DocumentoTipoManager)->getItems('nombre')->pluck('nombre', 'id')->toArray();
        $this->listasNegras = (new ListaNegraMotivoManager) ->getItems('nombre')->pluck('nombre', 'id')->toArray();
    }

    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function updatedElemNombre() {
        $this->validateOnly('listaNegra.apellido');
    }

    public function save()
    {
        $this->validate([
            'listaNegra.apellido' => [
                'required'
            ],
        ]);

        (new ListaNegraManager)->saveItem($this->listaNegra);

        return redirect()->to('/listaNegra');
        // $this->emit('notify-saved');

    }

    public function render()
    {
        return view('livewire.lista-negra-edit');
    }

    /**
     * Funciones relacionadas con ABM
     */
    public function create(){
        $this->listaNegra = new ListaNegra();
    }

    public function edit($listaNegraId){
        $this->listaNegra = (new ListaNegraManager)->getItem($listaNegraId);
        $this->apellido = $this->listaNegra->apellido;
    }

}
