<?php

namespace App\Http\Livewire;

use App\Managers\DocumentacionManager;
use App\Managers\PrecargaEstadoManager;
use App\Managers\PrecargaManager;
use App\Models\Propuesta;
use App\Tools\FormatsTool;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;

class PrecargaSubirLegajo extends Component
{
    use WithFileUploads;

    public $elem = '';
    public $key = 0;
    public $propuestas = [];
    public $propuesta_id = 0;
    public $tipo_documento = 0;
    public $conFirma = false;
    public $filesLegajos = [];
    public $estadoActualSlug = '';
    public $editable = false;
    public $gestionador = false;
    public $subidaErrores = '';
    public $pasarAAprobada = false;
    public $pasarAFirmada = false;

    public function mount($precarga, $propuestas)
    {
        $this->elem = $precarga;
        $this->estadoActualSlug = $precarga->precargaEstadoActual()->slug;

        $this->editable = false;
        $this->gestionador = false;

        $roles = auth()->user()->getRoleNames();
        $esAdmin = false;
        $this->estadoActual = $this->elem->precargaEstadoActual();
        if (is_null($this->estadoActual)) {
            $this->estadoActual = (new PrecargaEstadoManager)->getBySlug('iniciada');
        }

        foreach ($roles as $k => $v) {
            if (!$esAdmin) {
                switch ($roles[0]) {
                    case config('tools.constants.roles.administrador'):
                        $esAdmin = true;
                        $gestionador = true;
                        switch ($this->estadoActual->slug) {
                            case 'iniciada':
                            case 'observada':
                                if ($this->elem->user_id == Auth::user()->id) {
                                    $this->editable = true;
                                }
                                break;
                        }
                        break;
                    case config('tools.constants.roles.empleado'):
                        switch ($this->estadoActual->slug) {
                            case 'iniciada':
                            case 'observada':
                                if ($this->elem->user_id == Auth::user()->id) {
                                    $this->editable = true;
                                }
                                break;
                            default:
                                $this->gestionador = true;
                                break;
                        }
                        break;
                    case config('tools.constants.roles.comercial'):
                        switch ($this->estadoActual->slug) {
                            case 'iniciada':
                            case 'observada':
                                $this->editable = true;
                                break;
                        }
                        break;
                }
            }
        }
        if ($this->gestionador) {
            $this->tipo_documento = 1;
        } else {
            $this->tipo_documento = 2;
        }

        /* $listado = new Collection();
        foreach($propuestas as $propuesta) {
            if ($this->gestionador) {
                if (is_null($propuesta->legajo_sin_firma)) {
                    $listado->add($propuesta);
                }
            } elseif ($this->elem->user_id == auth()->user()->id) {
                if ((!is_null($propuesta->legajo_sin_firma)) && (is_null($propuesta->legajo_con_firma))) {
                    $listado->add($propuesta);
                }
            }
        } */

        $this->propuestas = $propuestas;
        if ($this->propuestas->count() == 1) {
            $this->propuesta_id = $this->propuestas->first()->id;
        }

        $totalPropuestas = $propuestas->count();
        $cantidadCompletas = 0;
        if ($this->gestionador) {
            foreach ($propuestas as $propuestaOriginal) {
                if (!is_null($propuestaOriginal->legajo_sin_firma)) {
                    $cantidadCompletas += 1;
                }
            }
            if ($totalPropuestas == $cantidadCompletas)
                $this->pasarAAprobada = true;
        } elseif ($this->elem->user_id == auth()->user()->id) {
            foreach ($propuestas as $propuestaOriginal) {
                if (!is_null($propuestaOriginal->legajo_con_firma)) {
                    $cantidadCompletas += 1;
                }
            }
            if (($totalPropuestas == $cantidadCompletas) && ($this->estadoActual->slug == 'aprobada'))
                $this->pasarAFirmada = true;
        }
    }

    public function hydrate()
    {
        $this->subidaErrores = '';
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function save()
    {
        if ($this->propuesta_id == 0) {
            $this->subidaErrores = 'Debe seleccionar una propuesta.';
        } else {
            $this->subidaErrores = '';
            $propuesta = Propuesta::find($this->propuesta_id);
            foreach ($this->filesLegajos as $file) {
                $fileName = $file->store('/', 'documents');
                if ($this->tipo_documento == 2) {
                    $propuesta->legajo_con_firma = $fileName;
                    $propuesta->save();
                    $estadoActual = $this->elem->precargaEstadoActual();
                    $propuestasOriginal = (new PrecargaManager)->getPropuestas($this->elem, $estadoActual);
                    $totalPropuestas = $propuestasOriginal->count();
                    $cantidadCompletas = 0;
                    foreach ($propuestasOriginal as $propuestaOriginal) {
                        if (!is_null($propuestaOriginal->legajo_con_firma)) {
                            $cantidadCompletas += 1;
                        }
                    }
                    if ($totalPropuestas == $cantidadCompletas)
                        $this->pasarAFirmada = true;
                } else if ($this->tipo_documento == 1) {
                    $propuesta->legajo_sin_firma = $fileName;
                    $propuesta->save();
                    $estadoActual = $this->elem->precargaEstadoActual();
                    $propuestasOriginal = (new PrecargaManager)->getPropuestas($this->elem, $estadoActual);
                    $totalPropuestas = $propuestasOriginal->count();
                    $cantidadCompletas = 0;
                    foreach ($propuestasOriginal as $propuestaOriginal) {
                        if (!is_null($propuestaOriginal->legajo_sin_firma)) {
                            $cantidadCompletas += 1;
                        }
                    }
                    if ($totalPropuestas == $cantidadCompletas)
                        $this->pasarAAprobada = true;
                }

            }
            return redirect()->to('/precargas/editar/' . (new \App\Tools\HelpersTool)->helper_getUrlHash($this->elem->id) . '/legajo');
        }
    }

    public function changeState($estadoSlug)
    {
        switch ($estadoSlug) {
            case 'rechazada':
            case 'aprobada':
                $this->pasarAAprobada = false;
                break;
            case 'firmada':
                $this->pasarAFirmada = false;
                break;
        }
        $precargaEstadoRelacion = (new PrecargaManager)->changeState($this->elem, $estadoSlug);
        return redirect()->to(url('/precargas/editar/' . (new \App\Tools\HelpersTool)->helper_getUrlHash($this->elem->id) . '/precarga'));
    }

    public function removeUploads()
    {
        $this->dispatchBrowserEvent('removeall');
    }

    public function render()
    {
        return view('livewire.precarga-subir-legajo');
    }

}
