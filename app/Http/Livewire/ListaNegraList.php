<?php

namespace App\Http\Livewire;

use App\Models\ListaNegra;
use Livewire\Component;
use Livewire\WithPagination;

class ListaNegraList extends Component
{
    use WithPagination;

    public $nombre = '';
    public $apellido = '';
    public $ordenarPor = 'apellido';
    public $sentido = 'ASC';

    protected $queryString = ['ordenarPor', 'sentido'];

    public function sortBy($field) {
        $this->sentido = $this->ordenarPor === $field
            ? $this->sentido === 'ASC' ? 'DESC' : 'ASC'
            : 'ASC';
    }

    public function updatingNombre()
    {
        $this->setPage(1);
    }

    public function cleanSort() {
        $this->apellido = '';
    }

    public function render()
    {
        return view('livewire.lista-negra-list', [
            'listasNegras' => ListaNegra::from('lista_negra')
                ->join('documento_tipos', 'lista_negra.documento_tipo_id', 'documento_tipos.id')
                ->join('lista_negra_motivos', 'lista_negra.lista_negra_motivo_id', 'lista_negra_motivos.id')
                ->search('lista_negra.apellido', $this->apellido)
                ->addSelect(['lista_negra.id','documento_tipos.nombre as tipoDocNombre', 'lista_negra.numero_doc',
                    'lista_negra.apellido as listaApellido', 'lista_negra.nombre as listaNombre', 'lista_negra.fecha', 'lista_negra.observacion',
                    'lista_negra_motivos.nombre as listaNegraNombre'])
                ->orderBy('listaNombre', $this->sentido)
                ->paginate(10),

        ]);
    }
}
