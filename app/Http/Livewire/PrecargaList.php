<?php

namespace App\Http\Livewire;

use App\Managers\PrecargaManager;
use App\Models\PrecargaEstado;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class PrecargaList extends Component
{
    use WithPagination;

    public $nombre = '';
    public $comercialNombre = '';
    public $email = '';
    public $reparticion = '';
    public $selectEstado = 'todos';
    public $estadosPrecarga = [];
    public $selectColaborador = 'todos';
    public $colaboradores = [];
    public $ordenarPor = 'created_at';
    public $sentido = 'DESC';
    public $userPerfil = '';
    public $users = [];
    public $buscar = false;

    protected $queryString = ['ordenarPor', 'sentido'];

    public function mount() {
        $this->nombre = request()->get('nombre');
        $this->comercialNombre = request()->get('comercialNombre');
        $this->selectEstado = request()->get('selectEstado');
        $this->selectColaborador = request()->get('selectColaborador');
        $this->userPerfil = auth()->user()->perfil();
        $this->estadosPrecarga = PrecargaEstado::orderBy('nombre')
            ->get()
            ->pluck('nombre', 'slug')
            ->toArray();
        $this->colaboradores = DB::table('users')
            ->join('model_has_roles', 'model_has_roles.model_id', 'users.id')
            ->where('model_has_roles.role_id', '<=', 2)
            ->orderBy('name')
            ->select('users.id', 'users.name')
            ->pluck('name', 'id')
            ->toArray();
        $this->users = DB::table('users')
            ->join('model_has_roles', 'model_has_roles.model_id', 'users.id')
            ->where('model_has_roles.role_id', '<=', 2)
            ->orderBy('name')
            ->select('users.id', 'users.name')
            ->pluck('name', 'id')
            ->toArray();
    }

    public function sortBy($field) {
        $this->sentido = $this->ordenarPor === $field
            ? $this->sentido === 'ASC' ? 'DESC' : 'ASC'
            : 'ASC';
    }

    public function cleanSort() {
        $this->nombre = '';
        $this->email = '';
        $this->reparticion = '';
        $this->selectEstado = 'todos';
        $this->comercialNombre = '';
    }

    public function updatingNombre()
    {
        $this->resetPage();
        $this->gotoPage(1);
    }

    public function search()
    {
        $this->buscar = true;
    }

    public function updatingEmail()
    {
        $this->resetPage();
        $this->gotoPage(1);
    }

    public function render()
    {
        $precargas = (new PrecargaManager)->getFilterItems($this->nombre, $this->email, $this->reparticion, $this->selectEstado, $this->ordenarPor, $this->sentido, $this->comercialNombre, $this->selectColaborador);
        $this->buscar = false;
        return view('livewire.precarga-list', [
            'precargas' => $precargas,
            'userRol' => auth()->user()->rolPrincipal(),
            'userPerfil' => $this->userPerfil
        ]);
    }
}
