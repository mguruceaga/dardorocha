<?php

namespace App\Http\Livewire;

use App\Managers\GrillaManager;
use App\Models\Grilla;
use Illuminate\Validation\Rule;
use Livewire\Component;

class GrillaEdit extends Component
{
    public $grilla = '';
    public $nombre = '';
    public $abreviatura = '';
    public $grillas = [];

    protected $rules = [
        'grilla.nombre' => 'required',
        'grilla.abreviatura' => 'required',
    ];

    protected $messages = [
        'grilla.nombre.required' => 'La grilla es requerida.',
        'grilla.abreviatura.required' => 'La abreviatura es requerida.',
        'grilla.nombre.unique' => 'Ya existe una grilla con este nombre',
    ];

    /**
     * Funciones relacionadas al Front
     */
    public function mount($grillaId = 0)
    {
        if ($grillaId != 0) {
            $this->edit($grillaId);
        }else{
            $this->create();
        }

    }

    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function updatedGrillaNombre() {
        $this->validateOnly('grilla.nombre');
    }

    public function render()
    {
        return view('livewire.grilla-edit');
    }

    /**
     * Funciones relacionadas con ABM
     */
    public function create() {
        $this->grilla = new Grilla();
    }

    public function edit($grillaId) {
        $this->grilla = (new GrillaManager())->getItem($grillaId);
        $this->nombre = $this->grilla->nombre;
        $this->abreviatura = $this->grilla->abreviatura;
    }

    public function save()
    {
        $this->validate([
            'grilla.nombre' => [
                'required',
                Rule::unique('grillas','nombre')->ignore($this->grilla->id),
            ],
            'grilla.abreviatura' => [
                'required',
                Rule::unique('grillas','abreviatura')->ignore($this->grilla->id),
            ],
        ]);

        (new GrillaManager)->saveItem($this->grilla);

        return redirect()->to('/grillas');
        // $this->emit('notify-saved');

    }
}
