<?php

namespace App\Http\Livewire;

use App\Managers\PrecargaEstadoManager;
use App\Managers\PrecargaManager;
use App\Managers\PropuestaManager;
use App\Managers\ToolsManager;
use App\Models\Banco;
use App\Models\CodigoPostal;
use App\Models\ContratacionTipo;
use App\Models\Documentacion;
use App\Models\DocumentacionAdjunta;
use App\Models\DocumentoTipo;
use App\Models\Genero;
use App\Models\Pais;
use App\Models\Reparticion;
use App\Models\Sector;
use App\Models\GrupoPago;
use App\Models\PrestamoTipo;
use App\Tools\FormatsTool;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithFileUploads;

class PrecargaEdit extends Component
{
    use WithFileUploads;

    public $files = [];
    public $showModalUpload = false;

    public $elem = '';
    public $tab = '';
    public $message = '';
    public $propuestas = [];
    public $documentoTipos = [];
    public $generos = [];
    public $paises = [];
    public $sectores = [];
    public $contratacionTipos = [];
    public $grupoPagoId = 0;
    public $gruposPagos = [];
    public $zonas = [];
    public $zonaId = 0;
    public $reparticiones = [];
    public $reparticionId = 0;
    public $reparticionNombre = '';
    public $bancos = [];
    public $bancoId = 0;
    public $bancoNombre = '';
    public $codigosPostales = [];
    public $codigoPostalId = 0;
    public $localidadNombre = '';
    public $prestamoTipos = [];
    public $userPerfil = '';
    public $reparticionGrupo = 0;
    public $enviar_revision = 0;
    public $mensajeError = 'Se produjeron errores en el guardado';
    public $formularios = [];

    public $propuestasTodasConfirmadasMensaje = '';
    public $propuestasTodasConfirmadas = false;

    public $propuestasTodasFirmadasMensaje = '';
    public $propuestasTodasFirmadas = false;

    public $editable = 0;
    public $gestionador = 0;
    public $estadoActual = 0;
    public $estaEnListaNegra = 0;
    public $fechaNacimientoMinimo = '';

    public $radioFondista = 1;
    public $checkOpcionales = [];

    public $estados = [];

    public $errorGeneral = '';

    protected $rules = [

        'elem.documento_tipo_id' => 'required|gt:0',
        'elem.numero_doc' => 'required',
        'elem.cuil_cuit' => 'required',
        'elem.nombre' => 'required',
        'elem.apellido' => 'required',
        'elem.fecha_nacimiento' => 'required',

        'elem.genero_id' => 'required|gt:0',
        'elem.pais_id' => '',

        'elem.sector_id' => 'required|gt:0',
        'grupoPagoId' => 'required|gt:0',
        'zonaId' => 'required|gt:0',
        'reparticionId' => 'required|gt:0',
        'elem.dependencia' => '',

        'elem.cuota_maxima' => '',
        'elem.plazo_maximo' => '',
        'elem.monto_maximo' => '',

        'elem.contratacion_tipo_id' => '',
        'elem.ingreso_laboral' => '',
        'elem.antiguedad' => '',
        'bancoId' => '',
        'elem.cbu' => 'digits:22',

        'elem.email' => 'email',
        'elem.prefijo_movil' => 'nullable|regex:/^[0-9]{2,4}+$/',
        'elem.telefono_movil' => 'nullable|regex:/^[0-9]{6,8}+$/',
        'elem.prefijo_fijo' => 'nullable|regex:/^[0-9]{2,4}+$/',
        'elem.telefono_fijo' => 'nullable|regex:/^[0-9]{6,8}+$/',
        'elem.calle' => '',
        'elem.numero' => '',
        'elem.piso' => '',
        'elem.departamento' => '',
        'codigoPostalId' => '',

    ];


    protected $messages = [

        'elem.documento_tipo_id.required' => 'Es requerido.',
        'elem.documento_tipo_id.gt' => 'Es requerido.',
        'elem.numero_doc.required' => 'Es requerido.',
        'elem.cuil_cuit.required' => 'Es requerido.',
        'elem.nombre.required' => 'Es requerido.',
        'elem.apellido.required' => 'Es requerido.',
        'elem.fecha_nacimiento.required' => 'Es requerido.',
        'elem.fecha_nacimiento.date' => 'Debe ser una fecha válida.',
        'elem.genero_id.required' => 'Es requerido.',
        'elem.genero_id.gt' => 'Es requerido.',

        'elem.sector_id.required' => 'Es requerido.',
        'elem.sector_id.gt' => 'Es requerido.',
        'grupoPagoId.required' => 'Es requerido.',
        'grupoPagoId.gt' => 'Es requerido.',
        'zonaId.required' => 'Es requerido.',
        'zonaId.gt' => 'Es requerido.',
        'reparticionId.required' => 'Es requerido.',
        'reparticionId.gt' => 'Es requerido.',
        'elem.dependencia.required' => 'Es requerido.',

        'elem.contratacion_tipo_id.required' => 'Es requerido.',
        'elem.contratacion_tipo_id.gt' => 'Es requerido.',
        'elem.ingreso_laboral.required' => 'Es requerido.',
        'elem.antiguedad.required' => 'Es requerida.',
        'bancoId.required' => 'Es requerido.',
        'elem.cbu.required' => 'Es requerido.',
        'elem.cbu.digits' => 'El CBU debe tener 22 digitos.',

        'elem.email.required' => 'Es requerido.',
        'elem.email.email' => 'Debe ser un correo electrónico válido.',
        'codigoPostalIdd.required' => 'Es requerido.',

        'elem.prefijo_movil.regex' => 'El <strong>Prefijo Telefónico</strong> debe tener entre 2 y 4 números',
        'elem.telefono_movil.regex' => 'El <strong>Teléfono</strong> debe tener entre 6 y 8 números',
        'elem.prefijo_fijo.regex' => 'El <strong>Prefijo Celular</strong> debe tener entre 2 y 4 números',
        'elem.telefono_fijo.regex' => 'El <strong>Celular</strong> debe tener entre 6 y 8 números',

    ];


    public function mount($precargaId = 0, $tab = 'precarga')
    {

        $this->estados = (new FormatsTool)->arregloEstados();

        $this->fechaNacimientoMinimo = Carbon::now()->subYears(18)->format('Y-m-d');
        $this->userPerfil = auth()->user()->perfil();
        $this->tab = $tab;
        $this->elem = (new PrecargaManager)->newItem();
        if ($precargaId != 0) {
            $this->elem = (new PrecargaManager)->getItem($precargaId);
            $this->grupoPagoId = $this->elem->grupo_pago_id;
            $this->zonaId = $this->elem->zona_id;
            $this->reparticionId = $this->elem->reparticion_id;
            $reparticion = Reparticion::find($this->reparticionId);
            $this->reparticionNombre = $reparticion->nombre;
            $this->codigoPostalId = $this->elem->codigo_postal_id;
            if (!is_null($this->codigoPostalId)) {
                $codigoPostal = CodigoPostal::find($this->codigoPostalId);
                $this->localidadNombre = $codigoPostal->localidad;
            }
            $this->bancoId = $this->elem->banco_id;
            if (!is_null($this->bancoId)) {
                $banco = Banco::find($this->bancoId);
                $this->bancoNombre = $banco->nombre;
            }
            $this->elem->cbu = substr($this->elem->cbu, 0, 8) . ' ' . substr($this->elem->cbu, 8, 14);
            $propuestas = (new PropuestaManager)->getItemFromPrecarga($this->elem->id, true);
            $this->propuestasTodasConfirmadas = true;
            $this->propuestasTodasFirmadas = true;
            foreach($propuestas as $prop) {
                if (!$prop->legajo_confirmado) {
                    $this->propuestasTodasConfirmadas = false;
                }
                if (!$prop->legajo_firmado) {
                    $this->propuestasTodasFirmadas = false;
                }
            }
            if ($this->propuestasTodasConfirmadas) {
                if ($propuestas->count() == 1) {
                    $this->propuestasTodasConfirmadasMensaje = 'La precarga se encuentra en estado de ser Autorizada porque tiene <br/>confirmado el legajo correspondiente a la propuesta realizada.';
                } else {
                    $this->propuestasTodasConfirmadasMensaje = 'La precarga se encuentra en estado de ser Autorizada porque tiene <br/>confirmados los legajos correspondientes a las propuestas realizadas.';
                }
            }
            if ($this->propuestasTodasFirmadas) {
                if ($propuestas->count() == 1) {
                    $this->propuestasTodasFirmadasMensaje = 'La precarga se encuentra en estado de ser enviada firmada porque tiene <br/>asociado el legajo firmado correspondiente a la propuesta realizada.';
                } else {
                    $this->propuestasTodasFirmadasMensaje = 'La precarga se encuentra en estado de ser enviada firmada porque tiene <br/>asociados los legajos firmados correspondientes a las propuestas realizadas.';
                }
            }
        } else {
            $this->elem->user_id = Auth::user()->id;
        }
        $this->documentoTipos = DocumentoTipo::orderBy('abreviatura')->get()->pluck('abreviatura', 'id')->toArray();
        $this->generos = Genero::orderBy('nombre')->get()->pluck('nombre', 'id')->toArray();
        $this->paises = Pais::orderBy('nombre')->get()->pluck('nombre', 'id')->toArray();
        $this->sectores = Sector::orderBy('descripcion')->get()->pluck('descripcion', 'id')->toArray();
        $this->gruposPagos = GrupoPago::orderBy('descripcion')->get()->pluck('descripcion', 'id')->toArray();
        $this->contratacionTipos = ContratacionTipo::orderBy('descripcion')->get()->pluck('descripcion', 'id')->toArray();
        $this->bancos = Banco::orderBy('nombre')->get()->pluck('nombre', 'id')->toArray();
        $this->prestamoTipos = PrestamoTipo::orderBy('nombre')->get()->pluck('nombre', 'id')->toArray();

        $this->editable = false;
        $this->gestionador = false;

        $roles = auth()->user()->getRoleNames();
        $esAdmin = false;
        $this->estadoActual = $this->elem->precargaEstadoActual();
        if (is_null($this->estadoActual)) {
            $this->estadoActual = (new PrecargaEstadoManager)->getBySlug('iniciada');
        }

        foreach ($roles as $k => $v) {
            if (!$esAdmin) {
                switch ($roles[0]) {
                    case config('tools.constants.roles.administrador'):
                    case config('tools.constants.roles.tesorero'):
                        $esAdmin = true;
                        $gestionador = true;
                        switch ($this->estadoActual->slug) {
                            case 'iniciada':
                            case 'observada':
                                if ($this->elem->user_id == Auth::user()->id) {
                                    $this->editable = true;
                                }
                                break;
                        }
                        break;
                    case config('tools.constants.roles.empleado'):
                        switch ($this->estadoActual->slug) {
                            case 'iniciada':
                            case 'observada':
                                if ($this->elem->user_id == Auth::user()->id) {
                                    $this->editable = true;
                                }
                                break;
                            default:
                                $this->gestionador = true;
                                break;
                        }
                        break;
                    case config('tools.constants.roles.comercial'):
                        switch ($this->estadoActual->slug) {
                            case 'iniciada':
                            case 'observada':
                                $this->editable = true;
                                break;
                        }
                        break;
                }
            }
        }

        $this->estaEnListaNegra = false;
        if ($this->gestionador) {
            $this->estaEnListaNegra = (new PrecargaManager)->estaEnListaNegra($this->elem);
        }

        $this->elem->documentos = new \Illuminate\Support\Collection();

    }

    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function reparticionSelect($value)
    {
        $reparticion = Reparticion::find($value);
        $this->reparticionId = $reparticion->id;
        $this->reparticionNombre = $reparticion->nombre;
    }

    public function clearReparticionSelect()
    {
        $this->reparticionId = 0;
        $this->reparticionNombre = '';
    }

    public function bancoSelect($value)
    {
        $banco = Banco::find($value);
        $this->bancoId = $banco->id;
        $this->bancoNombre = $banco->nombre;
    }

    public function clearBancoSelect()
    {
        $this->bancoId = 0;
        $this->bancoNombre = '';
    }

    public function localidadSelect($value)
    {
        $localidad = CodigoPostal::find($value);
        $this->codigoPostalId = $localidad->id;
        $this->localidadNombre = $localidad->localidad;
    }

    public function clearLocalidadSelect()
    {
        $this->codigoPostalId = 0;
        $this->localidadNombre = '';
    }

    public function updatedElemCuilCuit($value) {

        if (($this->elem->numero_doc != '') && ($this->elem->cuil_cuit != '')) {
            if (strlen($this->elem->cuil_cuit) == 13)
                $this->validate([
                    'elem.cuil_cuit' => 'dni_in_cuil_cuit:'.$this->elem->numero_doc,
                    'elem.cuil_cuit' => 'cuit_valid',
                ], [
                    'elem.cuil_cuit.dni_in_cuil_cuit' => 'El CUIT/CUIL debe incluir el Número de Documento.',
                    'elem.cuil_cuit.cuit_valid' => 'El CUIT/CUIL debe ser válido.',
                ]);
        }
    }

    public function updatedGrupoPagoId($value) {
        $this->zonaId = 0;
        $this->reparticionId = 0;
        $this->reparticionNombre = '';
        $this->zonas = DB::table('zonas')
            ->join('zona_por_grupo_pagos', 'zona_por_grupo_pagos.zona_id', 'zonas.id')
            ->where('zona_por_grupo_pagos.grupo_pago_id', $this->grupoPagoId)
            ->orderBy('zonas.nombre')
            ->select('zonas.id', 'zonas.nombre')
            ->get()
            ->pluck('nombre', 'id')
            ->toArray();
    }

    public function updatedZonaId($value) {
        $this->reparticionId = 0;
        $this->reparticionNombre = '';
    }

    public function updatedElemEmail($value) {
        $this->validateOnly('elem.email');
    }

    public function save($enviarARevision)
    {
        $this->enviar_revision = $enviarARevision;
        $this->elem->cbu = str_replace(' ', '', $this->elem->cbu);

        $this->elem->grupo_pago_id = $this->grupoPagoId;
        $this->elem->zona_id = $this->zonaId;
        $this->elem->reparticion_id = $this->reparticionId;

        $this->elem->codigo_postal_id = $this->codigoPostalId;

        $this->elem->banco_id = $this->bancoId;

        /**
         * Convertir números con decimales con , en número con decimales con .
         */
        $this->elem->cuota_maxima = (new FormatsTool)->prepareToSave('decimal', $this->elem->cuota_maxima);
        if ($this->elem->plazo_maximo == '') {
            $this->elem->plazo_maximo = null;
        }
        $this->elem->monto_maximo = (new FormatsTool)->prepareToSave('decimal', $this->elem->monto_maximo);

        if ($this->elem->ingreso_laboral == '')
            $this->elem->ingreso_laboral = null;
        else
            $this->elem->ingreso_laboral = (new FormatsTool)->prepareToSave('decimal', $this->elem->ingreso_laboral);

        $this->rules['elem.cuil_cuit'] = 'dni_in_cuil_cuit:'.$this->elem->numero_doc;
        $this->rules['elem.cuil_cuit'] = 'cuit_valid';
        $this->rules['elem.fecha_nacimiento'] = 'nullable|before_or_equal:'.Carbon::now()->subYears(18)->format("Y-m-d");
        $this->messages['elem.cuil_cuit.dni_in_cuil_cuit'] = 'El CUIT/CUIL debe incluir el Número de Documento.';
        $this->messages['elem.cuil_cuit.cuit_valid'] = 'El CUIT/CUIL debe ser válido.';

        $data = $this->validate();

        /**
         * Valida las condiciones de la repartición antes de guardar la precarga.
         */
        $revisionAutomatica = (new PrecargaManager)->validarCondiciones($this->elem, $enviarARevision);

        if ($revisionAutomatica['success'] == 'error') {
            $this->errorGeneral = $revisionAutomatica['message'];
        } else {
            $esNueva = isset($this->elem->id) ? false : true;
            $precarga = (new PrecargaManager)->saveItem($this->elem, $revisionAutomatica);
            if ($esNueva) {
                return redirect()->to('/precargas/editar/'.(new \App\Tools\HelpersTool)->helper_getUrlHash($precarga->id).'/precarga');
            } else {
                return redirect()->to('/precargas');
            }
        }

    }

    public function render()
    {
        if (is_null($this->estadoActual)) {
            $this->propuestas = collect();
        } else {
            $this->propuestas = (new PrecargaManager)->getPropuestas($this->elem, $this->estadoActual);
        }
        $antecedentes = (new PrecargaManager)->getItemsExcept($this->elem);

        if ($this->grupoPagoId != 0) {
            $this->zonas = DB::table('zonas')
                ->join('zona_por_grupo_pagos', 'zona_por_grupo_pagos.zona_id', 'zonas.id')
                ->where('zona_por_grupo_pagos.grupo_pago_id', $this->grupoPagoId)
                ->orderBy('zonas.nombre')
                ->select('zonas.id', 'zonas.nombre')
                ->get()
                ->pluck('nombre', 'id')
                ->toArray();
        } else {
            $this->zonas = [];
        }

        $this->reparticiones = [];
        if (($this->grupoPagoId != 0) && ($this->zonaId != 0)) {
            if (strlen($this->reparticionNombre) >= 3) {
                $this->reparticiones = DB::table('reparticion_zona_grupos')
                    ->join('reparticiones', 'reparticiones.id', 'reparticion_zona_grupos.reparticion_id')
                    ->where('reparticion_zona_grupos.grupo_pago_id', $this->grupoPagoId)
                    ->where('reparticion_zona_grupos.zona_id', $this->zonaId)
                    ->where('reparticiones.nombre', 'LIKE', '%' . $this->reparticionNombre . '%')
                    ->orderBy('reparticiones.nombre')
                    ->select('reparticiones.id', 'reparticiones.nombre')
                    ->get()
                    ->pluck('nombre', 'id')
                    ->toArray();
            }
        }

        if (strlen($this->localidadNombre) >= 3) {
            $this->codigosPostales = CodigoPostal::where('localidad', 'LIKE', '%' . $this->localidadNombre . '%')
                ->orderBy('localidad')
                ->get()
                ->pluck('localidad', 'id')
                ->toArray();
        } else {
            $this->codigosPostales = [];
        }

        if (strlen($this->bancoNombre) >= 3) {
            $this->bancos = Banco::where('nombre', 'LIKE', '%' . $this->bancoNombre . '%')
                ->orderBy('nombre')
                ->get()
                ->pluck('nombre', 'id')
                ->toArray();
        } else {
            $this->bancos = [];
        }

        return view('livewire.precarga-edit', [
            'precarga' => $this->elem,
            'estadoActualSlug' => $this->estadoActual->slug,
            'userPerfil' => $this->userPerfil,
            'mensajeError' => $this->mensajeError,
            'editable' => $this->editable,
            'gestionador' => $this->gestionador,
            'estaEnListaNegra' => $this->estaEnListaNegra,
            'antecedentes' => $antecedentes,
        ]);
    }

    public function changeState($estadoSlug)
    {
        try {
            $this->errorGeneral = '';
            $respuestaAutomatica = [];
            $respuestaAutomatica['success'] = 'ok';
            switch ($estadoSlug) {
                case 'iniciada':
                case 'en_revision':
                case 'preaprobada':
                    $this->elem->cbu = str_replace(' ', '', $this->elem->cbu);
                    /**
                     * Convertir números con decimales con , en número con decimales con .
                     */
                    $this->elem->cuota_maxima = (new FormatsTool)->prepareToSave('decimal', $this->elem->cuota_maxima);
                    if ($this->elem->plazo_maximo == '') {
                        $this->elem->plazo_maximo = null;
                    }
                    $this->elem->monto_maximo = (new FormatsTool)->prepareToSave('decimal', $this->elem->monto_maximo);

                    if ($this->elem->ingreso_laboral == '')
                        $this->elem->ingreso_laboral = null;
                    else
                        $this->elem->ingreso_laboral = (new FormatsTool)->prepareToSave('decimal', $this->elem->ingreso_laboral);

                    $this->rules['elem.cuil_cuit'] = 'dni_in_cuil_cuit:' . $this->elem->numero_doc;
                    $this->messages['elem.cuil_cuit.dni_in_cuil_cuit'] = 'El CUIT/CUIL debe incluir el Número de Documento.';
                    $this->rules['elem.cuil_cuit'] = 'cuit_valid';
                    $this->messages['elem.cuil_cuit.cuit_valid'] = 'El CUIT/CUIL debe ser válido.';

                    if ($this->elem->cuil_cuit != '') {
                        $this->rules['elem.genero_id'] = 'genero_cuit:' . $this->elem->cuil_cuit;
                        $this->messages['elem.genero_id.genero_cuit'] = 'El Género no coincide con el CUIT/CUIL ingresado.';
                    }

                    $data = $this->validate();

                    /**
                     * Valida las condiciones de la repartición antes de guardar la precarga.
                     */
                    $revisionAutomatica = (new PrecargaManager)->validarCondiciones($this->elem, $this->enviar_revision);
                    break;

                case 'aceptada':
                case 'autorizada':

                    $cantidadDocumentacion = DocumentacionAdjunta::where('precarga_id', $this->elem->id)
                        ->distinct()
                        ->select('documentacion_id')
                        ->groupBy('documentacion_id')
                        ->get()
                        ->count();
                    $documentaciones = Documentacion::all()->count();
                    if ($cantidadDocumentacion != $documentaciones) {
                        $respuestaAutomatica = [];
                        $respuestaAutomatica['success'] = 'error';
                        $respuestaAutomatica['message'] = 'La Documentación debe estar completa para continuar.';
                    }
                    break;
            }

            if ($respuestaAutomatica['success'] == 'error') {
                $this->errorGeneral = $respuestaAutomatica['message'];
            } else {
                $precargaEstadoRelacion = (new PrecargaManager)->changeState($this->elem, $estadoSlug);
                return redirect()->to(url('/precargas/editar/' . (new \App\Tools\HelpersTool)->helper_getUrlHash($this->elem->id) . '/precarga'));
            }

        } catch (\Exception $e) {
            $this->errorGeneral = $e->getMessage();
        }

    }

    public function nada()
    {
        return false;
    }

    public function mostrarGuardar()
    {
        $this->showModalUpload = true;
    }

    public function mostrarUploadModal($formularioId)
    {
        // $this->formularioFileUpload = $formularioId;
        $this->showModalUpload = true;
    }

    public function ocultarUploadModal()
    {
        // $this->formularioFileUpload = 0;
        $this->showModalUpload = false;
        // $this->dispatchBrowserEvent('removeall');
    }

}
