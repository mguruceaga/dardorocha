<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Condicion;
use App\Managers\CondicionManager;
use App\Managers\ReparticionManager;
use Illuminate\Validation\Rule;

class CondicionEdit extends Component
{
    public $condicion = '';
    public $nombre = '';
    public $reparticiones = [];

    protected $rules = [
        'condicion.nombre' => 'required',
        'condicion.haber' => 'required',
        'condicion.cbu' => 'required',
        'condicion.relacion_dependencia' => 'required',
        'condicion.edad_max_hombre' => 'nullable',
        'condicion.edad_max_mujer' => 'nullable',
        'condicion.antiguedad' => 'nullable',
        'condicion.cuotas_minimas_haber' => 'nullable',
        'condicion.cuotas_maximas_haber' => 'nullable',
        'condicion.cuotas_minimas_cbu' => 'nullable',
        'condicion.cuotas_maximas_cbu' => 'nullable',
        'condicion.monto_minimo' => 'nullable',
        'condicion.monto_maximo' => 'nullable',
        'condicion.observacion' => 'nullable',
        //'condicion.reparticion_id' => 'required|gt:0',
    ];

    protected $messages = [
        'condicion.nombre.required' => 'El nombre es requerido.',
        'condicion.nombre.unique' => 'Ya existe este nombre',
    ];

    /**
     * Funciones relacionadas al Front
     */
    public function mount($condicionId = 0)
    {
        if ($condicionId != 0) {
            $this->edit($condicionId);
        }else{
            $this->create();
        }
        $this->reparticiones = (new ReparticionManager)->getItems('nombre')->pluck('nombre', 'id')->toArray();
    }

    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function updatedCondicionNombre() {
        $this->validateOnly('condicion.nombre');
    }

    public function render()
    {
        return view('livewire.condicion-edit');
    }

    /**
     * Funciones relacionadas con ABM
     */
    public function create() {
        $this->condicion = new Condicion();
    }

    public function edit($condicionId) {
        $this->condicion = (new CondicionManager)->getItem($condicionId);
        $this->nombre = $this->condicion->nombre;
    }

    public function save()
    {
        $this->validate([
            'condicion.nombre' => [
                'required',
                Rule::unique('condiciones','nombre')->ignore($this->condicion->id),
            ],
        ]);

        (new CondicionManager)->saveItem($this->condicion);

        return redirect()->to('/condiciones');
        // $this->emit('notify-saved');

    }

    public function delete($condicionId){
        (new CondicionManager)->delete($condicionId);
    }

}
