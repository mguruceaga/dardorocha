<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BancoController;
use App\Http\Controllers\PrecargaController;
use App\Http\Controllers\ReparticionController;
use App\Http\Controllers\FormularioController;
use App\Http\Controllers\ZonaController;
use App\Http\Controllers\CondicionPorReparticionController;
use App\Http\Controllers\SelectController;
use App\Http\Controllers\ArchivoController;
use App\Http\Controllers\FondistaController;
use App\Http\Controllers\GrillaController;
use App\Http\Controllers\ListaNegraController;
use App\Http\Controllers\UsuarioController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test/{lineaId}/{reparticionId}/{fondistaId}', [FormularioController::class, 'test'])
    ->name('test');

Route::middleware(['auth:sanctum', 'verified', 'primer.ingreso'])->group(function () {

    Route::get('/', [PrecargaController::class, 'index']);

    Route::get('/dashboard', [PrecargaController::class, 'index'])
        ->name('dashboard');

    //precargas
    Route::get('/precargas', [PrecargaController::class, 'index'])
        ->name('precargas');
    Route::get('/precargas/crear', [PrecargaController::class, 'create'])
        ->name('precargas.crear');
    Route::get('/precargas/editar/{id}/{tab}', [PrecargaController::class, 'edit'])
        ->name('precargas.editar');

});

Route::middleware(['auth:sanctum', 'verified', 'administracion', 'primer.ingreso'])->group(function () {

    //archivos
    Route::get('/archivos', [ArchivoController::class, 'index'])
        ->name('archivos');
    Route::get('/archivos/crear', [ArchivoController::class, 'create'])
        ->name('archivos.crear');
    Route::get('/archivos/editar/{id}', [ArchivoController::class, 'edit'])
        ->name('archivos.editar');

    //bancos
    Route::get('/bancos', [BancoController::class, 'index'])
        ->name('bancos');
    Route::get('/bancos/crear', [BancoController::class, 'create'])
        ->name('bancos.crear');
    Route::get('/bancos/editar/{id}', [BancoController::class, 'edit'])
        ->name('bancos.editar');

    //listaNegra
    Route::get('/listaNegra', [ListaNegraController::class, 'index'])
        ->name('listaNegra');
    Route::get('/listaNegra/crear', [ListaNegraController::class, 'create'])
        ->name('listaNegra.crear');
    Route::get('/listaNegra/editar/{id}', [ListaNegraController::class, 'edit'])
        ->name('listaNegra.editar');

    //condiciones
    Route::get('/condiciones', [CondicionPorReparticionController::class, 'index'])
        ->name('condiciones');
    Route::get('/condiciones/crear', [CondicionPorReparticionController::class, 'create'])
        ->name('condiciones.crear');
    Route::get('/condiciones/editar/{id}', [CondicionPorReparticionController::class, 'edit'])
        ->name('condiciones.editar');

    //fondistas
    Route::get('/fondistas', [FondistaController::class, 'index'])
        ->name('fondistas');
    Route::get('/fondistas/crear', [FondistaController::class, 'create'])
        ->name('fondistas.crear');
    Route::get('/fondistas/editar/{id}', [FondistaController::class, 'edit'])
        ->name('fondistas.editar');

    //grillas
    Route::get('/grillas', [GrillaController::class, 'index'])
        ->name('grillas');
    Route::get('/grillas/crear', [GrillaController::class, 'create'])
        ->name('grillas.crear');
    Route::get('/grillas/editar/{id}', [GrillaController::class, 'edit'])
        ->name('grillas.editar');

    //reparticiones
    Route::get('/reparticiones', [ReparticionController::class, 'index'])
        ->name('reparticiones');
    Route::get('/reparticiones/crear', [ReparticionController::class, 'create'])
        ->name('reparticiones.crear');
    Route::get('/reparticiones/editar/{id}', [ReparticionController::class, 'edit'])
        ->name('reparticiones.editar');

    //zonas
    Route::get('/origenes', [ZonaController::class, 'index'])
        ->name('origenes');
    Route::get('/origenes/crear', [ZonaController::class, 'create'])
        ->name('origenes.crear');
    Route::get('/origenes/editar/{id}', [ZonaController::class, 'edit'])
        ->name('origenes.editar');
    Route::get('/origenes/borrar/{id}', [ZonaController::class, 'borrar'])
        ->name('origenes.borrar');

    //zonas
    Route::get('/usuarios', [UsuarioController::class, 'index'])
        ->name('usuarios');
    Route::get('/usuarios/crear', [UsuarioController::class, 'create'])
        ->name('usuarios.crear');
    Route::get('/usuarios/editar/{id}', [UsuarioController::class, 'edit'])
        ->name('usuarios.editar');
    Route::get('/usuarios/borrar/{id}', [UsuarioController::class, 'borrar'])
        ->name('usuarios.borrar');

});

Route::get('/form', function() {
   return view('forms.form');
});

Route::get('/imprimir', [FormularioController::class, 'procesarFormulario']);
