<x-app-layout>

    <x-slot name="header">
        <div class="sm:flex-1 sm:flex sm:items-center sm:justify-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Solicitudes') }}
            </h2>
            <div>
                <div class="flex space-x-2">
                    <a href="{{ route('precargas.crear') }}" class="text-indigo-600 hover:text-indigo-900">Nueva solicitud</a>
                    <div class="flex inline-block">
                        <a href="{{ route('precargas') }}"
                            class="inline-flex items-center justify-center w-6 h-6 mr-1 transition-colors duration-150 rounded-full focus:shadow-outline border border-indigo-500 text-indigo-500 hover:bg-indigo-500 hover:text-white">
                            <span class="relative inline-block">
                              <svg class="h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                <path fill-rule="evenodd" d="M4 2a1 1 0 011 1v2.101a7.002 7.002 0 0111.601 2.566 1 1 0 11-1.885.666A5.002 5.002 0 005.999 7H9a1 1 0 010 2H4a1 1 0 01-1-1V3a1 1 0 011-1zm.008 9.057a1 1 0 011.276.61A5.002 5.002 0 0014.001 13H11a1 1 0 110-2h5a1 1 0 011 1v5a1 1 0 11-2 0v-2.101a7.002 7.002 0 01-11.601-2.566 1 1 0 01.61-1.276z" clip-rule="evenodd" />
                              </svg>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @livewire('precarga-list')
        </div>
    </div>

</x-app-layout>
