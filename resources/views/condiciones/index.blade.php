<x-app-layout>

    <x-slot name="header">
        <div class="sm:flex-1 sm:flex sm:items-center sm:justify-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Condiciones') }}
                <p class="mt-1 text-sm text-gray-600"> Listado de condiciones </p>
            </h2>
            <div>
                <a href="{{ route('condiciones.crear') }}" class="text-indigo-600 hover:text-indigo-900">Nueva condición</a>
            </div>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @livewire('condicion-list')
        </div>
    </div>

</x-app-layout>