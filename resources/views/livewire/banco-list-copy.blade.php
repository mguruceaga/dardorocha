
<div class="flex flex-col">
    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">

        <div class="grid grid-cols-6 gap-6 mb-4 lg:px-8">
            <div class="col-span-6 sm:col-span-4">
            </div>
            <div class="col-span-6 sm:col-span-2">
                <x-custom.forms.input.find wire:model="search"/>
            </div>
        </div>

        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">

            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">

                <table class="min-w-full divide-y divide-gray-200">
                    <thead class="bg-gray-50">

                    <tr>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Código
                        </th>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Nombre
                        </th>

                        <th scope="col" class="relative px-6 py-3">
                            <span class="sr-only">Editar</span>
                        </th>

                    </tr>
                    </thead>

                   <tbody class="bg-white divide-y divide-gray-200">
                    @foreach($bancos as $banco)
                        <tr>
                            <td class="px-6 py-4 whitespace-nowrap">
                                <div class="text-base font-medium text-gray-900"> <strong>{{ $banco->codigo }}</strong> </div>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap">
                                <div class="text-base font-medium text-gray-900"> <strong>{{ $banco->nombre }}</strong> </div>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                <a href="{{ route('bancos.editar', $banco->id) }}" class="text-indigo-600 hover:text-indigo-900">Editar</a>
                            </td>
                        </tr>
                    @endforeach
                    <!-- More rows... -->
                   </tbody>
                </table>

            </div>

        </div>

    </div>

    <div class="px-6 py-4 bg-white shadow rounded-b-md">
        @php($paginator = $bancos->links()->paginator)
        @php($elements = $bancos->links()->elements)
        @if ($paginator->hasPages())
            <x-pagination wire:model="resetPagination" :paginator="$paginator" :elements="$elements" />
        @endif
    </div>

</div>



