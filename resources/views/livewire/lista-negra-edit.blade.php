<div>
    <form wire:submit.prevent="save" autocomplete="off">

        <div class="shadow overflow-hidden sm:rounded-md">
            <div class="px-4 py-6 bg-white sm:p-6">

                <div class="px-4 sm:px-0">
                    <h3 class="text-lg font-medium leading-6 text-gray-900">Lista Negra</h3>
                </div>

                <div class="hidden sm:block" aria-hidden="true">
                    <div class="py-5">
                        <div class="border-t border-gray-200"></div>
                    </div>
                </div>

                <div class="grid grid-cols-6 gap-6 mb-4">
                    <div class="col-span-6 sm:col-span-3">
                        <x-custom.forms.input.group label="Tipo de Documento" for="documento_tipo_id" :error="$errors->first('listaNegra.documento_tipo_id')">
                            <x-custom.forms.input.select
                                wire:model.defer="listaNegra.documento_tipo_id" name="documento_tipo_id" id="documento_tipo_id"
                                :error="$errors->first('listaNegra.documento_tipo_id')"
                                :options="$tipoDocs"
                                />
                        </x-custom.forms.input.group>
                    </div>

                    <div class="col-span-6 sm:col-span-3">
                        <x-custom.forms.input.group label="Número de Documento" for="numero_doc" :error="$errors->first('listaNegra.numero_doc')">
                            <x-custom.forms.input.number
                                wire:model.lazy="listaNegra.numero_doc" name="numero_doc" id="numero_doc" mask="[00]000000" scale="0" thousandsSeparator=""
                                :error="$errors->first('listaNegra.numero_doc')"
                            />
                        </x-custom.forms.input.group>
                    </div>

                    <div class="col-span-6 sm:col-span-2">
                        <x-custom.forms.input.group label="Apellido" for="apellido" :error="$errors->first('listaNegra.apellido')">
                            <x-custom.forms.input.text
                                wire:model.defer="listaNegra.apellido" name="apellido" id="apellido" >
                            </x-custom.forms.input.text>
                        </x-custom.forms.input.group>
                    </div>

                    <div class="col-span-6 sm:col-span-2">
                        <x-custom.forms.input.group label="Nombre" for="nombre" :error="$errors->first('listaNegra.nombre')">
                            <x-custom.forms.input.text
                                wire:model.defer="listaNegra.nombre" name="nombre" id="nombre" >
                            </x-custom.forms.input.text>
                        </x-custom.forms.input.group>
                    </div>

                    <div class="col-span-6 sm:col-span-3">
                        <x-custom.forms.input.group label="Motivo" for="lista_negra_motivo_id" :error="$errors->first('listaNegra.lista_negra_motivo_id')">
                            <x-custom.forms.input.select
                                wire:model.defer="listaNegra.lista_negra_motivo_id" name="lista_negra_motivo_id" id="lista_negra_motivo_id"
                                :error="$errors->first('listaNegra.lista_negra_motivo_id')"
                                :options="$listasNegras"
                            />
                        </x-custom.forms.input.group>
                    </div>

                    <div class="col-span-6 sm:col-span-2">
                        <x-custom.forms.input.group label="Fecha" for="fecha" :error="$errors->first('listaNegra.fecha')">
                            <x-custom.forms.input.date
                                wire:model.defer="listaNegra.fecha" name="fecha" id="fecha"
                                :error="$errors->first('listaNegra.fecha')"
                                />
                        </x-custom.forms.input.group>
                    </div>

                    <div class="col-span-6 sm:col-span-6">
                        <x-custom.forms.input.group label="Observaciones" for="observacion" :error="$errors->first('listaNegra.observacion')">
                            <x-custom.forms.input.text
                                wire:model.defer="listaNegra.observacion" name="observacion" id="observacion" >
                            </x-custom.forms.input.text>
                        </x-custom.forms.input.group>
                    </div>

                </div>

                <div class="hidden sm:block" aria-hidden="true">
                    <div class="py-5">
                        <div class="border-t border-gray-200"></div>
                    </div>
                </div>

                <div class="text-right sm:px-6">
                    <a href="{{ url()->previous() }}" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-indigo hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Cancelar
                    </a>
                    <x-custom.forms.buttons.primary color="indigo">
                        Guardar
                    </x-custom.forms.buttons.primary>
                </div>

            </div>
        </div>

    </form>
</div>

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/pikaday.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script src="{{ asset('js/pikaday.js') }}"></script>
    <script src="{{ asset('js/imask.js') }}" type="text/javascript"></script>
    <script>

    </script>
@endpush
