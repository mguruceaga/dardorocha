<div class="flex flex-row
    {{ $padding ? 'pt-1 md-10 md:ml-16' : '' }}">
    <x-custom.icon.circle-letters fillColor="{{ $mensaje->user->color() }}">
        {{ $mensaje->user->iniciales() }}
    </x-custom.icon.circle-letters>
    <div class="flex-col mt-1">
        <div class="flex items-center flex-1 px-4 font-bold leading-tight">{{ $mensaje->user->name }}
            <span class="ml-2 text-xs font-normal text-gray-500">{{ \Carbon\Carbon::parse($mensaje->created_at)->format('d-m-Y h:m:s') }}</span>
            @if (!$mensaje->leido())
                <span class="ml-4 p-1 rounded-md text-xs text-gray-600 bg-{{ $mensaje->user->color() }}-300">Nuevo</span>
            @endif
        </div>
        <div class="flex-1 px-2 ml-2 text-sm font-medium leading-loose text-gray-600">
            {{ $mensaje->texto }}
        </div>
    </div>
</div>

<!-- rounded-md bg-{ $respuesta->user->color() }}-300 -->