
<div class="flex flex-col">
    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <x-custom.table>
                    <x-slot name="head">
                        <x-custom.table.heading sortable wire:click="sortBy('apellido')" :direction="$ordenarPor === 'apellido' ? $sentido : null">
                            Solicitante
                        </x-custom.table.heading>
                        @if ($userRol->name != config('tools.constants.roles.comercial'))
                            <x-custom.table.heading>
                                Comercial
                            </x-custom.table.heading>
                        @endif
                        @if ($userRol->name == config('tools.constants.roles.comercial'))
                            <x-custom.table.heading>
                                Información Laboral
                            </x-custom.table.heading>
                        @endif
                        <x-custom.table.heading>
                            Estado
                        </x-custom.table.heading>
                        @if ($userRol->name == config('tools.constants.roles.administrador'))
                            <x-custom.table.heading>
                                Colaborador
                            </x-custom.table.heading>
                        @endif
                        <x-custom.table.heading>
                            Acción
                        </x-custom.table.heading>
                    </x-slot>

                    <x-slot name="body">
                        <x-custom.table.row wire:loading.class.delay="opacity-50" class="bg-gra">
                            <form action="{{ route('precargas') }}" method="GET">
                                <x-custom.table.cell>
                                    <input wire:model.defer="nombre" name="nombre" autocomplete="nope" type="text" value="{{ $nombre }}"
                                           class="rounded-none flex-1 form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading -5" />
                                </x-custom.table.cell>
                                @if ($userRol->name == config('tools.constants.roles.comercial'))
                                    <x-custom.table.cell>
                                    </x-custom.table.cell>
                                @else
                                    <x-custom.table.cell>
                                        <input wire:model.defer="comercialNombre" name="comercialNombre" autocomplete="nope" type="text" value="{{ $comercialNombre }}"
                                               class="rounded-none flex-1 form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading -5" />
                                    </x-custom.table.cell>
                                @endif
                                <x-custom.table.cell>
                                    <select wire:model.defer="selectEstado"
                                        name="selectEstado" id="selectEstado"
                                        class="select2 mt-1 block w-full py-2 px-3
                                                border bg-white rounded-md shadow-sm
                                                focus:outline-none focus:ring-indigo-500
                                                focus:border-indigo-500 sm:text-sm">
                                        <option value="todos" {{ ($selectEstado == 'todos') ? ' selected="selected"' : '' }}>
                                            Todas
                                        </option>
                                        @foreach ($estadosPrecarga as $k => $v)
                                            <option value="{{ $k }}" {{ $selectEstado == $k ? ' selected="selected"' : '' }}>{{ $v }}</option>
                                        @endforeach
                                    </select>
                                </x-custom.table.cell>
                                @if ($userRol->name == config('tools.constants.roles.administrador'))
                                    <x-custom.table.cell>
                                        <select wire:model.defer="selectColaborador"
                                                name="selectColaborador" id="selectColaborador"
                                                class="select2 mt-1 block w-full py-2 px-3
                                                border bg-white rounded-md shadow-sm
                                                focus:outline-none focus:ring-indigo-500
                                                focus:border-indigo-500 sm:text-sm">
                                            <option value="todos" {{ ($selectColaborador == 'todos') ? ' selected="selected"' : '' }}>
                                                Todos
                                            </option>
                                            @foreach ($colaboradores as $k => $v)
                                                <option value="{{ $k }}" {{ $selectColaborador == $k ? ' selected="selected"' : '' }}>{{ $v }}</option>
                                            @endforeach
                                        </select>
                                    </x-custom.table.cell>
                                @endif
                                <x-custom.table.cell>
                                    <button type="submit"
                                       class="inline-flex items-center justify-center w-10 h-10 mr-2 transition-colors duration-150 rounded-full focus:shadow-outline border border-indigo-500 text-indigo-500 hover:bg-indigo-500 hover:text-white">
                                        <span class="relative inline-block">
                                          <x-custom.icon.small-search />
                                        </span>
                                    </button>
                                    <a href="{{ route('precargas') }}"
                                       class="inline-flex items-center justify-center w-10 h-10 mr-2 transition-colors duration-150 rounded-full focus:shadow-outline border border-indigo-500 text-indigo-500 hover:bg-indigo-500 hover:text-white">
                                        <span class="relative inline-block">
                                          <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                            <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                                           </svg>
                                        </span>
                                    </a>
                                </x-custom.table.cell>
                            </form>
                        </x-custom.table.row>
                        @forelse($precargas as $precarga)
                            <livewire:precarga-list-item :precargaId="$precarga->id" :userRol="$userRol" :userPerfil="$userPerfil" />
                        @empty
                            <x-custom.table.row wire:loading.class.delay="opacity-50">
                                <x-custom.table.cell colspan="5">
                                    <div class="flex justify-center items-center space-x-2 text-cool-gray-400">
                                        <x-custom.icon.small-search-document />
                                        <span class="font-medium py-8 text-cool-gray-400 text-xl">
                                            No se encontraron solicitudes...
                                        </span>
                                    </div>
                                </x-custom.table.cell>
                                @if ($userRol->name == config('tools.constants.roles.administrador'))
                                    <x-custom.table.cell>
                                    </x-custom.table.cell>
                                @endif
                            </x-custom.table.row>
                        @endforelse
                    </x-slot>
                </x-custom.table>
                <div class="px-6 py-4 bg-white shadow rounded-b-md">
                    {!! $precargas->links('pagination::tailwind') !!}
                </div>
            </div>
        </div>
    </div>
</div>
