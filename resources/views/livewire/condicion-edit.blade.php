<div>
    <form wire:submit.prevent="save" autocomplete="off">

        <div class="shadow overflow-hidden sm:rounded-md">
            <div class="px-4 py-6 bg-white sm:p-6">

                <div class="px-4 sm:px-0">
                    <h3 class="text-lg font-medium leading-6 text-gray-900">Condición</h3>
                </div>

                <div class="hidden sm:block" aria-hidden="true">
                    <div class="py-5">
                        <div class="border-t border-gray-200"></div>
                    </div>
                </div>

                <div class="grid grid-cols-6 gap-6 mb-4">
                    <div class="col-span-6 sm:col-span-6">
                        <x-custom.forms.input.group label="Nombre" for="nombre" :error="$errors->first('condicion.nombre')">
                            <x-custom.forms.input.text   
                                wire:model.defer="condicion.nombre" name="nombre" id="nombre"
                                :error="$errors->first('condicion.nombre')" /><!-- codigo php lo que esta entre "" y empieza con : -->
                        </x-custom.forms.input.group>
                    </div>
                </div>
               
                <div class="grid grid-cols-6 gap-6 mb-4">
                    <div class="col-span-6 sm:col-span-1">
                        <x-custom.forms.input.group label="Haber" for="haber" :error="$errors->first('condicion.haber')">
                            <x-custom.forms.input.checkbox   
                                wire:model.defer="condicion.haber" name="haber" id="haber" value="21"
                                :value="$condicion->haber">
                            </x-custom.forms.input.checkbox>
                        </x-custom.forms.input.group>
                    </div>
                    
                    <div class="col-span-6 sm:col-span-1">
                        <x-custom.forms.input.group label="CBU" for="cbu" :error="$errors->first('condicion.cbu')">
                            <x-custom.forms.input.checkbox   
                                wire:model.defer="condicion.cbu" name="cbu" id="cbu" value="31"
                                :value="$condicion->cbu">
                            </x-custom.forms.input.checkbox>
                        </x-custom.forms.input.group>
                    </div><!-- para que vaya en misma linea que check haber? -->
                    
                    <div class="col-span-6 sm:col-span-1">
                        <x-custom.forms.input.group label="Inactivo" for="relacion_dependencia" :error="$errors->first('condicion.relacion_dependencia')">
                            <x-custom.forms.input.checkbox   
                                wire:model.defer="condicion.relacion_dependencia"
                                name="relacion_dependencia" id="relacion_dependencia" value="1"
                                :value="$condicion->relacion_dependencia">
                            </x-custom.forms.input.checkbox>
                        </x-custom.forms.input.group>
                    </div>
                </div>    
                
                <div class="grid grid-cols-6 gap-6 mb-4">
                    <div class="col-span-6 sm:col-span-1">
                        <x-custom.forms.input.group label="Edad máxima hombre" for="edad_max_hombre" :error="$errors->first('condicion.edad_max_hombre')">
                            <x-custom.forms.input.text   
                                wire:model.defer="condicion.edad_max_hombre" name="edad_max_hombre" id="edad_max_hombre" >
                            </x-custom.forms.input.text>
                        </x-custom.forms.input.group>
                    </div>
                    
                    <div class="col-span-6 sm:col-span-1">
                        <x-custom.forms.input.group label="Edad máxima mujer" for="edad_max_mujer" :error="$errors->first('condicion.edad_max_mujer')">
                            <x-custom.forms.input.text   
                                wire:model.defer="condicion.edad_max_mujer" name="edad_max_mujer" id="edad_max_mujer" >
                            </x-custom.forms.input.text>
                        </x-custom.forms.input.group>
                    </div>
                    
                    <div class="col-span-6 sm:col-span-1">
                        <x-custom.forms.input.group label="Cuotas mínimas haber" for="cuotas_minimas_haber" :error="$errors->first('condicion.cuotas_minimas_haber')">
                            <x-custom.forms.input.text   
                                wire:model.defer="condicion.cuotas_minimas_haber" name="cuotas_minimas_haber" id="cuotas_minimas_haber" >
                            </x-custom.forms.input.text>
                        </x-custom.forms.input.group>
                    </div>
                    
                    <div class="col-span-6 sm:col-span-1">
                        <x-custom.forms.input.group label="Cuotas máximas haber" for="cuotas_maximas_haber" :error="$errors->first('condicion.cuotas_maximas_haber')">
                            <x-custom.forms.input.text   
                                wire:model.defer="condicion.cuotas_maximas_haber" name="cuotas_maximas_haber" id="cuotas_maximas_haber" >
                            </x-custom.forms.input.text>
                        </x-custom.forms.input.group>
                    </div>
                    
                    <div class="col-span-6 sm:col-span-1">
                        <x-custom.forms.input.group label="Cuotas mínimas cbu" for="cuotas_minimas_cbu" :error="$errors->first('condicion.cuotas_minimas_cbu')">
                            <x-custom.forms.input.text   
                                wire:model.defer="condicion.cuotas_minimas_cbu" name="cuotas_minimas_cbu" id="cuotas_minimas_cbu" >
                            </x-custom.forms.input.text>
                        </x-custom.forms.input.group>
                    </div>
                    
                    <div class="col-span-6 sm:col-span-1">
                        <x-custom.forms.input.group label="Cuotas máximas cbu" for="cuotas_maximas_cbu" :error="$errors->first('condicion.cuotas_maximas_cbu')">
                            <x-custom.forms.input.text   
                                wire:model.defer="condicion.cuotas_maximas_cbu" name="cuotas_maximas_cbu" id="cuotas_maximas_cbu" >
                            </x-custom.forms.input.text>
                        </x-custom.forms.input.group>
                    </div>
                    
                    <div class="col-span-6 sm:col-span-1">
                        <x-custom.forms.input.group label="Monto mínimo" for="monto_minimo" :error="$errors->first('condicion.monto_minimo')">
                            <x-custom.forms.input.text   
                                wire:model.defer="condicion.monto_minimo" name="monto_minimo" id="monto_minimo" >
                            </x-custom.forms.input.text>
                        </x-custom.forms.input.group>
                    </div>
                    
                    <div class="col-span-6 sm:col-span-1">
                        <x-custom.forms.input.group label="Monto máximo" for="monto_maximo" :error="$errors->first('condicion.monto_maximo')">
                            <x-custom.forms.input.text   
                                wire:model.defer="condicion.monto_maximo" name="monto_maximo" id="monto_maximo" >
                            </x-custom.forms.input.text>
                        </x-custom.forms.input.group>
                    </div>
                    
                    <div class="col-span-6 sm:col-span-1">
                        <x-custom.forms.input.group label="Antigüedad" for="antiguedad" :error="$errors->first('condicion.antiguedad')">
                            <x-custom.forms.input.text   
                                wire:model.defer="condicion.antiguedad" name="antiguedad" id="antiguedad" >
                            </x-custom.forms.input.text>
                        </x-custom.forms.input.group>
                    </div>
                    
                    <div class="col-span-6 sm:col-span-3">
                        <x-custom.forms.input.group label="Observaciones" for="observacion" :error="$errors->first('condicion.observacion')">
                            <x-custom.forms.input.text   
                                wire:model.defer="condicion.observacion" name="observacion" id="observacion" >
                            </x-custom.forms.input.text>
                        </x-custom.forms.input.group>
                    </div>
                    
                </div>
                
                <!-- <div class="grid grid-cols-6 gap-6 mb-4">
                    <div class="col-span-6 sm:col-span-6">
                        <x-custom.forms.input.group label="Repartición" for="reparticion_id" :error="$errors->first('condicion.reparticion_id')">
                            <x-custom.forms.input.select   
                                wire:model.defer="condicion.reparticion_id" name="reparticion_id" id="reparticion_id"
                                :options="$reparticiones" >                                
                            </x-custom.forms.input.select>
                        </x-custom.forms.input.group>
                    </div>
                </div> -->
                
                

                
                <div class="hidden sm:block" aria-hidden="true">
                    <div class="py-5">
                        <div class="border-t border-gray-200"></div>
                    </div>
                </div>

                <div class="text-right sm:px-6">
                    <a href="{{ url()->previous() }}" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-indigo hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Cancelar
                    </a>
                    <x-custom.forms.buttons.primary color="indigo">
                        Guardar
                    </x-custom.forms.buttons.primary>
                </div>

            </div>
        </div>

    </form>
</div>

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/pikaday.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script src="{{ asset('js/pikaday.js') }}"></script>
    <script src="{{ asset('js/imask.js') }}" type="text/javascript"></script>
    <script>

    </script>
@endpush
