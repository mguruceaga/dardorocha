<div>
    <div
        x-data="{
                      openTab: '{{ $tab }}',
                      activeClasses: 'border-l border-t border-r rounded-t text-blue-700 border-gray-100 bg-white',
                      inactiveClasses: 'text-gray-600 hover:text-blue-800 bg-gray-100',
                      'errors': {{ json_encode(array_keys($errors->getMessages())) }},
                      focusField(input){
                          fieldError = Array.from(document.getElementsByName(input));
                          if(fieldError.length > 0){
                            fieldError[0].focus({preventScroll:false});
                          }
                      },
            }"
        x-init="() => { $watch('errors', value => focusField(value[0])) }"
        class="bg-white"
    >
        <ul class="flex border-b border-gray-100 bg-gray-100">
            <li @click="openTab = 'precarga'" :class="{ '-mb-px': openTab === 'precarga' }" class="-mb-px mr-1">
                <a :class="openTab === 'precarga' ? activeClasses : inactiveClasses"
                   class="bg-white inline-block py-2 px-4 font-semibold" href="#">
                    Solicitud
                </a>
            </li>
            @if (isset($precarga->id))
                <li @click="openTab = 'documentos'" :class="{ '-mb-px': openTab === 'documentos' }" class="-mb-px mr-1">
                    <a :class="openTab === 'documentos' ? activeClasses : inactiveClasses"
                       class="bg-white inline-block py-2 px-4 font-semibold" href="#">
                        Documentación
                    </a>
                </li>
            @endif
            @if (!$precarga->fondeada)
                @if ((($estadoActualSlug == 'en_revision') && ($gestionador)) || ($estadoActualSlug == 'preaprobada') || ($estadoActualSlug == 'aceptada'))
                    <li @click="openTab = 'propuesta'" :class="{ '-mb-px': openTab === 'propuesta' }" class="mr-1">
                        <a :class="openTab === 'propuesta' ? activeClasses : inactiveClasses"
                           class="inline-block py-2 px-4 font-semibold" href="#">
                            Propuesta
                        </a>
                    </li>
                @endif
            @else
                <li @click="openTab = 'legajo'" :class="{ '-mb-px': openTab === 'legajo' }" class="mr-1">
                    <a :class="openTab === 'legajo' ? activeClasses : inactiveClasses"
                       class="inline-block py-2 px-4 font-semibold" href="#">
                        Legajo
                    </a>
                </li>
            @endif
            @if ($antecedentes->count() > 0)
                @if($gestionador || ($precarga->user_id == auth()->user()->id))
                    <li @click="openTab = 'antecedentes'" :class="{ '-mb-px': openTab === 'antecedentes' }" class="mr-1">
                        <a :class="openTab === 'antecedentes' ? activeClasses : inactiveClasses"
                           class="inline-block py-2 px-4 font-semibold" href="#">
                            Antecedentes
                        </a>
                    </li>
                @endif
            @endif
            @if (isset($precarga->id))
                <li @click="openTab = 'mensajes'" :class="{ '-mb-px': openTab === 'mensajes' }" class="mr-1">
                    <a :class="openTab === 'mensajes' ? activeClasses : inactiveClasses"
                       class="inline-block py-2 px-4 font-semibold" href="#">
                        Mensajes
                    </a>
                </li>
            @endif
        </ul>

        <div class="w-full">
            <div x-show="openTab === 'precarga'">

                <div class="md:grid md:grid-cols-3 md:gap-6">
                    <div class="mt-5 md:mt-0 md:col-span-2">

                        @if ($gestionador)
                            @if ($estaEnListaNegra)
                                <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 relative" role="alert">
                                    <strong class="font-bold">El cliente se encuentra en lista negra.</strong>
                                </div>
                            @endif
                        @endif

                        <form id="formPrecarga" autocomplete="off">
                            <div class="shadow overflow-hidden sm:rounded-md z-10">
                                <div class="px-4 py-6 bg-white sm:p-6">

                                    <div class="px-4 sm:px-0">
                                        <h3 class="text-lg font-medium leading-6 text-gray-900">Datos Personales</h3>
                                    </div>

                                    <div class="hidden sm:block" aria-hidden="true">
                                        <div class="py-5">
                                            <div class="border-t border-gray-200"></div>
                                        </div>
                                    </div>

                                    <div class="grid grid-cols-6 gap-6 mb-4">
                                        <div class="col-span-6 sm:col-span-2">
                                            <x-custom.forms.input.group label="Tipo de Documento" for="documento_tipo_id" :error="$errors->first('elem.documento_tipo_id')">
                                                <x-custom.forms.input.select
                                                    wire:model.defer="elem.documento_tipo_id"
                                                    name="documento_tipo_id" id="documento_tipo_id"
                                                    :error="$errors->first('elem.documento_tipo_id')"
                                                    :options="$documentoTipos"
                                                    :disabled="!$editable" />
                                            </x-custom.forms.input.group>
                                        </div>
                                        <div class="col-span-6 sm:col-span-2">
                                            <x-custom.forms.input.group label="Número de Documento" for="numero_doc" :error="$errors->first('elem.numero_doc')">
                                                <x-custom.forms.input.number
                                                    wire:model.lazy="elem.numero_doc" name="numero_doc" id="numero_doc"
                                                    mask="[00]000000" scale="0" thousandsSeparator=""
                                                    :error="$errors->first('elem.numero_doc')"
                                                    :disabled="!$editable" />
                                            </x-custom.forms.input.group>
                                        </div>
                                        <div class="col-span-6 sm:col-span-2">
                                            <x-custom.forms.input.group label="Género" for="genero_id" :error="$errors->first('elem.genero_id')">
                                                <x-custom.forms.input.select
                                                    wire:model.defer="elem.genero_id" name="genero_id" id="genero_id"
                                                    :error="$errors->first('elem.genero_id')"
                                                    :options="$generos"
                                                    :disabled="!$editable"/>
                                            </x-custom.forms.input.group>
                                        </div>
                                    </div>

                                    <div class="grid grid-cols-6 gap-6 mb-4">
                                        <div class="col-span-6 sm:col-span-2">
                                            <x-custom.forms.input.group label="Apellido" for="apellido" :error="$errors->first('elem.apellido')">
                                                <x-custom.forms.input.text
                                                    wire:model.defer="elem.apellido" name="apellido" id="apellido"
                                                    :error="$errors->first('elem.apellido')"
                                                    :disabled="!$editable" />
                                            </x-custom.forms.input.group>
                                        </div>
                                        <div class="col-span-6 sm:col-span-2">
                                            <x-custom.forms.input.group label="Nombre" for="nombre" :error="$errors->first('elem.nombre')">
                                                <x-custom.forms.input.text
                                                    wire:model.defer="elem.nombre" name="nombre" id="nombre"
                                                    :error="$errors->first('elem.nombre')"
                                                    :disabled="!$editable" />
                                            </x-custom.forms.input.group>
                                        </div>
                                        <div class="col-span-6 sm:col-span-2">
                                            <x-custom.forms.input.group label="CUIL / CUIT" for="cuil_cuit" :error="$errors->first('elem.cuil_cuit')">
                                                <x-custom.forms.input.number
                                                    wire:model.lazy="elem.cuil_cuit" name="cuil_cuit" id="cuil_cuit"
                                                    mask="00-00000000-0" scale="0" thousandsSeparator=""
                                                    :error="$errors->first('elem.cuil_cuit')"
                                                    :disabled="!$editable" />
                                            </x-custom.forms.input.group>
                                        </div>
                                    </div>

                                    <div class="grid grid-cols-6 gap-6 mb-4">
                                        <div class="col-span-6 sm:col-span-2">
                                            <x-custom.forms.input.group label="Fecha de Nacimiento" for="fecha_nacimiento" :error="$errors->first('elem.fecha_nacimiento')">
                                                <x-custom.forms.input.date
                                                    wire:model.defer="elem.fecha_nacimiento" name="fecha_nacimiento" id="fecha_nacimiento"
                                                    :max="$fechaNacimientoMinimo"
                                                    :error="$errors->first('elem.fecha_nacimiento')"
                                                    :disabled="!$editable"/>
                                            </x-custom.forms.input.group>
                                        </div>
                                        <div class="col-span-6 sm:col-span-2">
                                            <x-custom.forms.input.group label="Nacido en" for="pais_id" :error="$errors->first('elem.pais_id')">
                                                <x-custom.forms.input.select
                                                    wire:model.defer="elem.pais_id" name="pais_id" id="pais_id"
                                                    :error="$errors->first('elem.pais_id')"
                                                    :options="$paises"
                                                    :disabled="!$editable"/>
                                            </x-custom.forms.input.group>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="shadow overflow-hidden sm:rounded-md z-0">
                                <div class="px-4 py-6 bg-white sm:p-6">

                                    <div class="px-4 sm:px-0">
                                        <h3 class="text-lg font-medium leading-6 text-gray-900">Lugar de Trabajo</h3>
                                        <p class="mt-1 text-sm text-gray-600">
                                        </p>
                                    </div>

                                    <div class="hidden sm:block" aria-hidden="true">
                                        <div class="py-5">
                                            <div class="border-t border-gray-200"></div>
                                        </div>
                                    </div>

                                    <x-custom.forms.input.hidden
                                        id="prestamo_id"
                                        name="prestamo_id"
                                        value="8" />

                                    <div class="grid grid-cols-6 gap-6 mb-4">
                                        <div class="col-span-6 sm:col-span-1">
                                            <x-custom.forms.input.group label="Sector" for="sector_id" :error="$errors->first('elem.sector_id')">
                                                <x-custom.forms.input.select
                                                    wire:model="elem.sector_id" name="sector_id" id="sector_id"
                                                    :error="$errors->first('elem.sector_id')"
                                                    :options="$sectores"
                                                    :disabled="!$editable"/>
                                            </x-custom.forms.input.group>
                                        </div>

                                        <div class="col-span-6 sm:col-span-2">
                                            <x-custom.forms.input.group label="Estado Laboral" for="grupo_pago_id" :error="$errors->first('grupoPagoId')">
                                                <x-custom.forms.input.select
                                                    wire:model="grupoPagoId"
                                                    name="grupo_pago_id"
                                                    id="grupo_pago_id"
                                                    :error="$errors->first('grupoPagoId')"
                                                    :options="$gruposPagos"
                                                    :disabled="!$editable"/>
                                            </x-custom.forms.input.group>
                                        </div>

                                        <div class="col-span-6 sm:col-span-3">
                                            <x-custom.forms.input.group label="Origen de la Repartición" for="zonaId" :error="$errors->first('zonaId')">
                                                <x-custom.forms.input.select
                                                    wire:model="zonaId" name="zonaId" id="zonaId"
                                                    :error="$errors->first('zonaId')"
                                                    :options="$zonas"
                                                    :disabled="!$editable"/>
                                            </x-custom.forms.input.group>
                                        </div>
                                    </div>

                                    <div class="grid grid-cols-6 gap-6 mb-4">
                                        <div class="col-span-6 sm:col-span-6">
                                            <x-custom.forms.input.group label="Repartición" for="reparticionNombre" :error="$errors->first('reparticionId')">
                                                <div class="flex rounded-md shadow-sm mt-1">
                                                    <input wire:model.debounce.500ms="reparticionNombre"
                                                           name="reparticionNombre" id="reparticionNombre"
                                                           placeholder="Ingrese al menos 3 caracteres."
                                                           autocomplete="nope" type="text"
                                                           {{ ($reparticionId != 0) ? ' disabled="disabled"' : '' }}
                                                           class="rounded-none rounded-l-md flex-1 form-input block w-full
                                                                    transition duration-150 ease-in-out sm:text-sm sm:leading -5
                                                           {{ $errors->first('reparticionId') ? ' border-red-500' : '' }}" />
                                                        @if ($reparticionId != 0)
                                                            <span wire:click.prevent="clearReparticionSelect()"
                                                                  class="inline-flex items-center px-3
                                                                      rounded-r-md border border-l-0
                                                                      border-gray-300 bg-gray-50
                                                                      text-gray-500 text-sm">
                                                                <x-custom.icon.small-trash />
                                                            </span>
                                                        @endif
                                                </div>
                                            </x-custom.forms.input.group>
                                        </div>
                                    </div>
                                    @if ($reparticionId == 0)
                                        @if (count($reparticiones) > 0)
                                            <div class="grid grid-cols-6 gap-6 mb-4">
                                                <div class="col-span-6 sm:col-span-6">
                                                    <div class="w-full p-3 border border-dashed border-gray-200 rounded" style="height: 150px; overflow: scroll">
                                                        @foreach ($reparticiones as $k => $v)
                                                            <a href="#" wire:click.prevent="reparticionSelect({{ $k }})" class="w-full bg-indigo-100 inline-flex items-center text-sm rounded mt-2 mr-1">
                                                                <p class="ml-2 mr-1 leading-relaxed truncate max-w-xs px-1">{{ $v }}</p>
                                                                <div class="py-2 px-1 w-8 h-8 inline-block align-middle text-gray-500 bg-indigo-200 focus:outline-none">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                                                        <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                                                    </svg>
                                                                </div>
                                                            </a>
                                                            <br/>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endif
                                </div>
                            </div>
{{--3/7/22 Gaby, cambio pedido x mail 25/05/2022 Asunto: Fw: SISTEMA DE PRECARGAS/SOLICITUDES--}}
{{--                            <div class="shadow overflow-hidden sm:rounded-md z-10">--}}
{{--                                <div class="px-4 py-6 bg-white sm:p-6">--}}

{{--                                    <div class="px-4 sm:px-0">--}}
{{--                                        <h3 class="text-lg font-medium leading-6 text-gray-900">Referencias de la solicitud</h3>--}}
{{--                                    </div>--}}

{{--                                    <div class="hidden sm:block" aria-hidden="true">--}}
{{--                                        <div class="py-5">--}}
{{--                                            <div class="border-t border-gray-200"></div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="grid grid-cols-6 gap-6 mb-4">--}}
{{--                                        <div class="col-span-6 sm:col-span-2">--}}
{{--                                            <x-custom.forms.input.group label="Monto máximo de cuota" for="cuota_maxima" :error="$errors->first('elem.cuota_maxima')">--}}
{{--                                                <div wire:ignore>--}}
{{--                                                    <x-custom.forms.input.number--}}
{{--                                                        wire:model.defer="elem.cuota_maxima" name="cuota_maxima" id="cuota_maxima" :currency="true"--}}
{{--                                                        mask="Number" scale="2" thousandsSeparator="."--}}
{{--                                                        :error="$errors->first('elem.cuota_maxima')"--}}
{{--                                                        :disabled="!$editable"/>--}}
{{--                                                </div>--}}
{{--                                            </x-custom.forms.input.group>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-span-6 sm:col-span-2">--}}
{{--                                            <x-custom.forms.input.group label="Plazo máximo del plan" for="plazo_maximo" :error="$errors->first('elem.plazo_maximo')">--}}
{{--                                                <div wire:ignore>--}}
{{--                                                    <x-custom.forms.input.number--}}
{{--                                                        wire:model.defer="elem.plazo_maximo" name="plazo_maximo" id="plazo_maximo" :currency="false"--}}
{{--                                                        mask="Number" scale="0" thousandsSeparator=""--}}
{{--                                                        :error="$errors->first('elem.plazo_maximo')"--}}
{{--                                                        :disabled="!$editable"/>--}}
{{--                                                </div>--}}
{{--                                            </x-custom.forms.input.group>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-span-6 sm:col-span-2">--}}
{{--                                            <x-custom.forms.input.group label="Monto máximo a solicitar" for="monto_maximo" :error="$errors->first('elem.monto_maximo')">--}}
{{--                                                <div wire:ignore>--}}
{{--                                                    <x-custom.forms.input.number--}}
{{--                                                        wire:model.defer="elem.monto_maximo" name="monto_maximo" id="monto_maximo" :currency="true"--}}
{{--                                                        mask="Number" scale="2" thousandsSeparator="."--}}
{{--                                                        :error="$errors->first('elem.monto_maximo')"--}}
{{--                                                        :disabled="!$editable"/>--}}
{{--                                                </div>--}}
{{--                                            </x-custom.forms.input.group>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                </div>--}}
{{--                            </div>--}}

                            <div class="shadow overflow-hidden sm:rounded-md z-40">
                                <div class="px-4 py-6 bg-white sm:p-6">

                                    <div class="px-4 sm:px-0">
                                        <h3 class="text-lg font-medium leading-6 text-gray-900">Datos Laborales</h3>
                                    </div>

                                    <div class="hidden sm:block" aria-hidden="true">
                                        <div class="py-5">
                                            <div class="border-t border-gray-200"></div>
                                        </div>
                                    </div>

                                    <div class="grid grid-cols-6 gap-6 mb-4">
                                        <div class="col-span-6 sm:col-span-2">
                                            <x-custom.forms.input.group label="Tipo Contratación" for="contratacion_tipo_id" :error="$errors->first('elem.contratacion_tipo_id')">
                                                <x-custom.forms.input.select
                                                    wire:model.defer="elem.contratacion_tipo_id" name="contratacion_tipo_id" id="contratacion_tipo_id"
                                                    :error="$errors->first('elem.contratacion_tipo_id')"
                                                    :options="$contratacionTipos"
                                                    :disabled="!$editable"/>
                                            </x-custom.forms.input.group>
                                        </div>
{{--3/7/22 Gaby, cambio pedido x mail 25/05/2022 Asunto: Fw: SISTEMA DE PRECARGAS/SOLICITUDES--}}
{{--                                        <div class="col-span-6 sm:col-span-2">--}}
{{--                                            <x-custom.forms.input.group label="Ingreso Laboral" for="ingreso_laboral" :error="$errors->first('elem.ingreso_laboral')">--}}
{{--                                                <div wire:ignore>--}}
{{--                                                    <x-custom.forms.input.number--}}
{{--                                                        wire:model.defer="elem.ingreso_laboral"--}}
{{--                                                        name="ingreso_laboral"--}}
{{--                                                        id="ingreso_laboral" :currency="true"--}}
{{--                                                        mask="Number" scale="2" thousandsSeparator="."--}}
{{--                                                        :error="$errors->first('elem.ingreso_laboral')"--}}
{{--                                                        :disabled="!$editable"/>--}}
{{--                                                </div>--}}
{{--                                            </x-custom.forms.input.group>--}}
{{--                                        </div>--}}
                                        <div class="col-span-6 sm:col-span-2">
                                            <x-custom.forms.input.group label="Antigüedad laboral" for="antiguedad" :error="$errors->first('elem.antiguedad')">
                                                <div>
                                                    <x-custom.forms.input.number
                                                        wire:model.defer="elem.antiguedad" name="antiguedad" id="antiguedad"
                                                        mask="00" scale="0" thousandsSeparator=""
                                                        :error="$errors->first('elem.antiguedad')"
                                                        :disabled="!$editable"/>
                                                    <p class="text-xs text-gray-700 mt-2">Colocar en años; si fuera menor a 1 año colocar 0.</p>
                                                </div>
                                            </x-custom.forms.input.group>
                                        </div>
                                    </div>

                                    <div class="grid grid-cols-6 gap-6 mb-4">
                                        <div class="col-span-6 sm:col-span-4">
                                            <x-custom.forms.input.group label="Banco" for="bancoNombre" :error="$errors->first('bancoId')">
                                                <div class="flex rounded-md shadow-sm mt-1">
                                                    <input wire:model.debounce.500ms="bancoNombre"
                                                           name="bancoNombre" id="bancoNombre"
                                                           placeholder="Ingrese al menos 3 caracteres."
                                                           autocomplete="nope" type="text"
                                                           {{ ($bancoId != 0) ? ' disabled="disabled"' : '' }}
                                                           class="rounded-none
                                                            @if ($bancoId != 0)
                                                               rounded-l-md
                                                            @else
                                                               rounded-md
                                                           @endif
                                                               flex-1 form-input block w-full
                                                               transition duration-150 ease-in-out sm:text-sm sm:leading -5
                                                                {{ $errors->first('bancoId') ? ' border-red-500' : '' }}" />
                                                    @if ($bancoId != 0)
                                                        <span wire:click.prevent="clearBancoSelect()"
                                                              class="inline-flex items-center px-3
                                                                      rounded-r-md border border-l-0
                                                                      border-gray-300 bg-gray-50
                                                                      text-gray-500 text-sm">
                                                                <x-custom.icon.small-trash />
                                                            </span>
                                                    @endif
                                                </div>
                                            </x-custom.forms.input.group>
                                            @if ($bancoId == 0)
                                                @if (count($bancos) > 0)
                                                    <div class="grid grid-cols-6 gap-6 mb-4">
                                                        <div class="col-span-6 sm:col-span-6">
                                                            <div class="w-full p-3 border border-dashed border-gray-200 rounded" style="height: 150px; overflow: scroll">
                                                                @foreach ($bancos as $k => $v)
                                                                    <a href="#" wire:click.prevent="bancoSelect({{ $k }})" class="w-full bg-indigo-100 inline-flex items-center text-sm rounded mt-2 mr-1">
                                                                        <p class="ml-2 mr-1 leading-relaxed truncate max-w-xs px-1">{{ $v }}</p>
                                                                        <div class="py-2 px-1 w-8 h-8 inline-block align-middle text-gray-500 bg-indigo-200 focus:outline-none">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                                                                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                                                            </svg>
                                                                        </div>
                                                                    </a>
                                                                    <br/>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif
                                        </div>
                                        <div class="col-span-6 sm:col-span-2">
                                            <x-custom.forms.input.group label="CBU" for="cbu" :error="$errors->first('elem.cbu')">
                                                <x-custom.forms.input.number
                                                    wire:model.defer="elem.cbu" name="cbu" id="cbu"
                                                    mask="00000000 00000000000000" scale="0" thousandsSeparator=""
                                                    :error="$errors->first('elem.cbu')"
                                                    :disabled="!$editable"/>
                                            </x-custom.forms.input.group>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="shadow overflow-hidden sm:rounded-md z-20">
                                <div class="px-4 py-6 bg-white sm:p-6">

                                    <div class="px-4 sm:px-0">
                                        <h3 class="text-lg font-medium leading-6 text-gray-900">Datos de Contacto del Solicitante del Préstamo</h3>
                                    </div>

                                    <div class="hidden sm:block" aria-hidden="true">
                                        <div class="py-5">
                                            <div class="border-t border-gray-200"></div>
                                        </div>
                                    </div>

                                    <div class="grid grid-cols-6 gap-6 mb-4">
                                        <div class="col-span-6 sm:col-span-6">
                                            <x-custom.forms.input.group label="Correo electrónico" for="email" :error="$errors->first('elem.email')">
                                                <x-custom.forms.input.text
                                                    wire:model.defer="elem.email" name="email" id="email"
                                                    :error="$errors->first('elem.email')"
                                                    :disabled="!$editable"/>
                                            </x-custom.forms.input.group>
                                        </div>
                                    </div>

                                    <div class="grid grid-cols-6 gap-6 mb-4">
                                        <div class="col-span-6 sm:col-span-3">
                                            <x-custom.forms.input.group label="Calle" for="calle" :error="$errors->first('elem.calle')">
                                                <x-custom.forms.input.text
                                                    wire:model.defer="elem.calle" name="calle" id="calle"
                                                    :error="$errors->first('elem.calle')"
                                                    :disabled="!$editable"/>
                                            </x-custom.forms.input.group>
                                        </div>
                                        <div class="col-span-6 sm:col-span-1">
                                            <x-custom.forms.input.group label="Número" for="numero" :error="$errors->first('elem.numero')">
                                                <x-custom.forms.input.text
                                                    wire:model.defer="elem.numero" name="numero" id="numero"
                                                    :error="$errors->first('elem.numero')"
                                                    :disabled="!$editable"/>
                                            </x-custom.forms.input.group>
                                        </div>
                                        <div class="col-span-6 sm:col-span-1">
                                            <x-custom.forms.input.group label="Piso" for="piso" :error="$errors->first('elem.piso')">
                                                <x-custom.forms.input.text
                                                    wire:model.defer="elem.piso" name="piso" id="piso"
                                                    :error="$errors->first('elem.piso')"
                                                    :disabled="!$editable"/>
                                            </x-custom.forms.input.group>
                                        </div>
                                        <div class="col-span-6 sm:col-span-1">
                                            <x-custom.forms.input.group label="Depto." for="departamento" :error="$errors->first('elem.departamento')">
                                                <x-custom.forms.input.text
                                                    wire:model.defer="elem.departamento" name="departamento" id="departamento"
                                                    :error="$errors->first('elem.departamento')"
                                                    :disabled="!$editable"/>
                                            </x-custom.forms.input.group>
                                        </div>
                                    </div>

                                    <div class="grid grid-cols-6 gap-6 mb-4">
                                        <div class="col-span-6 sm:col-span-3">
                                            <x-custom.forms.input.group label="Localidad" for="localidadNombre" :error="$errors->first('codigoPostalId')">
                                                <div class="flex rounded-md shadow-sm mt-1">
                                                    <input wire:model.debounce.500ms="localidadNombre"
                                                           placeholder="Ingrese al menos 3 caracteres."
                                                           name="localidadNombre" id="localidadNombre"
                                                           autocomplete="nope" type="text"
                                                           {{ ($codigoPostalId != 0) ? ' disabled="disabled"' : '' }}
                                                           class="rounded-none
                                                            @if ($codigoPostalId != 0)
                                                                rounded-l-md
                                                            @else
                                                               rounded-md
                                                           @endif
                                                            flex-1 form-input block w-full
                                                            transition duration-150 ease-in-out sm:text-sm sm:leading -5
                                                           {{ $errors->first('codigoPostalId') ? ' border-red-500' : '' }}" />
                                                    @if ($codigoPostalId != 0)
                                                        <span wire:click.prevent="clearLocalidadSelect()"
                                                              class="inline-flex items-center px-3
                                                                      rounded-r-md border border-l-0
                                                                      border-gray-300 bg-gray-50
                                                                      text-gray-500 text-sm">
                                                                <x-custom.icon.small-trash />
                                                            </span>
                                                    @endif
                                                </div>
                                            </x-custom.forms.input.group>
                                            @if ($codigoPostalId == 0)
                                                @if (count($codigosPostales) > 0)
                                                    <div class="grid grid-cols-6 gap-6 mb-4">
                                                        <div class="col-span-6 sm:col-span-6">
                                                            <div class="w-full p-3 border border-dashed border-gray-200 rounded" style="height: 150px; overflow: scroll">
                                                                @foreach ($codigosPostales as $k => $v)
                                                                    <a href="#" wire:click.prevent="localidadSelect({{ $k }})" class="w-full bg-indigo-100 inline-flex items-center text-sm rounded mt-2 mr-1">
                                                                        <p class="ml-2 mr-1 leading-relaxed truncate max-w-xs px-1">{{ $v }}</p>
                                                                        <div class="py-2 px-1 w-8 h-8 inline-block align-middle text-gray-500 bg-indigo-200 focus:outline-none">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                                                                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                                                            </svg>
                                                                        </div>
                                                                    </a>
                                                                    <br/>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-6 gap-6 mb-4">

                                        <div class="col-span-6 sm:col-span-1">
                                            <x-custom.forms.input.group label="Prefijo móvil" for="prefijo_movil" :error="$errors->first('elem.prefijo_movil')">
                                                <x-custom.forms.input.phone-mask
                                                    wire:model.defer="elem.prefijo_movil" name="prefijo_movil" id="prefijo_movil"
                                                    mask="[00]00" scale="0" thousandsSeparator=""
                                                    :error="$errors->first('elem.prefijo_movil')"
                                                    :disabled="!$editable"/>
                                            </x-custom.forms.input.group>
                                        </div>
                                        <div class="col-span-6 sm:col-span-2">
                                            <x-custom.forms.input.group label="Teléfono móvil" for="telefono_movil" :error="$errors->first('elem.telefono_movil')">
                                                <x-custom.forms.input.phone-mask
                                                    wire:model.defer="elem.telefono_movil" name="telefono_movil" id="telefono_movil"
                                                    mask="[00]000000" scale="0" thousandsSeparator=""
                                                    :error="$errors->first('elem.telefono_movil')"
                                                    :disabled="!$editable"/>
                                            </x-custom.forms.input.group>
                                        </div>

                                        <div class="col-span-6 sm:col-span-1">
                                            <x-custom.forms.input.group label="Prefijo fijo" for="prefijo_fijo" :error="$errors->first('elem.prefijo_fijo')">
                                                <x-custom.forms.input.phone-mask
                                                    wire:model.defer="elem.prefijo_fijo" name="prefijo_fijo" id="prefijo_fijo"
                                                    mask="[00]00" scale="0" thousandsSeparator=""
                                                    :error="$errors->first('elem.prefijo_fijo')"
                                                    :disabled="!$editable"/>
                                            </x-custom.forms.input.group>
                                        </div>
                                        <div class="col-span-6 sm:col-span-2">
                                            <x-custom.forms.input.group label="Teléfono fijo" for="telefono_fijo" :error="$errors->first('elem.telefono_fijo')">
                                                <x-custom.forms.input.phone-mask
                                                    wire:model.defer="elem.telefono_fijo" name="telefono_fijo" id="telefono_fijo"
                                                    mask="[00]000000" scale="0" thousandsSeparator=""
                                                    :error="$errors->first('elem.telefono_fijo')"
                                                    :disabled="!$editable"/>
                                            </x-custom.forms.input.group>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            <div class="shadow overflow-hidden sm:rounded-md z-10">
                                <div class="px-4 py-6 bg-white sm:p-6">

                                    <div class="px-4 sm:px-0">
                                        <h3 class="text-lg font-medium leading-6 text-gray-900">Otros datos</h3>
                                    </div>

                                    <div class="hidden sm:block" aria-hidden="true">
                                        <div class="py-5">
                                            <div class="border-t border-gray-200"></div>
                                        </div>
                                    </div>

                                    <div class="grid grid-cols-6 gap-6 mb-4">
                                        <div class="col-span-6 sm:col-span-2">
                                            <x-custom.forms.input.group label="Monto solicitado" for="monto_maximo" :error="$errors->first('elem.monto_maximo')">
                                                <div wire:ignore>
                                                    <x-custom.forms.input.number
                                                        wire:model.defer="elem.monto_maximo" name="monto_maximo" id="monto_maximo" :currency="true"
                                                        mask="Number" scale="2" thousandsSeparator="."
                                                        :error="$errors->first('elem.monto_maximo')"
                                                        :disabled="!$editable"/>
                                                </div>
                                            </x-custom.forms.input.group>
                                        </div>
                                    </div>

                                    @if (isset($precarga->id))
                                        <div class="hidden sm:block" aria-hidden="true">
                                            <div class="py-5">
                                                <div class="border-t border-gray-200"></div>
                                            </div>
                                        </div>

                                        <div class="grid grid-cols-6 gap-6 mb-4">
                                            <div class="col-span-6 sm:col-span-6">
                                                <p class="text-red-500 text-sm">Recuerde cargar la documentación obligatoria antes de Enviar</p>
                                            </div>
                                        </div>
                                    @endif

                                </div>
                            </div>

                            @if ($errorGeneral != '')
                                <div class="shadow overflow-hidden sm:rounded-md z-30 bg-gray-50">
                                    <div class="px-4 py-6 bg-white sm:p-6">
                                        <div class="text-right sm:px-6">
                                            <p class="text-sm text-red-600 mt-2">{{ $errorGeneral }}</p>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if ($editable)
                                <div class="shadow overflow-hidden sm:rounded-md z-30 bg-gray-50">
                                    <div class="px-4 py-6 bg-white sm:p-6">
                                        <div class="text-right sm:px-6">
                                            @if (($estadoActualSlug == 'iniciada') || ($estadoActualSlug == 'observada'))
                                                @if (isset($precarga->id))
                                                    <x-custom.forms.buttons.secondary wire:click.prevent="changeState('anulada')" color="cool-gray">
                                                        Anular
                                                    </x-custom.forms.buttons.secondary>
                                                @endif
                                            @endif
                                            <x-custom.forms.buttons.a href="{{ route('precargas') }}" color="gray">
                                                Cancelar
                                            </x-custom.forms.buttons.a>
                                            @if (isset($precarga->id))
                                                <x-custom.forms.buttons.secondary wire:click="save(0)">
                                                    Guardar
                                                </x-custom.forms.buttons.secondary>
                                                <x-custom.forms.buttons.secondary wire:click="save(1)" color="indigo">
                                                    Guardar y enviar
                                                </x-custom.forms.buttons.secondary>
                                            @else
                                                <x-custom.forms.buttons.secondary wire:click="save(0)" color="indigo">
                                                    Iniciar
                                                </x-custom.forms.buttons.secondary>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="shadow overflow-hidden sm:rounded-md z-30 bg-gray-50">
                                    <div class="px-4 py-6 bg-white sm:p-6">
                                        <div class="text-right sm:px-6">
                                            <x-custom.forms.input.error
                                                :error="$errors->count()"
                                                :message="'Hubo '.$errors->count().' error'.(($errors->count()>1) ? 'es' : '').' en el guardado.'" />
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="shadow overflow-hidden sm:rounded-md z-30 bg-gray-50">
                                    <div class="px-4 py-6 bg-white sm:p-6">
                                        <div class="text-right sm:px-6">
                                            <x-custom.forms.buttons.a href="{{ route('precargas') }}" color="gray">
                                                Cerrar
                                            </x-custom.forms.buttons.a>
                                            @if ($gestionador)
                                                @switch($estadoActualSlug)
                                                    @case('iniciada')
                                                    @if ($precarga->user_id == auth()->user()->id)
                                                        <x-custom.forms.buttons.secondary wire:click.prevent="changeState('anulada')" color="cool-gray">
                                                            Anular
                                                        </x-custom.forms.buttons.secondary>
                                                    @endif
                                                    @case('en_revision')
                                                    <x-custom.forms.buttons.secondary wire:click.prevent="changeState('observada')" color="yellow">
                                                        Observar
                                                    </x-custom.forms.buttons.secondary>
                                                    <x-custom.forms.buttons.secondary wire:click.prevent="changeState('rechazada')" color="red">
                                                        Rechazar
                                                    </x-custom.forms.buttons.secondary>
                                                    @break
                                                    @case('observada')
                                                    @case('preaprobada')
                                                    @if ($precarga->user_id == auth()->user()->id)
                                                        <x-custom.forms.buttons.secondary wire:click.prevent="changeState('anulada')" color="cool-gray">
                                                            Anular
                                                        </x-custom.forms.buttons.secondary>
                                                    @endif
                                                    @break
                                                    @case('rechazada')
                                                    <x-custom.forms.buttons.primary wire:click.prevent="changeState('observada')" color="yellow">
                                                        Observar
                                                    </x-custom.forms.buttons.primary>
                                                    @break
                                                    @case('aceptada')
                                                    <x-custom.forms.buttons.action-button wire:click.prevent="changeState('observada')" :data="$estados['observada']" />
                                                    @break
                                                    @case('aprobada')
                                                    @if ($precarga->user_id == auth()->user()->id)
                                                        <x-custom.forms.buttons.secondary wire:click.prevent="changeState('anulada')" color="cool-gray">
                                                            Anular
                                                        </x-custom.forms.buttons.secondary>
                                                    @endif
                                                    @break
                                                    @case('firmada')
                                                    @break
                                                @endswitch
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endif

                        </form>
                    </div>

                    <div class="md:col-span-1 shadow overflow-hidden sm:rounded-md bg-white ">
                        <div class="px-4 pt-6 sm:p-6">
                            <div class="px-4 sm:px-0">
                                <h3 class="text-lg font-medium leading-6 text-gray-900">Estado de la Solicitud</h3>
                                <p class="mt-1 text-sm text-gray-600">
                                    Presenta una cronología con la situación por la cual transita una Solicitud y muestra
                                    información relevante de fecha, hora y el usuario actuante en el pasaje de estados
                                    de la solicitud.
                                </p>
                            </div>
                            <div class="hidden sm:block" aria-hidden="true">
                                <div class="pt-2">
                                    <div class="border-t border-gray-200"></div>
                                </div>
                            </div>
                        </div>
                        <div class="sm:px-0 mt-2">

                            <livewire:precarga-estados :precargaId="$precarga->id" />

                        </div>
                    </div>
                </div>
            </div>

            @if (isset($precarga->id))
                <div x-show="openTab === 'documentos'">
                    <div class="shadow overflow-hidden sm:rounded-md z-0">
                        <div class="px-4 py-6 bg-white sm:p-6">

                            <div class="px-4 sm:px-0">
                                <h3 class="text-lg font-medium leading-6 text-gray-900">Documentación requerida</h3>
                                <p class="mt-1 text-sm text-gray-600">

                                </p>
                            </div>

                            <x-custom.hr />

                            <section class="flex items-center justify-center">
                                <div class="shadow overflow-hidden border-b border-gray-200 w-full sm:rounded-lg">
                                    <x-custom.table>
                                        <x-slot name="head">
                                        </x-slot>
                                        <x-slot name="body">
                                            @forelse($precarga->documentos() as $documento)
                                                <x-custom.table.row>
                                                    <x-custom.table.cell class="align-text-top p-4">
                                                        <livewire:precarga-documento-adjunto :precarga="$precarga" :documento="$documento" :estadoActualSlug="$estadoActualSlug" :key="'documento_'.$documento->id" />
                                                    </x-custom.table.cell>
                                                </x-custom.table.row>
                                            @empty
                                                <x-custom.table.row wire:loading.class.delay="opacity-50">
                                                    <x-custom.table.cell colspan="5">
                                                        <div class="flex justify-center items-center space-x-2 text-cool-gray-400">
                                                            <x-custom.icon.small-search-document />
                                                            <span class="font-medium py-8 text-cool-gray-400 text-xl">
                                                                No se encontraron documentos...
                                                            </span>
                                                        </div>
                                                    </x-custom.table.cell>
                                                </x-custom.table.row>
                                            @endforelse
                                        </x-slot>
                                    </x-custom.table>
                                </div>
                            </section>

                            <x-custom.hr />

                            @if (($estadoActualSlug != 'anulada') && ($estadoActualSlug != 'rechazada') && ($estadoActualSlug != 'firmada') && ($estadoActualSlug != 'transferida'))
                                @php($roles = auth()->user()->getRoleNames())
                                @if (($roles[0] == config('tools.constants.roles.administrador')) || (auth()->user()->id == $precarga->user_id))
                                    <livewire:precarga-documentos :precarga="$precarga" />
                                @endif
                            @endif

                        </div>
                    </div>
                </div>
            @endif

        <!-- if ((($estadoActualSlug == 'en_revision') && ($gestionador)) || ($estadoActualSlug == 'preaprobada') || ($estadoActualSlug == 'aceptada')) -->
            @if (!$precarga->fondeada)
                <div x-show="openTab === 'propuesta'">
                    <livewire:propuestas-list :precargaId="$precarga->id" :estadoActualSlug="$estadoActualSlug" :gestionador="$gestionador" />
                </div>
            @endif

        <!-- if (($estadoActualSlug == 'aprobada') || ($estadoActualSlug == 'autorizada') || ($estadoActualSlug == 'firmada')) -->
            @if ($precarga->fondeada)

                <div x-show="openTab === 'legajo'">
                    <div class="shadow overflow-hidden sm:rounded-md z-0">
                        <section class="px-4 py-6 bg-white sm:p-6">
                            <div class="px-4 sm:px-0">
                                <h3 class="text-lg font-medium leading-6 text-gray-900">Legajo</h3>
                                <p class="mt-1 text-sm text-gray-600">

                                </p>
                            </div>

                            <x-custom.hr />

                            @if (($estadoActualSlug == 'firmada') && (auth()->user()->hasRole('Tesorero')))
                                <div class="shadow overflow-hidden sm:rounded-md z-30 bg-gray-50">
                                    <div class="px-4 py-6 bg-white sm:p-6">
                                        <div class="text-right sm:px-6">
                                            <p class="float-left w-50 text-justify">La solicitud se encuentra disponible para proceder a realizar la transferencia o pago al cliente, <br/>o de solicitar una intervención de una administrador si lo considera necesario.</p>
                                            <x-custom.forms.buttons.primary wire:click.prevent="changeState('transferida')" color="green">
                                                Transferir
                                            </x-custom.forms.buttons.primary>
                                            <x-custom.forms.buttons.primary wire:click.prevent="changeState('intervenida')" color="red">
                                                Solicitar intervención
                                            </x-custom.forms.buttons.primary>
                                        </div>
                                    </div>
                                </div>
                                <x-custom.hr />
                            @endif

                            @if (($propuestasTodasConfirmadas) && ($estadoActualSlug == 'aceptada'))
                                @if ($errorGeneral != '')
                                    <div class="shadow overflow-hidden sm:rounded-md z-30 bg-gray-50">
                                        <div class="px-4 py-6 bg-white sm:p-6">
                                            <div class="text-right sm:px-6">
                                                <p class="text-sm text-red-600 mt-2">{{ $errorGeneral }}</p>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if ($gestionador)
                                    <div class="shadow overflow-hidden sm:rounded-md z-30 bg-gray-50">
                                        <div class="px-4 py-6 bg-white sm:p-6">
                                            <div class="text-right sm:px-6">
                                                <p class="float-left w-50 text-justify">{!! $propuestasTodasConfirmadasMensaje !!}</p>
                                                <x-custom.forms.buttons.primary wire:click.prevent="changeState('autorizada')" color="green">
                                                    Autorizar
                                                </x-custom.forms.buttons.primary>
                                                <x-custom.forms.buttons.primary wire:click.prevent="changeState('rechazada')" color="red">
                                                    Rechazar
                                                </x-custom.forms.buttons.primary>
                                            </div>
                                        </div>
                                    </div>
                                    <x-custom.hr />
                                @endif
                            @endif

                            @if (($propuestasTodasFirmadas) && ($estadoActualSlug == 'autorizada'))
                                <div class="shadow overflow-hidden sm:rounded-md z-30 bg-gray-50">
                                    <div class="px-4 py-6 bg-white sm:p-6">
                                        <div class="text-right sm:px-6">
                                            <p class="float-left w-50 text-justify">{!! $propuestasTodasFirmadasMensaje !!}</p>
                                            <x-custom.forms.buttons.primary wire:click.prevent="changeState('firmada')" color="green">
                                                Enviar firmada
                                            </x-custom.forms.buttons.primary>
                                            <x-custom.forms.buttons.primary wire:click.prevent="changeState('rechazada')" color="red">
                                                Rechazar
                                            </x-custom.forms.buttons.primary>
                                        </div>
                                    </div>
                                </div>
                                <x-custom.hr />
                            @endif

                            <section class="flex items-center justify-center">
                                <div class="shadow overflow-hidden border-b border-gray-200 w-full sm:rounded-lg">
                                    @php($id=0)
                                    @if (isset($propuestas))
                                        @foreach($propuestas as $propuesta)
                                            @php($id++)
                                            @if ($id > 1)
                                                <hr class="mt-5 mb-5"/>
                                            @endif
                                            <livewire:propuesta-legajo wire:key="{{ $propuesta->id }}" :precarga="$precarga" :propuesta="$propuesta" :estadoActualSlug="$estadoActualSlug" :gestionador="$gestionador" />
                                        @endforeach
                                    @endif
                                </div>
                            </section>
                        </section>
                    </div>

                </div>
            @endif

            @if ($antecedentes->count() > 0)
                @if($gestionador || ($precarga->user_id == auth()->user()->id))
                    <div x-show="openTab === 'antecedentes'">
                        <div class="shadow overflow-hidden sm:rounded-md z-0">
                            <section class="px-4 py-6 bg-white sm:p-6">
                                <div class="px-4 sm:px-0">
                                    <h3 class="text-lg font-medium leading-6 text-gray-900">Antecedentes</h3>
                                    <p class="mt-1 text-sm text-gray-600">

                                    </p>
                                </div>
                                <x-custom.hr />
                                <section class="flex items-center justify-center">
                                    <div class="shadow overflow-hidden border-b border-gray-200 w-full sm:rounded-lg">
                                        <x-custom.table>
                                            <x-slot name="head">
                                                <x-custom.table.heading>
                                                    Comercial
                                                </x-custom.table.heading>
                                                <x-custom.table.heading class="w-1/6">
                                                    Estado
                                                </x-custom.table.heading>
                                                <x-custom.table.heading>
                                                    Último usuario
                                                </x-custom.table.heading>
                                                <x-custom.table.heading class="w-1/12">
                                                    Acción
                                                </x-custom.table.heading>
                                            </x-slot>

                                            <x-slot name="body">
                                                @forelse($antecedentes as $antecedente)
                                                    @if (!isset($antecedente->created_at))
                                                        @php($antecedente = (new \App\Managers\PrecargaManager)->getItem($antecedente->id))
                                                    @endif
                                                    <x-custom.table.row wire:loading.class.delay="opacity-50">
                                                        <x-custom.table.cell>
                                                            <div class="flex items-center">
                                                                <div>
                                                                    <div class="text-base font-medium text-gray-900">
                                                                        <strong>{{ $precarga->user->name }}</strong>
                                                                    </div>
                                                                    <div class="text-xs text-gray-500">
                                                                        {{ $precarga->user->email }}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </x-custom.table.cell>
                                                        <x-custom.table.cell>
                                                            @php($precargaEstadoActual = $antecedente->precargaEstadoActual())
                                                            <div class="flex items-center">
                                                                <div>
                                                                    <div class="text-base font-medium text-gray-900">
                                                                        <span class="py-2 px-2 w-full inline-flex text-lg leading-5 font-semibold rounded-md bg-{{ $precargaEstadoActual->color }}-500 text-white">
                                                                            {{ $precargaEstadoActual->nombre }}
                                                                        </span>
                                                                    </div>
                                                                    @if ($precargaEstadoActual->slug == 'en_revision')
                                                                        <div class="text-base font-medium text-gray-900">
                                                                            {{ $precargaEstadoActual->pivot->created_at->format('d-m-Y') }}
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </x-custom.table.cell>
                                                        <x-custom.table.cell>
                                                            <div class="flex items-center">
                                                                <div>
                                                                    <div class="text-base font-medium text-gray-900">
                                                                        <strong></strong>
                                                                    </div>
                                                                    <div class="text-xs text-gray-500">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </x-custom.table.cell>
                                                        <x-custom.table.cell>
                                                            <a href="{{ route('precargas.editar', [(new \App\Tools\HelpersTool)->helper_getUrlHash($antecedente->id), 'precarga']) }}"
                                                               class="inline-flex items-center justify-center w-10 h-10 mr-2 transition-colors duration-150 rounded-full focus:shadow-outline border border-indigo-500 text-indigo-500 hover:bg-indigo-500 hover:text-white">
                                                                <span class="relative inline-block">
                                                                  <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                                      <path fill-rule="evenodd" d="M.458 10C1.732 5.943 5.522 3 10 3s8.268 2.943 9.542 7c-1.274 4.057-5.064 7-9.542 7S1.732 14.057.458 10zM14 10a4 4 0 11-8 0 4 4 0 018 0z" clip-rule="evenodd" />
                                                                   </svg>
                                                                </span>
                                                            </a>
                                                        </x-custom.table.cell>
                                                    </x-custom.table.row>
                                                @empty
                                                    <x-custom.table.row wire:loading.class.delay="opacity-50">
                                                        <x-custom.table.cell colspan="5">
                                                            <div class="flex justify-center items-center space-x-2 text-cool-gray-400">
                                                                <x-custom.icon.small-search-document />
                                                                <span class="font-medium py-8 text-cool-gray-400 text-xl">
                                                                    No se encontraron solicitudes...
                                                                </span>
                                                            </div>
                                                        </x-custom.table.cell>
                                                    </x-custom.table.row>
                                                @endforelse
                                            </x-slot>
                                        </x-custom.table>
                                    </div>
                                </section>
                            </section>
                        </div>
                    </div>
                @endif
            @endif

            @if (isset($precarga->id))
                <div x-show="openTab === 'mensajes'">
                    <div class="w-full">

                        @if (isset($precarga->id))
                            <div class="flex float-right inline-block">
                                <a href="{{ route('precargas.editar', [(new \App\Tools\HelpersTool)->helper_getUrlHash($precarga->id), 'mensajes']) }}"
                                   class="inline-flex items-center justify-center w-6 h-6 m-2 transition-colors duration-150 rounded-full focus:shadow-outline border border-indigo-500 text-indigo-500 hover:bg-indigo-500 hover:text-white">
                                    <span class="relative inline-block">
                                      <svg class="h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd" d="M4 2a1 1 0 011 1v2.101a7.002 7.002 0 0111.601 2.566 1 1 0 11-1.885.666A5.002 5.002 0 005.999 7H9a1 1 0 010 2H4a1 1 0 01-1-1V3a1 1 0 011-1zm.008 9.057a1 1 0 011.276.61A5.002 5.002 0 0014.001 13H11a1 1 0 110-2h5a1 1 0 011 1v5a1 1 0 11-2 0v-2.101a7.002 7.002 0 01-11.601-2.566 1 1 0 01.61-1.276z" clip-rule="evenodd" />
                                      </svg>
                                    </span>
                                </a>
                            </div>
                            <div class="clearfix"></div>
                        @endif

                        <div class="shadow overflow-hidden sm:rounded-md">
                            <div class="px-4 py-6 bg-white sm:p-6">

                                @if($precarga->mensajes->count() > 0)
                                    <div class="px-4 sm:px-0">
                                        <h3 class="text-lg font-medium leading-6 text-gray-900">Historial</h3>
                                    </div>

                                    <div class="hidden sm:block" aria-hidden="true">
                                        <div class="py-5">
                                            <div class="border-t border-gray-200"></div>
                                        </div>
                                    </div>
                                @endif

                                <x-mensajes :precarga="$precarga" />

                                <!-- Marco todo los mensajes como leidos -->
                                @php($precarga->marcarMensajesComoLeidos($userPerfil))

                            </div>
                        </div>
                    </div>
                </div>
            @endif

        </div>

    </div>

    <!-- <div id="loading-screen" class="w-full h-full fixed block top-0 left-0 bg-white opacity-75 z-50">
          <span class="text-green-500 opacity-75 top-1/2 my-0 mx-auto block relative w-0 h-0">
            <i class="fas fa-circle-notch fa-spin fa-5x"></i>
          </span>
    </div> -->

    <div style="display: none" class="d-none fixed z-10 inset-0 overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true">
        <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <!--
              Background overlay, show/hide based on modal state.

              Entering: "ease-out duration-300"
                From: "opacity-0"
                To: "opacity-100"
              Leaving: "ease-in duration-200"
                From: "opacity-100"
                To: "opacity-0"
            -->
            <div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>

            <!-- This element is to trick the browser into centering the modal contents. -->
            <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>

            <!--
              Modal panel, show/hide based on modal state.

              Entering: "ease-out duration-300"
                From: "opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                To: "opacity-100 translate-y-0 sm:scale-100"
              Leaving: "ease-in duration-200"
                From: "opacity-100 translate-y-0 sm:scale-100"
                To: "opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            -->
            <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden
                shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="sm:flex sm:items-start">
                        <div class="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full text-purple-500 bg-purple-100 sm:mx-0 sm:h-10 sm:w-10">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                <path fill-rule="evenodd" d="M8 4a3 3 0 00-3 3v4a5 5 0 0010 0V7a1 1 0 112 0v4a7 7 0 11-14 0V7a5 5 0 0110 0v4a3 3 0 11-6 0V7a1 1 0 012 0v4a1 1 0 102 0V7a3 3 0 00-3-3z" clip-rule="evenodd" />
                            </svg>
                        </div>
                        <div class="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left w-full">
                            <h3 class="text-lg leading-6 font-medium text-gray-900" id="modal-title">
                                Adjuntar documento
                            </h3>
                            <div class="mt-2 w-full">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <button wire:click="ocultarUploadModal()" type="button" class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-purple-600 text-base font-medium text-white hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-purple-500 sm:ml-3 sm:w-auto sm:text-sm">
                        Guardar
                    </button>
                    <button wire:click="ocultarUploadModal()" type="button" class="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">
                        Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div x-data="{ show: false }"
         x-show="show"
         x-on:notify.window="show = true"
         x-on:click.window="show = false"
         class="fixed inset-0 flex items-end justify-end px-4 py-6
                        sm:p-6 sm:items-start sm:justify-end mt-8"
         style="display: none;">
        <div class="w-full px-4 py-4 overflow-x-auto text-right whitespace-no-wrap rounded-md">
            <div class="inline-flex w-full max-w-sm ml-3 overflow-hidden bg-white border border-red-500 rounded-lg shadow-sm">
                <div class="flex items-center justify-center w-12 bg-red-500 border border-red-500">
                    <svg class="w-6 h-6 text-white fill-current" viewBox="0 0 40 40" xmlns="http://www.w3.org/2000/svg">
                        <path d="M20 3.36667C10.8167 3.36667 3.3667 10.8167 3.3667 20C3.3667 29.1833 10.8167 36.6333 20 36.6333C29.1834 36.6333 36.6334 29.1833 36.6334 20C36.6334 10.8167 29.1834 3.36667 20 3.36667ZM19.1334 33.3333V22.9H13.3334L21.6667 6.66667V17.1H27.25L19.1334 33.3333Z" />
                    </svg>
                </div>
                <div class="px-4 py-2 text-left">
                    <span class="font-semibold text-red-500">Error</span>
                    <p class="mb-1 text-sm leading-none text-gray-500">Se produjo algun error en el guardado.</p>
                </div>
            </div>
        </div>
    </div>

</div>

@push('styles')

    <link rel="stylesheet" type="text/css" href="{{ asset('css/pikaday.css') }}">

@endpush

@push('scripts')

    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script src="{{ asset('js/pikaday.js') }}"></script>
    <script src="{{ asset('js/imask.js') }}" type="text/javascript"></script>

    <script>
        @if ($estadoActualSlug == 'en_revision')
        window.livewire.on('changeLinea', data => {
            document.querySelectorAll('.borrarGrillas').forEach(function(element) {
                element.style.display = 'none';
            });
            document.querySelectorAll('.linea_'+data).forEach(function(element) {
                element.style.display = 'block';
            });
        });
        @endif
        window.livewire.on('deleteDocumentacion', data => {
            var e = document.getElementById('documento'+data).remove();
        });
        window.livewire.on('openModal', data => {
            alert('openModal');
        });
    </script>

@endpush
