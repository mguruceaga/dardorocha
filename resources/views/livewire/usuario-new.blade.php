<div>
    <form wire:submit.prevent="save" autocomplete="off">

        <input type="hidden" name="token" value="{{ $token }}">

        <div class="block">
            <label class="block font-medium text-sm text-gray-700" for="email">
                Correo electrónico
            </label>
            <input type="text" name="email" id="email" disabled value="{{ $email }}"
                   class="form-input rounded-md shadow-sm mt-1 block w-full"/>
        </div>

        <div class="mt-4">
            <x-custom.forms.input.group label="Contraseña" for="password" :error="$errors->first('password')">
                <x-custom.forms.input.text wire:model.defer="password" name="password" id="password"/>
            </x-custom.forms.input.group>
        </div>

        <div class="mt-4">
            <x-custom.forms.input.group label="Confirmación de Contraseña" for="password_confirmation" :error="$errors->first('password_confirmation')">
                <x-custom.forms.input.text wire:model.defer="password_confirmation" name="password_confirmation" id="password_confirmation"/>
            </x-custom.forms.input.group>
        </div>

        <div class="flex items-center justify-end mt-4">
            <button type="submit"
                    class="inline-flex justify-center py-2 px-4 border border-transparent
                           shadow-sm text-sm font-medium bg-indigo-500 rounded-md text-white
                           hover:bg-indigo-200 focus:outline-none focus:ring-2
                           focus:ring-offset-2 focus:ring-indigo-500">
                Guardar..
            </button>
        </div>
    </form>
</div>

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/pikaday.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script src="{{ asset('js/pikaday.js') }}"></script>
    <script src="{{ asset('js/imask.js') }}" type="text/javascript"></script>
    <script>

    </script>
@endpush
