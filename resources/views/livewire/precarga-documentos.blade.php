<div class="shadow overflow-hidden sm:rounded-md z-0 w-full">
    <div class="px-4 py-6 bg-white sm:p-6">

        <div class="container">
            @php($roles = auth()->user()->getRoleNames())
            @switch($estadoActualSlug)
                @case('iniciada')
                @case('en_revision')
                @case('preaprobada')
                @case('observada')
                    @if (($roles[0] == config('tools.constants.roles.administrador')) || (auth()->user()->id == $elem->user_id))
                        <x-custom.forms.input.filepond wire:model="files" multiple />
                    @endif
                    @break
                @case('aceptada')
                    @if ($roles[0] == config('tools.constants.roles.administrador'))
                        <x-custom.forms.input.filepond wire:model="files" multiple />
                    @endif
                    @break
            @endswitch
        </div>

        <x-custom.hr />

        @if (count($files) > 0)
            @if (($estadoActualSlug != 'anulada') && ($estadoActualSlug != 'rechazada') && ($estadoActualSlug != 'firmada') && ($estadoActualSlug != 'transferida'))
                <div class="container">
                    <div class="text-right sm:px-6">
                        <x-custom.forms.buttons.secondary wire:click.prevent="save()" color="green">
                            Guardar
                        </x-custom.forms.buttons.secondary>
                    </div>
                </div>
            @endif
        @endif

    </div>
</div>
