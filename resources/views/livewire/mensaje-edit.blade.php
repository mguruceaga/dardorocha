<div class="px-4">
    <form wire:submit.prevent="save" autocomplete="off">
        <x-custom.forms.input.group label="Mensaje" for="mensaje" :error="$errors->first('mensaje.texto')">
            <x-custom.forms.input.textarea wire:model.lazy="mensaje.texto" name="mensaje" id="mensaje"
                                :error="$errors->first('mensaje.texto')"/>
        </x-custom.forms.input.group>
        <div class="sm:flex-1 sm:flex sm:items-center sm:justify-between">
            <!-- <div class="block w-full mt-3">
                <x-custom.forms.buttons.link>
                    <p class="text-xs">Marcar mensajes como leídos</p>
                </x-custom.forms.buttons.link>
            </div> -->
            <div class="block w-full mt-3 text-right">
                <x-custom.forms.buttons.link color="indigo" class="pr-2" href="#"
                                             wire:click.prevent="clearMessage()" >
                    Limpiar
                </x-custom.forms.buttons.link>
                <x-custom.forms.buttons.primary color="indigo">
                    Enviar
                </x-custom.forms.buttons.primary>
            </div>
        </div>

    </form>
</div>
