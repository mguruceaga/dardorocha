<ul class="rounded-md p-2 sm:p-5 xl:p-6 space-y-8">
    @foreach($estados as $estado)
        <li>
            <article>
                <!-- <h3 class="font-semibold text-gray-900 md:col-start-3 md:col-span-6 xl:col-start-3 xl:col-span-7 mb-1 ml-9 md:ml-0">Tailwind CSS v2.0</h3> -->
                <div class="md:col-start-1 md:col-span-2 row-start-1 md:row-end-3 flex
                             rounded-md items-center font-medium mb-1 md:mb-0">
                    <svg viewBox="0 0 8 8" class="w-3 h-3 mr-6 overflow-visible text-{{ $estado->color }}-500">
                        <path d="M 6 18 V 500" fill="none" stroke-width="1" stroke="#EEE"
                              class="text-gray-200">
                        </path>
                        <circle cx="6" cy="6" r="14" fill="#FFF" stroke="currentColor"></circle>
                        <path d="{{ $estado->icono }}" fill="currentColor" stroke="#FFF"
                              transform="translate(-4, -4)">
                        </path>
                    </svg>
                    <span class="text-white bold bg-{{ $estado->color }}-500 rounded-md p-2 block w-full"><strong>{{ $estado->nombre }}</strong></span>
                </div>
                <div class="ml-10 sm:z-10 space-y-1">
                    <div>
                        <div class="flex items-center space-x-0.5 mt-2">
                            <span class="inline-flex items-center px-3 bg-gray-50 text-gray-500 text-sm">
                                <x-custom.icon.small-calendar />
                            </span>
                            <span class="text-xs">{{ $estado->fecha }}</span>
                        </div>
                        <div class="flex items-center space-x-0-5 mt-2">
                            <span class="inline-flex items-center px-3 text-gray-500 text-sm">
                                <x-custom.icon.small-user-circle />
                            </span>
                            <span class="text-xs">{{ $estado->name }}</span>
                        </div>
                        @if ($estado->mensaje != '')
                            <div class="flex space-x-0-5 mt-2">
                                <span class="inline-flex align-top px-3 text-gray-500 text-sm">
                                    <svg class="h-5 w-5 text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                                        <path fill-rule="evenodd" d="M18 5v8a2 2 0 01-2 2h-5l-5 4v-4H4a2 2 0 01-2-2V5a2 2 0 012-2h12a2 2 0 012 2zM7 8H5v2h2V8zm2 0h2v2H9V8zm6 0h-2v2h2V8z" clip-rule="evenodd" />
                                    </svg>
                                </span>
                                <span class="text-xs">
                                    {!! $estado->mensaje !!}
                                </span>
                            </div>
                         @endif
                    </div>
                </div>
            </article>
        </li>
    @endforeach
    @if ($estados->count() > 0)
        <li>
            @if ($estado->slug == 'iniciada' || $estado->slug == 'observada')
                <!-- <article>
                    <div class="md:col-start-1 md:col-span-2 row-start-1 md:row-end-3
                                flex items-start font-medium mb-1 md:mb-0 align-top">
                        <svg viewBox="0 0 12 12" class="w-3 h-3 mt-3 mr-6 overflow-visible text-gray-300 align-text-top">
                            <circle cx="6" cy="6" r="6" fill="currentColor"></circle>
                            <path d="M 6 -6 V -30" fill="none" stroke-width="2" stroke="currentColor" class="text-gray-200"></path>
                            <path d="M 6 18 V 500" fill="none" stroke-width="2" stroke="currentColor" class="text-gray-200"></path>
                        </svg>
                        <div x-data="{ show: false }" class="block w-full shadow-sm rounded-b-md bg-white">
                            <span class="text-gray-500 bold bg-gray-300 rounded-md p-2 block w-full">
                                Enviar
                            </span>
                            <div>
                                <div class="flex m-2 rounded-b-md bg-white">
                                    <div class="text-base block w-full">
                                        ¿Querés enviar la Solicitud para que sea analizada?.
                                    </div>
                                </div>
                                <div class="flex m-2">
                                    <div class="text-base text-right block w-full">
                                        <x-custom.forms.buttons.secondary wire:click.prevent="changeState('en_revision')" color="purple">
                                            Enviar
                                        </x-custom.forms.buttons.secondary>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article> -->
            @endif

        </li>
    @endif
</ul>
