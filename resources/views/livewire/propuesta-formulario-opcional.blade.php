<div>
    <input wire:model="formulario.activo"
           type="checkbox" name="activo" value="1" {!! ($propuesta->legajo_confirmado) ? 'disabled' : '' !!}
           class="form-checkbox h-4 w-4 {!! ($propuesta->legajo_confirmado) ? 'text-gray-400 border-gray-400' : 'text-blue-400 border-blue-400' !!} mr-4 float-left absolute"
           style="margin-left: -10px">
    <div class="text-sm text-gray-700" style="padding-left: 20px">
        {{ $formulario->formulario->nombre }}
    </div>
    @if ($propuesta->legajo_confirmado)
        @if ($formulario->activo)
            <div class="text-sm text-gray-700" style="padding-left: 40px">
                <ul class="list-disc">
                    @if ($formulario->original_bloqueado)
                        <li>Original</li>
                    @endif
                    @if ($formulario->copias_identicas)
                        <li>Copias: {{ $formulario->copias_identicas }}</li>
                    @endif
                    @if ($formulario->copias_blanco)
                        <li>Copias en blanco: {{ $formulario->copias_blanco }}</li>
                    @endif
                </ul>
            </div>
        @endif
    @endif
</div>
