<div>

    @if ($propuestas->count() > 0)
        <div class="shadow overflow-hidden sm:rounded-md z-10">
            <div class="px-4 py-6 bg-white sm:p-6">

                <div class="px-4 sm:px-0">
                    <h3 class="text-lg font-medium leading-6 text-gray-900">Opciones</h3>
                </div>

                <div class="hidden sm:block" aria-hidden="true">
                    <div class="py-5">
                        <div class="border-t border-gray-200"></div>
                    </div>
                </div>

                <div class="grid grid-cols-6 gap-6 mb-4">
                    <div class="col-span-6 sm:col-span-6">
                        <x-custom.table>
                            <x-slot name="head">
                                <x-custom.table.heading class="w-2/12">
                                    Línea de Crédito
                                </x-custom.table.heading>
                                <x-custom.table.heading class="w-3/12">
                                    Grilla
                                </x-custom.table.heading>
                                @if (($estadoActualSlug == 'aceptada') && ($gestionador))
                                    <x-custom.table.heading class="w-2/12">
                                        Fondista
                                    </x-custom.table.heading>
                                @endif
                                <x-custom.table.heading class="w-1/12">
                                    Monto
                                </x-custom.table.heading>
                                <x-custom.table.heading class="w-1/12">
                                    Cuotas
                                </x-custom.table.heading>
                                <x-custom.table.heading class="w-1/12">
                                    Valor de Cuota
                                </x-custom.table.heading>
                                <x-custom.table.heading class="w-1/12">
                                    Servicios Adicionales
                                </x-custom.table.heading>
                                <x-custom.table.heading class="w-1/12">
                                    Cuota Social
                                </x-custom.table.heading>
                                <x-custom.table.heading class="w-1/12">

                                </x-custom.table.heading>
                            </x-slot>

                            <x-slot name="body">
                                @php($id=0)
                                @foreach($propuestas as $propuesta)
                                    @php($id++)
                                    <livewire:propuesta :key="$propuesta->id" :precarga="$precarga" :propuesta="$propuesta" :estadoActualSlug="$estadoActualSlug" :gestionador="$gestionador" :fondistas="$fondistas" />
                                @endforeach
                            </x-slot>
                        </x-custom.table>
                    </div>
                </div>
                <div class="text-right sm:px-6">
                    @if (($estadoActualSlug == 'en_revision') && $gestionador)
                        @if ($propuestas->count() > 1)
                            <label class="inline-flex items-center mt-3 float-left">
                                <input
                                        wire:model="multiplesPropuestas"
                                        type="checkbox" value="1" class="form-checkbox h-5 w-5 text-pink-600" >
                                <span class="ml-2 text-gray-700">Permitir selección múltiple de propuestas</span>
                            </label>
                        @endif
                        <x-custom.forms.buttons.secondary wire:click.prevent="changeState('preaprobada')" color="blue">
                            Pre-aprobar
                        </x-custom.forms.buttons.secondary>
                    @elseif ($estadoActualSlug == 'preaprobada' && (!$gestionador || ($precarga->user_id == auth()->user()->id)))
                        <x-custom.forms.input.error
                            :error="$error"
                            :message="$mensajeError" />
                        @if (!$gestionador || ($precarga->user_id == auth()->user()->id))
                            <x-custom.forms.buttons.secondary wire:click.prevent="changeState('en_revision')" color="purple">
                                Solicito otro plan
                            </x-custom.forms.buttons.secondary>
                        @else
                            <x-custom.forms.buttons.secondary wire:click.prevent="changeState('observada')" color="yellow">
                                Observar
                            </x-custom.forms.buttons.secondary>
                        @endif
                        @if (!$gestionador || ($precarga->user_id == auth()->user()->id))
                            <x-custom.forms.buttons.secondary wire:click.prevent="propuestaAceptada()" color="green">
                                Aceptar
                            </x-custom.forms.buttons.secondary>
                        @endif
                    @elseif (($estadoActualSlug == 'aceptada') && $gestionador)
                        <x-custom.forms.input.error
                            :error="$error"
                            :message="$mensajeError" />
                        <x-custom.forms.buttons.secondary wire:click.prevent="fondearPropuestas()" color="green">
                            Guardar
                        </x-custom.forms.buttons.secondary>
                    @endif
                    @if ($gestionador && ($estadoActualSlug != 'rechazada'))
                        <x-custom.forms.buttons.secondary wire:click.prevent="changeState('rechazada')" color="red">
                            Rechazar
                        </x-custom.forms.buttons.secondary>
                    @endif
                </div>
            </div>
        </div>

    @endif

    @if ($estadoActualSlug == 'en_revision')
        <form id="formPrecarga" wire:submit.prevent="save" autocomplete="off">

            <div class="shadow overflow-hidden sm:rounded-md z-10">
                <div class="px-4 py-6 bg-white sm:p-6">

                    <div class="px-4 sm:px-0">
                        <h3 class="text-lg font-medium leading-6 text-gray-900">Agregar propuesta</h3>
                    </div>

                    <div class="hidden sm:block" aria-hidden="true">
                        <div class="py-5">
                            <div class="border-t border-gray-200"></div>
                        </div>
                    </div>

                    <div class="grid grid-cols-12 gap-6 mb-4">
                        <div class="col-span-3 sm:col-span-4">
                            <x-custom.forms.input.group label="Línea de crédito" for="linea_credito_id" :error="$errors->first('elem.linea_credito_id')">
                                <select
                                        wire:change="changeLineaId($event.target.value)"
                                        wire:keydown.prevent="nada()"
                                        wire:model.defer="elem.linea_credito_id"
                                        name="linea_credito_id" id="linea_credito_id"
                                        onkeydown="return false"
                                        {{ !$editable ? ' disabled="disabled"' : '' }}
                                        class="select2 mt-1 block w-full py-2 px-3
                                                        border bg-white rounded-md shadow-sm
                                                        focus:outline-none focus:ring-indigo-500
                                                        focus:border-indigo-500 sm:text-sm
                                                        {{ $errors->first('elem.linea_credito_id') ? ' border-red-500' : (!$editable ? ' border-gray-50' : 'border-gray-300') }}">
                                    <option wire:key="0" value="0" {!! ($elem->linea_credito_id == 0) ? 'selected="selected"' : '' !!}>Seleccione una opción</option>
                                    @foreach ($lineasCredito as $linea)
                                        @if (isset($grilla->id))
                                            <option wire:key="{{ $linea->id }}" value="{{ $linea->id }}" {!! ($linea->id == $elem->linea_credito_id) ? 'selected="selected"' : '' !!}>{{ $linea->nombre }}</option>
                                        @else
                                            <option wire:key="{{ $linea['id'] }}" value="{{ $linea['id'] }}" {!! ($linea->id == $elem['linea_credito_id']) ? 'selected="selected"' : '' !!}>{{ $linea['nombre'] }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </x-custom.forms.input.group>
                        </div>
                        <div class="col-span-3 sm:col-span-3">
                            <x-custom.forms.input.group label="Grillas" for="grilla_id" :error="$errors->first('elem.grilla_id')">
                                <select
                                        wire:model.defer="elem.grilla_id"
                                        name="grilla_id" id="grilla_id"
                                        onkeydown="return false"
                                        {{ !$editable ? ' disabled="disabled"' : '' }}
                                        class="select2 mt-1 block w-full py-2 px-3
                                                        border bg-white rounded-md shadow-sm
                                                        focus:outline-none focus:ring-indigo-500
                                                        focus:border-indigo-500 sm:text-sm
                                                        {{ $errors->first('elem.grilla_id') ? ' border-red-500' : (!$editable ? ' border-gray-50' : 'border-gray-300') }}">
                                    <option wire:key="0" value="0" {!! ($elem->grilla_id == 0) ? 'selected="selected"' : '' !!}>Seleccione una opción</option>
                                    @foreach ($grillas as $grilla)
                                        @if (isset($grilla->id))
                                            <option wire:key="{{ $grilla->id }}" value="{{ $grilla->id }}" {!! ($grilla->id == $elem->grilla_id) ? 'selected="selected"' : '' !!} {!! ($grilla->mutual_linea_id == $elem->linea_credito) ? 'style="display:block!important"' : 'style="display:none!important"' !!} class="borrarGrillas linea_{{ $grilla->mutual_linea_id }}">{{ $grilla->nombre }}</option>
                                        @else
                                            <option wire:key="{{ $grilla['id'] }}" value="{{ $grilla['id'] }}" {!! ($grilla['id'] == $elem['grilla_id']) ? 'selected="selected"' : '' !!} {!! ($grilla['mutual_linea_id'] == $elem['linea_credito']) ? 'style="display:block!important"' : 'style="display:none!important"' !!} class="borrarGrillas linea_{{ $grilla['mutual_linea_id'] }}">{{ $grilla['nombre'] }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </x-custom.forms.input.group>
                        </div>
                        <div class="col-span-3 sm:col-span-3">
                            <x-custom.forms.input.group label="Forma de Débito" for="debito_id" :error="$errors->first('elem.debito_id')">
                                <x-custom.forms.input.select
                                        wire:model.defer="elem.debito_id"
                                        name="debito_id" id="debito_id"
                                        :options="$debitos" />
                            </x-custom.forms.input.group>
                        </div>
                    </div>

                    <div class="grid grid-cols-12 gap-6 mb-4">
                        <div class="col-span-3 sm:col-span-2">
                            <x-custom.forms.input.group label="Monto" for="monto" :error="$errors->first('elem.monto')">
                                <div wire:ignore>
                                    <x-custom.forms.input.number
                                            wire:model.defer="elem.monto" name="monto" id="monto" :currency="true" autocomplete="nope"
                                            mask="Number" scale="2" thousandsSeparator="." />
                                </div>
                                <div x-data="{ isShowingErrorMonto: @entangle('errorMonto') }">
                                    <div x-show="isShowingErrorMonto" style="display: none">
                                        <p class="text-sm text-red-600 mt-2">Monto inválido</p>
                                    </div>
                                </div>
                            </x-custom.forms.input.group>
                        </div>
                        <div class="col-span-3 sm:col-span-2">
                            <x-custom.forms.input.group label="Cuotas" for="cuotas" :error="$errors->first('elem.cuotas')">
                                <div wire:ignore>
                                    <x-custom.forms.input.number
                                            wire:model.defer="elem.cuotas" name="cuotas" id="cuotas" :currency="false" autocomplete="nope"
                                            mask="Number" scale="0" thousandsSeparator="." />
                                </div>
                                <div x-data="{ isShowingErrorCuotas: @entangle('errorCuotas') }">
                                    <div x-show="isShowingErrorCuotas" style="display: none">
                                        <p class="text-sm text-red-600 mt-2">Monto inválido</p>
                                    </div>
                                </div>
                            </x-custom.forms.input.group>
                        </div>
                        <div class="col-span-3 sm:col-span-2">
                            <x-custom.forms.input.group label="Valor cuota" for="valor_cuota" :error="$errors->first('elem.valor_cuota')">
                                <div wire:ignore>
                                    <x-custom.forms.input.number
                                            wire:model.defer="elem.valor_cuota" name="valor_cuota" id="valor_cuota" :currency="true" autocomplete="nope"
                                            mask="Number" scale="2" thousandsSeparator="." />
                                </div>
                                <div x-data="{ isShowingErrorValorCuota: @entangle('errorValorCuota') }">
                                    <div x-show="isShowingErrorValorCuota" style="display: none">
                                        <p class="text-sm text-red-600 mt-2">Monto inválido</p>
                                    </div>
                                </div>
                            </x-custom.forms.input.group>
                        </div>
                        <div class="col-span-3 sm:col-span-2">
                            <x-custom.forms.input.group label="Servicios adicionales" for="adicionales" :error="$errors->first('elem.adicionales')">
                                <div wire:ignore>
                                    <x-custom.forms.input.number
                                            wire:model.defer="elem.adicionales" name="adicionales" id="adicionales" :currency="true" autocomplete="nope"
                                            mask="Number" scale="2" thousandsSeparator="." />
                                </div>
                                <div x-data="{ isShowingErrorAdicionales: @entangle('errorAdicionales') }">
                                    <div x-show="isShowingErrorAdicionales" style="display: none">
                                        <p class="text-sm text-red-600 mt-2">Monto inválido</p>
                                    </div>
                                </div>
                            </x-custom.forms.input.group>
                        </div>
                        <div class="col-span-3 sm:col-span-2">
                            <x-custom.forms.input.group label="Valor cuota social" for="valor_cuota_social" :error="$errors->first('elem.valor_cuota_social')">
                                <div wire:ignore>
                                    <x-custom.forms.input.number
                                            wire:model.defer="elem.valor_cuota_social" name="valor_cuota_social" id="valor_cuota_social" :currency="true"
                                            mask="Number" scale="2" thousandsSeparator="." />
                                </div>
                                <div x-data="{ isShowingErrorCuotaSocial: @entangle('errorCuotaSocial') }">
                                    <div x-show="isShowingErrorCuotaSocial" style="display: none">
                                        <p class="text-sm text-red-600 mt-2">Monto inválido</p>
                                    </div>
                                </div>
                            </x-custom.forms.input.group>
                        </div>
                    </div>

                </div>
            </div>

            <div class="shadow overflow-hidden sm:rounded-md z-30 bg-gray-50">
                <div class="px-4 py-6 bg-white sm:p-6">
                    <div class="text-right sm:px-6">
                        <x-custom.forms.buttons.secondary wire:click.prevent="cleanForm()" color="gray">
                            Limpiar
                        </x-custom.forms.buttons.secondary>
                        <x-custom.forms.buttons.primary color="indigo">
                            Guardar
                        </x-custom.forms.buttons.primary>
                    </div>
                </div>
            </div>

        </form>
    @endif

</div>

@push('scripts')

    <script>
        window.livewire.on('errorAlAceptarPropuestas', data => {
            console.log(data);
            var e = document.getElementById('cajaErrorAlAprobarPropuesta').html(data);
        });
    </script>

@endpush
