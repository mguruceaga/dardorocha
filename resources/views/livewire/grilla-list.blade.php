<div class="flex flex-col">
    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">

                <x-custom.table>

                    <x-slot name="head">

                        <x-custom.table.heading class="w-1/6"
                                                sortable wire:click="sortBy('nombre')" :direction="$ordenarPor === 'nombre' ? $sentido : null">
                            Nombre
                        </x-custom.table.heading>

                        <x-custom.table.heading sortable wire:click="sortBy('abreviatura')" :direction="$ordenarPor === 'abreviatura' ? $sentido : null">
                            Abreviatura
                        </x-custom.table.heading>

                        <x-custom.table.heading class="w-1/6">
                            <span class="sr-only">Editar</span>
                        </x-custom.table.heading>

                    </x-slot>

                    <x-slot name="body">
                        <x-custom.table.row wire:loading.class.delay="opacity-50" class="bg-gray">

                            <x-custom.table.cell>
                                <x-custom.forms.input.find-small wire:model.lazy="nombre"/>
                            </x-custom.table.cell>

                            <x-custom.table.cell>
                                <x-custom.forms.input.find-small wire:model.lazy="abreviatura"/>
                            </x-custom.table.cell>

                            <x-custom.table.cell>
                                <x-custom.forms.buttons.link wire:click="cleanSort">
                                    Limpiar
                                </x-custom.forms.buttons.link>
                            </x-custom.table.cell>

                        </x-custom.table.row>
                        @forelse($grillas as $grilla)
                            <x-custom.table.row wire:loading.class="opacity-50">

                                <x-custom.table.cell>
                                    <div class="flex items-center">
                                        <div>
                                            <div class="text-base font-medium text-gray-900">
                                                <strong>{{ $grilla->nombre }}</strong>
                                            </div>
                                        </div>
                                    </div>
                                </x-custom.table.cell>

                                <x-custom.table.cell>
                                    <div class="flex items-center">
                                        <div>
                                            <div class="text-base font-medium text-gray-900">
                                                <strong>{{ $grilla->abreviatura }}</strong>
                                            </div>
                                        </div>
                                    </div>
                                </x-custom.table.cell>

                                <x-custom.table.cell class="text-sm-right">
                                    <x-custom.forms.buttons.link href="{{ route('grillas.editar', $grilla->id) }}">
                                        Editar
                                    </x-custom.forms.buttons.link>
                                </x-custom.table.cell>

                            </x-custom.table.row>

                        @empty
                            <x-custom.table.row wire:loading.class.delay="opacity-50">
                                <x-custom.table.cell colspan="5">
                                    <div class="flex justify-center items-center space-x-2 text-cool-gray-400">
                                        <x-custom.icon.small-search-document />
                                        <span class="font-medium py-8 text-cool-gray-400 text-xl">
                                        No se encontraron grillas...
                                    </span>
                                    </div>
                                </x-custom.table.cell>
                            </x-custom.table.row>
                        @endforelse
                    </x-slot>

                </x-custom.table>

                <div class="px-6 py-4 bg-white shadow rounded-b-md">
                    {!! $grillas->links('pagination::tailwind') !!}
                </div>

            </div>
        </div>
    </div>
</div>
