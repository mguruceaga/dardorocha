<x-custom.table.row>
    <x-custom.table.cell>
        <div class="flex items-center">
            <div>
                <div class="text-base font-medium text-gray-900">
                    <strong>{{ $propuesta->lineaCredito()->nombre }}</strong>
                </div>
                <div class="text-xs text-gray-500">
                    {{ $propuesta->debito->nombre }}
                </div>
            </div>
        </div>
    </x-custom.table.cell>
    <x-custom.table.cell>
        <div class="flex items-center">
            <div>
                <div class="text-base text-gray-700">
                    {{ $propuesta->grilla->nombre }}
                </div>
                <div class="text-xs text-gray-500">
                    <a href="{{ asset($propuesta->path_documento) }}" target="_blank">Documento asociado</a>
                </div>
            </div>
        </div>
    </x-custom.table.cell>
    @if (($estadoActualSlug == 'aceptada') && ($gestionador))
        <x-custom.table.cell>
            @if ($propuesta->aceptada)
                <div class="flex items-center">
                    <select
                        wire:model="propuesta.fondista_id"
                        name="fondista_id" id="fondista_id"
                        class="select2 mt-1 block w-full py-2 px-3
                                border bg-white rounded-md shadow-sm
                                focus:outline-none focus:ring-indigo-500
                                focus:border-indigo-500 sm:text-sm">
                        <option value="0"> </option>
                        @foreach($fondistas as $fondista)
                            <option value="{{ $fondista->id }}">{{ $fondista->nombre }}</option>
                        @endforeach
                    </select>
                </div>
            @endif
        </x-custom.table.cell>
    @endif
    <x-custom.table.cell>
        <div class="flex items-center">
            <div>
                <div class="text-base text-gray-700 text-right">
                    ${{ number_format($propuesta->monto, 2, ',', '.') }}
                </div>
            </div>
        </div>
    </x-custom.table.cell>
    <x-custom.table.cell>
        <div class="flex items-center">
            <div>
                <div class="text-base text-gray-700 text-right">
                    {{ $propuesta->cuotas }}
                </div>
            </div>
        </div>
    </x-custom.table.cell>
    <x-custom.table.cell>
        <div class="flex items-center">
            <div>
                <div class="text-base text-gray-700 text-right">
                    ${{ number_format($propuesta->valor_cuota, 2, ',', '.') }}
                </div>
            </div>
        </div>
    </x-custom.table.cell>
    <x-custom.table.cell>
        <div class="flex items-center">
            <div>
                <div class="text-base text-gray-700 text-right">
                    ${{ number_format($propuesta->adicionales, 2, ',', '.') }}
                </div>
            </div>
        </div>
    </x-custom.table.cell>
    <x-custom.table.cell>
        <div class="flex items-center">
            <div>
                <div class="text-base text-gray-700 text-right">
                    ${{ number_format($propuesta->valor_cuota_social, 2, ',', '.') }}
                </div>
            </div>
        </div>
    </x-custom.table.cell>

    <x-custom.table.cell>
        <div class="flex items-center">
            @if (($estadoActualSlug == 'en_revision') && $gestionador)
                <a wire:click.prevent="delete({{ $propuesta->id }})"
                   class="inline-flex items-center justify-center w-6 h-6 mr-2
                        transition-colors duration-150 rounded-full focus:shadow-outline
                        border border-red-500 text-red-500 hover:bg-red-500 hover:text-white">
                <span class="relative inline-block">
                  <svg class="h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                      <path fill-rule="evenodd" d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clip-rule="evenodd"></path>
                   </svg>
                </span>
                </a>
            @elseif ($estadoActualSlug == 'preaprobada' && (!$gestionador || ($precarga->user_id == auth()->user()->id)))
                @if ($precarga->propuesta_multiple)
                    <input wire:model="propuesta.aceptada" type="checkbox" class="form-checkbox h-5 w-5 text-blue-400 border-blue-400" name="aceptada" value="1">
                @else
                    <input wire:model="propuesta.aceptada"
                           type="radio"
                           class="form-radio h-5 w-5 text-blue-400 border-blue-400" name="aceptada" value="1">
                @endif
            @elseif ($estadoActualSlug == 'aceptada')
                @if ($propuesta->aceptada)
                    <span class="relative inline-block">
                        <svg viewBox="0 0 8 8" class="w-3 h-3 mr-6 overflow-visible text-indigo-500">
                            <path d="M 6 18 V 500" fill="none" stroke-width="1" stroke="#EEE"
                                  class="text-gray-200">
                            </path>
                            <circle cx="6" cy="6" r="14" fill="#FFF" stroke="currentColor"></circle>
                            <path d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" fill="currentColor" stroke="#FFF"
                                  transform="translate(-4, -4)">
                            </path>
                        </svg>
                    </span>
                @endif
            @endif
        </div>
    </x-custom.table.cell>
</x-custom.table.row>
