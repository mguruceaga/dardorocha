<div>
    <x-custom.table>
        <x-slot name="head">
            <x-custom.table.heading class="w-2/12">
                Línea de Crédito
            </x-custom.table.heading>
            <x-custom.table.heading class="w-4/12">
                Grilla<br/>
                Fondista
            </x-custom.table.heading>
            <x-custom.table.heading class="w-2/12">
                Monto<br/>
                Valor / Cuotas
            </x-custom.table.heading>
            <x-custom.table.heading class="w-2/12">
                Serv.Adicionales<br/>
                Cuota Social
            </x-custom.table.heading>
            <x-custom.table.heading class="w-2/12">
                @if ($propuesta->legajo_confirmado)
                    Descargar
                @endif
            </x-custom.table.heading>
        </x-slot>
        <x-slot name="body">
            <x-custom.table.row>
                <x-custom.table.cell>
                    <div class="flex items-center">
                        <div>
                            <div class="text-base font-medium text-gray-900">
                                <strong>{{ $propuesta->lineaCredito()->nombre }}</strong>
                            </div>
                            <div class="text-xs text-gray-500">
                                {{ $propuesta->debito->nombre }}
                            </div>
                        </div>
                    </div>
                </x-custom.table.cell>
                <x-custom.table.cell>
                    <div class="flex items-center">
                        <div>
                            <div class="text-base text-gray-700">
                                {{ $propuesta->grilla->nombre }}
                            </div>
                            <div class="text-base text-gray-700">
                                {{ $propuesta->fondista->nombre }}
                            </div>
                        </div>
                    </div>
                </x-custom.table.cell>
                <x-custom.table.cell>
                    <div class="flex items-right">
                        <div>
                            <div class="text-xs text-gray-700 text-right">
                                ${{ number_format($propuesta->monto, 2, ',', '.') }}
                            </div>
                            <div class="text-xs text-gray-700 text-right">
                                ${{ number_format($propuesta->valor_cuota, 2, ',', '.') }} / {{ $propuesta->cuotas }}
                            </div>
                        </div>
                    </div>
                </x-custom.table.cell>
                <x-custom.table.cell>
                    <div class="flex items-right">
                        <div>
                            <div class="text-xs text-gray-700 text-right">
                                ${{ number_format($propuesta->adicionales, 2, ',', '.') }}
                            </div>
                            <div class="text-xs text-gray-700 text-right">
                                ${{ number_format($propuesta->valor_cuota_social, 2, ',', '.') }}
                            </div>
                        </div>
                    </div>
                </x-custom.table.cell>
                <x-custom.table.cell>
                    @if ($propuesta->legajo_confirmado)
                        <a href="#" wire:click.prevent="descargarLegajo({{ $propuesta->id }})"
                           class="inline-flex items-center justify-center w-10 h-10 mr-2 transition-colors duration-150 rounded-full focus:shadow-outline border border-indigo-500 text-indigo-500 hover:bg-indigo-500 hover:text-white">
                            <span class="relative inline-block">
                                <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                  <path fill-rule="evenodd" d="M3 17a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zm3.293-7.707a1 1 0 011.414 0L9 10.586V3a1 1 0 112 0v7.586l1.293-1.293a1 1 0 111.414 1.414l-3 3a1 1 0 01-1.414 0l-3-3a1 1 0 010-1.414z" clip-rule="evenodd" />
                                </svg>
                            </span>
                        </a>
                        @if ($propuesta->legajo_firmado)
                            <a href="#" wire:click.prevent="descargarLegajoFirmado({{ $propuesta->id }})"
                               class="inline-flex items-center justify-center w-10 h-10 mr-2 transition-colors duration-150 rounded-full focus:shadow-outline border border-green-500 text-green-500 hover:bg-green-500 hover:text-white">
                                <span class="relative inline-block">
                                    <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path d="M17.414 2.586a2 2 0 00-2.828 0L7 10.172V13h2.828l7.586-7.586a2 2 0 000-2.828z" />
                                        <path fill-rule="evenodd" d="M2 6a2 2 0 012-2h4a1 1 0 010 2H4v10h10v-4a1 1 0 112 0v4a2 2 0 01-2 2H4a2 2 0 01-2-2V6z" clip-rule="evenodd" />
                                    </svg>
                                </span>
                            </a>
                        @endif
                    @endif
                </x-custom.table.cell>
            </x-custom.table.row>
        </x-slot>
    </x-custom.table>
    <x-custom.table>
        <x-slot name="head">
            <x-custom.table.heading class="w-4/12">
                <strong>Formularios Obligatorios</strong>
            </x-custom.table.heading>
            <x-custom.table.heading class="w-4/12">
                <strong>Formularios Opcionales</strong>
            </x-custom.table.heading>
            <x-custom.table.heading class="w-4/12">
                <strong>Formularios Extras</strong>
            </x-custom.table.heading>
        </x-slot>
        <x-slot name="body">
            <x-custom.table.row>
                <x-custom.table.cell class="align-text-top p-4">
                    <x-custom.table class="m-4">
                        <x-slot name="head">
                        </x-slot>
                        <x-slot name="body">
                            @foreach($obligatorios as $formulario)
                                <x-custom.table.row>
                                    <x-custom.table.cell>
                                        <div class="flex items-center">
                                            <div>
                                                <input type="checkbox" name="activo" value="1" checked="checked" disabled
                                                       class="form-checkbox h-4 w-4 text-gray-400 border-gray-400 mr-4 absolute"
                                                       style="margin-left: -10px">
                                                <div class="text-sm text-gray-700" style="padding-left: 20px">
                                                    {{ $formulario->formulario->nombre }}
                                                </div>
                                                @if ($propuesta->legajo_confirmado)
                                                    <div class="text-sm text-gray-700" style="padding-left: 40px">
                                                        <ul class="list-disc">
                                                            @if ($formulario->original_bloqueado)
                                                                <li>Original</li>
                                                            @endif
                                                            @if ($formulario->copias_identicas)
                                                                <li>Copias: {{ $formulario->copias_identicas }}</li>
                                                            @endif
                                                            @if ($formulario->copias_blanco)
                                                                <li>Copias en blanco: {{ $formulario->copias_blanco }}</li>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </x-custom.table.cell>
                                </x-custom.table.row>
                            @endforeach
                        </x-slot>
                    </x-custom.table>
                </x-custom.table.cell>
                @if ($opcionales->count() > 0)
                    <x-custom.table.cell class="align-text-top p-4">
                        <x-custom.table class="m-4">
                            <x-slot name="head">
                            </x-slot>
                            <x-slot name="body">
                                @foreach($opcionales as $formulario)
                                    <x-custom.table.row>
                                        <x-custom.table.cell>
                                            <div class="flex items-center">
                                                <livewire:propuesta-formulario-opcional wire:key="{{ $propuesta->id }}" :propuesta="$propuesta" :formulario="$formulario" />
                                            </div>
                                        </x-custom.table.cell>
                                    </x-custom.table.row>
                                @endforeach
                            </x-slot>
                        </x-custom.table>
                    </x-custom.table.cell>
                @else
                    <x-custom.table.cell class="align-text-top p-4">
                    </x-custom.table.cell>
                @endif
                @if ($extras->count() > 0)
                    <x-custom.table.cell class="align-text-top p-4">
                        <x-custom.table class="m-4">
                            <x-slot name="head">
                            </x-slot>
                            <x-slot name="body">
                                @foreach($extras as $formulario)
                                    <x-custom.table.row>
                                        <x-custom.table.cell>
                                            <div class="flex items-center">
                                                <div>
                                                    <input type="checkbox" name="activo" value="1" checked="checked" disabled
                                                           class="form-checkbox h-4 w-4 text-gray-400 border-gray-400 mr-4 absolute"
                                                           style="margin-left: -10px">
                                                    <div class="text-sm text-gray-700" style="padding-left: 20px">
                                                        {{ $formulario->formulario->nombre }}
                                                    </div>
                                                    @if ($propuesta->legajo_confirmado)
                                                        <div class="text-sm text-gray-700" style="padding-left: 40px">
                                                            <ul class="list-disc">
                                                                @if ($formulario->original_bloqueado)
                                                                    <li>Original</li>
                                                                @endif
                                                                @if ($formulario->copias_identicas)
                                                                    <li>Copias: {{ $formulario->copias_identicas }}</li>
                                                                @endif
                                                                @if ($formulario->copias_blanco)
                                                                    <li>Copias en blanco: {{ $formulario->copias_blanco }}</li>
                                                                @endif
                                                            </ul>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </x-custom.table.cell>
                                    </x-custom.table.row>
                                @endforeach
                            </x-slot>
                        </x-custom.table>
                    </x-custom.table.cell>
                @else
                    <x-custom.table.cell class="align-text-top p-4">
                    </x-custom.table.cell>
                @endif
            </x-custom.table.row>
            <!-- if ((($estadoActualSlug != 'autorizada') && ($estadoActualSlug != 'firmada')) && $gestionador) -->
            <x-custom.table.row>
                <x-custom.table.cell colspan="9" class="align-text-top p-4 text-right">
                    @if ($propuesta->legajo_confirmado)
                        @if (!$propuesta->legajo_firmado)
                            @if (($estadoActualSlug == 'autorizada') && (!$gestionador || ($precarga->user_id == auth()->user()->id)))
                                <div class="container">
                                    <div
                                        wire:ignore
                                        x-data="{pond{{ $propuesta->id }}: null}"
                                        x-init="
                                            FilePond.registerPlugin(FilePondPluginImagePreview);
                                            pond{{ $propuesta->id }} = FilePond.create($refs.input);
                                            pond{{ $propuesta->id }}.setOptions({
                                                allowMultiple: false,
                                                server: {
                                                    process: (fieldName, file, metadata, load, error, progress, abort, transfer, options) => {
                                                        @this.upload('legajoUpload', file, load, error, progress, abort, transfer, options)
                                                    },
                                                    revert: (filename, load) => {
                                                        @this.removeUpload('legajoUpload', filename, load)
                                                    },
                                                },
                                                // translate
                                                labelIdle: 'Click aquí para subir el legajo firmado.',
                                                labelFileLoading: 'Cargando',
                                                labelFileLoadError: 'Error durante la carga',
                                                labelFileProcessing: 'Cargando',
                                                labelFileProcessingComplete: 'Carga completa',
                                                labelFileProcessingAborted: 'Carga cancelada',
                                            });
                                        ">
                                        <input type="file" name="docs" x-ref="input" />
                                    </div>
                                </div>
                                @if (count($legajoUpload) > 0)
                                    <div class="container pt-3">
                                        <div class="text-right sm:px-6">
                                            <x-custom.forms.input.error
                                                :error="$errorLegajo"
                                                :message="$mensajeErrorLegajo" />
                                            <x-custom.forms.buttons.secondary wire:click.prevent="guardarLegajoFirmado({{ $propuesta->id }})" color="green">
                                                Guardar legajo firmado
                                            </x-custom.forms.buttons.secondary>
                                        </div>
                                    </div>
                                @endif
                            @endif
                        @else
                            @if ($estadoActualSlug == 'autorizada')
                                <x-custom.table>
                                    <x-slot name="head">
                                    </x-slot>
                                    <x-slot name="body">
                                        <x-custom.table.row>
                                            <x-custom.table.cell class="w-2/5 align-text-top p-4">
                                                <div class="flex items-center">
                                                    <div>
                                                        <div class="text-base font-medium text-gray-900">
                                                            <strong>Documento Firmado:</strong>
                                                        </div>
                                                    </div>
                                                </div>
                                            </x-custom.table.cell>
                                            <x-custom.table.cell class="w-3/5 align-text-top p-4">
                                                <div class="flex items-center">
                                                    <div>
                                                        <div class="text-base font-medium text-gray-900">
                                                            <strong>{{ $propuesta->nombre_original_documento_firmado }}</strong>
                                                        </div>
                                                    </div>
                                                </div>
                                            </x-custom.table.cell>
                                            <x-custom.table.cell class="w-4/5 align-text-top p-4">
                                                <div class="flex items-center">
                                                    <div>
                                                        <div class="text-base font-medium text-gray-900">
                                                            {{ $propuesta->path_documento_firmado }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </x-custom.table.cell>
                                            <x-custom.table.cell class="w-1/5 items-right">
                                                <a href="#"
                                                   wire:click.prevent="borrarLegajoFirmado()"
                                                   class="inline-flex items-center justify-center w-10 h-10 mr-2
                                                            transition-colors duration-150 rounded-full
                                                            focus:shadow-outline border border-red-500
                                                            text-red-500 hover:bg-red-500 hover:text-white">
                                                    <span class="relative inline-block">
                                                      <x-custom.icon.small-trash />
                                                    </span>
                                                </a>
                                            </x-custom.table.cell>
                                        </x-custom.table.row>
                                        @if ($seguroDeEliminar)
                                            <x-custom.table.row>
                                                <x-custom.table.cell colspan="4" class="w-1/5 items-right">
                                                    <div class="container pt-3 pb-3">
                                                        <div class="text-right sm:px-6">
                                                            <x-custom.forms.input.error
                                                                :error="true"
                                                                :message="'¿Está seguro de eliminar este legajo ya firmado?'" />
                                                            <x-custom.forms.buttons.secondary wire:click.prevent="confirmarBorrarLegajoFirmado({{ $propuesta->id }})" color="green">
                                                                Eliminar legajo firmado
                                                            </x-custom.forms.buttons.secondary>
                                                            <x-custom.forms.buttons.secondary wire:click.prevent="cancelarBorrarLegajoFirmado()" color="gray">
                                                                Cancelar
                                                            </x-custom.forms.buttons.secondary>
                                                        </div>
                                                    </div>
                                                </x-custom.table.cell>
                                            </x-custom.table.row>
                                        @endif
                                    </x-slot>
                                </x-custom.table>
                            @endif
                        @endif
                    @endif
                    @if ($estadoActualSlug != 'firmada')
                        @if ($propuesta->legajo_confirmado)
                            @if ($gestionador && ($estadoActualSlug != 'autorizada'))
                                <x-custom.forms.buttons.primary wire:click.prevent="deshacerLegajo({{ $propuesta->id }})" color="gray">
                                    Deshacer legajo
                                </x-custom.forms.buttons.primary>
                            @endif
                        @else
                            <x-custom.forms.buttons.primary wire:click.prevent="confirmarLegajo({{ $propuesta->id }})" color="green">
                                Confirmar legajo
                            </x-custom.forms.buttons.primary>
                        @endif
                        @if ($gestionador && ($estadoActualSlug != 'autorizada'))
                            <x-custom.forms.buttons.secondary wire:click.prevent="changeState('rechazada')" color="red">
                                Rechazar
                            </x-custom.forms.buttons.secondary>
                        @endif
                    @endif
                </x-custom.table.cell>
            </x-custom.table.row>
        </x-slot>
    </x-custom.table>
</div>
