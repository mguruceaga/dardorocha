<x-custom.table>
    <x-slot name="head">
    </x-slot>
    <x-slot name="body">
        <x-custom.table.row>
            <x-custom.table.cell class="w-2/5 align-text-top p-4">
                <div class="flex items-center">
                    <div>
                        <div class="text-base font-medium text-gray-900">
                            <strong>{{ $documento->nombre }}</strong>
                        </div>
                        <div class="text-xs text-gray-500">
                            {{ $documento->sizeKb }}
                        </div>
                    </div>
                </div>
            </x-custom.table.cell>
            <x-custom.table.cell class="w-2/5 align-text-top p-4">
                <div class="flex items-center">
                    <x-custom.forms.input.select
                            wire:model="documentacionId"
                            name="documentacionId" id="documentacionId"
                            :options="$documentaciones"/>
                </div>
            </x-custom.table.cell>
            <x-custom.table.cell class="w-1/5 items-right">
                @if (($estadoActualSlug != 'anulada') && ($estadoActualSlug != 'rechazada') && ($estadoActualSlug != 'firmada') && ($estadoActualSlug != 'transferida'))
                    @php($roles = auth()->user()->getRoleNames())
                    @switch($estadoActualSlug)
                        @case('iniciada')
                        @case('en_revision')
                        @case('preaprobada')
                        @case('observada')
                            @if (($roles[0] == config('tools.constants.roles.administrador')) || (auth()->user()->id == $precarga->user_id))
                                <a href="#"
                                   wire:click.prevent="asociateDocumentacion()"
                                   class="inline-flex items-center justify-center w-10 h-10 mr-2
                                            transition-colors duration-150 rounded-full
                                            focus:shadow-outline border
                                            {{ $documento->documentacion_id == 0 ? ' border-purple-500 text-purple-500 ' :  ' border-green-500 bg-green-500 text-white' }}
                                             hover:bg-purple-500 hover:text-white">
                                    <span class="relative inline-block">
                                        <x-custom.icon.arrow-right />
                                    </span>
                                </a>
                                <a href="#"
                                   wire:click.prevent="deleteDocumentacion()"
                                   class="inline-flex items-center justify-center w-10 h-10 mr-2
                                            transition-colors duration-150 rounded-full
                                            focus:shadow-outline border border-red-500
                                            text-red-500 hover:bg-red-500 hover:text-white">
                                    <span class="relative inline-block">
                                      <x-custom.icon.small-trash />
                                    </span>
                                </a>
                            @endif
                            @break
                        @case('aceptada')
                            @if ($roles[0] == config('tools.constants.roles.administrador'))
                                <a href="#"
                                   wire:click.prevent="asociateDocumentacion()"
                                   class="inline-flex items-center justify-center w-10 h-10 mr-2
                                                transition-colors duration-150 rounded-full
                                                focus:shadow-outline border
                                                {{ $documento->documentacion_id == 0 ? ' border-purple-500 text-purple-500 ' :  ' border-green-500 bg-green-500 text-white' }}
                                       hover:bg-purple-500 hover:text-white">
                                        <span class="relative inline-block">
                                            <x-custom.icon.arrow-right />
                                        </span>
                                </a>
                                <a href="#"
                                   wire:click.prevent="deleteDocumentacion()"
                                   class="inline-flex items-center justify-center w-10 h-10 mr-2
                                                transition-colors duration-150 rounded-full
                                                focus:shadow-outline border border-red-500
                                                text-red-500 hover:bg-red-500 hover:text-white">
                                        <span class="relative inline-block">
                                          <x-custom.icon.small-trash />
                                        </span>
                                </a>
                            @endif
                            @break
                    @endswitch
                    <a href="{{ url(env('APP_URL_DOCUMENTS').$documento->path) }}"
                       download
                       class="inline-flex items-center justify-center w-10 h-10 mr-2
                                transition-colors duration-150 rounded-full
                                focus:shadow-outline border border-purple-500
                                text-purple-500 hover:bg-purple-500 hover:text-white">
                        <span class="relative inline-block">
                          <x-custom.icon.download />
                        </span>
                    </a>
                @endif
            </x-custom.table.cell>
        </x-custom.table.row>
        @if ($muestroPreview)
            <x-custom.table.row>
                <x-custom.table.cell colspan="3" class="align-text-top p-4">
                    @php($path = url(env('APP_URL_DOCUMENTS').$documento->path))
                    <section class="flex items-center justify-center">
                        <div class="shadow overflow-hidden border-b border-gray-200 w-full mb-12" >
                            <object data="{{ $path }}" type="application/pdf" width="100%" height="460px">
                                <iframe src="{{ $path }}" width="100%" height="460px">
                                    <p>Este browser no soporta archivos PDF</p>
                                </iframe>
                            </object>
                        </div>
                    </section>
                </x-custom.table.cell>
            </x-custom.table.row>
        @endif
    </x-slot>
</x-custom.table>
