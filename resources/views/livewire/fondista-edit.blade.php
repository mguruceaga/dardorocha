<div>
    <form wire:submit.prevent="save" autocomplete="off">

        <div class="shadow overflow-hidden sm:rounded-md">
            <div class="px-4 py-6 bg-white sm:p-6">

                <div class="px-4 sm:px-0">
                    <h3 class="text-lg font-medium leading-6 text-gray-900">Fondista</h3>
                </div>

                <div class="hidden sm:block" aria-hidden="true">
                    <div class="py-5">
                        <div class="border-t border-gray-200"></div>
                    </div>
                </div>

                <div class="grid grid-cols-6 gap-6 mb-4">

                    <div class="col-span-6 sm:col-span-4">
                        <x-custom.forms.input.group label="Nombre" for="nombre" :error="$errors->first('fondista.nombre')">
                            <x-custom.forms.input.text wire:model.defer="fondista.nombre" name="nombre" id="nombre"/>
                        </x-custom.forms.input.group>
                    </div>

                    <div class="col-span-6 sm:col-span-2">
                        <x-custom.forms.input.group label="Abreviatura" for="abreviatura" :error="$errors->first('fondista.abreviatura')">
                            <x-custom.forms.input.text wire:model.defer="fondista.abreviatura" name="abreviatura" id="abreviatura"/>
                        </x-custom.forms.input.group>
                    </div>

                </div>

                <div class="hidden sm:block" aria-hidden="true">
                    <div class="py-5">
                        <div class="border-t border-gray-200"></div>
                    </div>
                </div>

                <div class="text-right sm:px-6">
                    <a href="{{ url()->previous() }}" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-indigo hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Cancelar
                    </a>
                    <x-custom.forms.buttons.primary color="indigo">
                        Guardar
                    </x-custom.forms.buttons.primary>
                </div>

            </div>
        </div>

    </form>
</div>

@push('styles')

    <link rel="stylesheet" type="text/css" href="{{ asset('css/pikaday.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/select2.min.css') }}">

@endpush

@push('scripts')

    <script src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script src="{{ asset('js/pikaday.js') }}"></script>
    <script src="{{ asset('js/imask.js') }}" type="text/javascript"></script>

    <script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/select2.min.js') }}" type="text/javascript"></script>

@endpush
