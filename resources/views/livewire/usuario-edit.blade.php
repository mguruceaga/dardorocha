<div>

    <form wire:submit.prevent="save" autocomplete="off">

        <div class="shadow overflow-hidden sm:rounded-md">
            <div class="px-4 py-6 bg-white sm:p-6">

                <div class="px-4 sm:px-0">
                    <h3 class="text-lg font-medium leading-6 text-gray-900">Usuario</h3>
                </div>

                <div class="hidden sm:block" aria-hidden="true">
                    <div class="py-5">
                        <div class="border-t border-gray-200"></div>
                    </div>
                </div>

                <div class="grid grid-cols-6 gap-6 mb-4">
                    <div class="col-span-6 sm:col-span-2">
                        <x-custom.forms.input.group label="Nombre" for="name" :error="$errors->first('usuario.name')">
                            <x-custom.forms.input.text wire:model.defer="usuario.name" name="name" id="name"/>
                        </x-custom.forms.input.group>
                    </div>
                    <div class="col-span-6 sm:col-span-2">
                        <x-custom.forms.input.group label="Correo electrónico" for="email" :error="$errors->first('usuario.email')">
                            <x-custom.forms.input.text
                                wire:model.defer="usuario.email" name="email" id="email"
                                :disabled="isset($usuario->id) ? true : false"
                            />
                        </x-custom.forms.input.group>
                    </div>
                    <div class="col-span-6 sm:col-span-2">
                        <x-custom.forms.input.group label="Rol" for="usuarioRol" :error="$errors->first('usuarioRol')">
                            <x-custom.forms.input.select
                                wire:model="usuarioRol" name="usuarioRol" id="usuarioRol"
                                :error="$errors->first('usuarioRol')"
                                :options="$roles" />
                        </x-custom.forms.input.group>
                    </div>
                </div>

                <div class="grid grid-cols-6 gap-6 mb-4">
                    <div class="col-span-6 sm:col-span-2">
                        <x-custom.forms.input.group label="Suspendido" for="suspended" :error="$errors->first('usuario.suspended')">
                            <x-custom.forms.input.checkbox
                                wire:model.defer="suspended" name="suspended" id="suspended" value="1"
                                :value="$suspended">
                            </x-custom.forms.input.checkbox>
                        </x-custom.forms.input.group>
                    </div>
                    <div class="col-span-6 sm:col-span-2">
                        <x-custom.forms.input.group label="Recibe precargas" for="recibePrecargas" :error="$errors->first('recibePrecargas')">
                            <x-custom.forms.input.checkbox
                                wire:model.defer="recibePrecargas" name="recibePrecargas" id="recibePrecargas" value="1"
                                :value="$recibePrecargas">
                            </x-custom.forms.input.checkbox>
                        </x-custom.forms.input.group>
                    </div>
                </div>

                <div class="hidden sm:block" aria-hidden="true">
                    <div class="py-5">
                        <div class="border-t border-gray-200"></div>
                    </div>
                </div>

                @if (!$userDeleted)
                    <div class="grid grid-cols-6 gap-6 mb-4">
                        @if (!$deleteUser)
                            <div class="col-span-6 sm:col-span-2">
                                <x-custom.forms.buttons.link wire:click="consultDeleteUser()" class="text-right" color="indigo">
                                    Eliminar usuario
                                </x-custom.forms.buttons.link>
                            </div>
                        @else
                            <div class="col-span-6 sm:col-span-3">
                                <p>Estás seguro de eliminar el usuario?</p>
                                <x-custom.forms.buttons.secondary wire:click="cancelDeleteUser()" color="gray">
                                    No
                                </x-custom.forms.buttons.secondary>
                                <x-custom.forms.buttons.secondary wire:click="deleting()" color="red">
                                    Sí
                                </x-custom.forms.buttons.secondary>
                            </div>
                        @endif
                    </div>
                @else
                    <div class="grid grid-cols-6 gap-6 mb-4">
                        <div class="col-span-6 sm:col-span-3">
                            <p>Existe un usuario con ese correo electrónico que se encuentra eliminado, ¿querés recuperarlo?</p>
                            <x-custom.forms.buttons.secondary wire:click="cancelRestoreUser()" color="gray">
                                No
                            </x-custom.forms.buttons.secondary>
                            <x-custom.forms.buttons.secondary wire:click="restoring()" color="red">
                                Sí
                            </x-custom.forms.buttons.secondary>
                        </div>
                    </div>
                @endif

                <div class="hidden sm:block" aria-hidden="true">
                    <div class="py-5">
                        <div class="border-t border-gray-200"></div>
                    </div>
                </div>

                <div class="text-right sm:px-6">
                    <a href="{{ url()->previous() }}" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-indigo hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Cancelar
                    </a>
                    <x-custom.forms.buttons.primary color="indigo">
                        Guardar
                    </x-custom.forms.buttons.primary>
                </div>

            </div>
        </div>

    </form>
</div>

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/pikaday.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script src="{{ asset('js/pikaday.js') }}"></script>
    <script src="{{ asset('js/imask.js') }}" type="text/javascript"></script>
    <script>

    </script>
@endpush
