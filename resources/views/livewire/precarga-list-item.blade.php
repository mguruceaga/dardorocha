<x-custom.table.row wire:loading.class.delay="opacity-50">
    <x-custom.table.cell>
        <div class="flex items-center">
            <div>
                <div class="text-base font-medium text-gray-900">
                    <strong>{{ $precarga->apellido }} {{ $precarga->nombre }}</strong>
                </div>
                @if ($userRol->name != config('tools.constants.roles.comercial'))
                    <div class="text-xs text-gray-500">
                        {{ $precarga->reparticion->nombre }}
                    </div>
                @endif
                <div class="text-xs text-gray-500">
                    {{ $precarga->documentoTipo->abreviatura }} {{ $precarga->numero_doc }}
                </div>
                <div class="text-xs text-gray-500">
                    {{ $precarga->created_at }}
                </div>
            </div>
        </div>
    </x-custom.table.cell>
    <x-custom.table.cell>
        @if ($userRol->name != config('tools.constants.roles.comercial'))
            <div class="flex items-center">
                <div>
                    <div class="text-base font-medium text-gray-900">
                        <strong>{{ $precarga->user->name }}</strong>
                    </div>
                    <div class="text-xs text-gray-500">
                        {{ $precarga->user->email }}
                    </div>
                </div>
            </div>
        @else
            <div>
                <div class="text-base font-medium text-gray-900">
                    <strong>{{ $precarga->reparticion->nombre }}</strong>
                </div>
                <div class="text-xs text-gray-500">
                    @php($contratacionTipo = $precarga->contratacionTipo)
                    @if (!is_null($contratacionTipo))
                        {{ $precarga->contratacionTipo->descripcion }}
                    @endif
                </div>
                <div class="text-xs text-gray-500">

                </div>
            </div>
        @endif
    </x-custom.table.cell>
    <x-custom.table.cell>
        @php($precargaEstadoActual = $precarga->precargaEstadoActual())
        <div class="flex items-center">
            <div>
                <div class="text-base font-medium text-gray-900">
                                                <span class="py-2 px-2 w-full inline-flex text-lg leading-5 font-semibold rounded-md bg-{{ $precargaEstadoActual->color }}-500 text-white">
                                                    {{ $precargaEstadoActual->nombre }}
                                                </span>
                </div>
                @if (($precargaEstadoActual->slug == 'en_revision') || ($precargaEstadoActual->slug == 'aceptada'))
                    <div class="text-base font-medium text-gray-900">
                        {{ $precargaEstadoActual->pivot->created_at->diffAsCarbonInterval(\Carbon\Carbon::now())->forHumans(['parts' => 4,'short' => true,'syntax' => \Carbon\CarbonInterface::DIFF_ABSOLUTE]) }}
                    </div>
                @endif
            </div>
        </div>
    </x-custom.table.cell>
    @if ($userRol->name == config('tools.constants.roles.administrador'))
        <x-custom.table.cell>
            @if (!is_null($precarga->user_asignado_id))
                <div class="flex items-center">
                    <div>
                        <div class="text-base font-medium text-gray-900">
                            <x-custom.forms.input.select
                                wire:model="selectUser" name="selectUser" id="selectUser"
                                :error="$errors->first('selectUser')"
                                :nonzero="true"
                                :options="$users" />
                        </div>
                        <div class="text-base font-medium text-gray-900 float-right">
                            <a href="#" wire:click.prevent="changeUser()"
                               class="mt-1 inline-flex items-center justify-center w-6 h-6 mr-2 transition-colors duration-150 rounded-full focus:shadow-outline border border-indigo-500 text-indigo-500 hover:bg-indigo-500 hover:text-white">
                                <span class="relative inline-block">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20" fill="currentColor">
                                        <path fill-rule="evenodd" d="M3 3a1 1 0 00-1 1v12a1 1 0 102 0V4a1 1 0 00-1-1zm10.293 9.293a1 1 0 001.414 1.414l3-3a1 1 0 000-1.414l-3-3a1 1 0 10-1.414 1.414L14.586 9H7a1 1 0 100 2h7.586l-1.293 1.293z" clip-rule="evenodd" />
                                    </svg>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            @endif
        </x-custom.table.cell>
    @endif
    <x-custom.table.cell>
        @php($mensajesNoLeidos = $precarga->mensajesNoLeidos($userPerfil))
        @if ($mensajesNoLeidos > 0)
            <a href="{{ route('precargas.editar', [(new \App\Tools\HelpersTool)->helper_getUrlHash($precarga->id), 'mensajes']) }}"
               class="inline-flex items-center justify-center w-10 h-10 mr-2 transition-colors duration-150 rounded-full focus:shadow-outline border border-indigo-500 text-indigo-500 hover:bg-indigo-500 hover:text-white">
                                            <span class="relative inline-block">
                                              <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                  <path d="M18 5v8a2 2 0 01-2 2h-5l-5 4v-4H4a2 2 0 01-2-2V5a2 2 0 012-2h12a2 2 0 012 2zM7 8H5v2h2V8zm2 0h2v2H9V8zm6 0h-2v2h2V8z" clip-rule="evenodd" fill-rule="evenodd"></path>
                                              </svg>
                                              <span class="absolute top-0 right-0 inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-white transform translate-x-1/2 -translate-y-1/2 bg-pink-600 rounded-full">{{ $mensajesNoLeidos }}</span>
                                            </span>
            </a>
        @else
            <a href="{{ route('precargas.editar', [(new \App\Tools\HelpersTool)->helper_getUrlHash($precarga->id), 'precarga']) }}"
               class="inline-flex items-center justify-center w-10 h-10 mr-2 transition-colors duration-150 rounded-full focus:shadow-outline border border-indigo-500 text-indigo-500 hover:bg-indigo-500 hover:text-white">
                                            <span class="relative inline-block">
                                              <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                  <path d="M17.414 2.586a2 2 0 00-2.828 0L7 10.172V13h2.828l7.586-7.586a2 2 0 000-2.828z" />
                                                  <path fill-rule="evenodd" d="M2 6a2 2 0 012-2h4a1 1 0 010 2H4v10h10v-4a1 1 0 112 0v4a2 2 0 01-2 2H4a2 2 0 01-2-2V6z" clip-rule="evenodd" />
                                               </svg>
                                            </span>
            </a>
        @endif
    </x-custom.table.cell>
</x-custom.table.row>
