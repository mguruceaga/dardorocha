<div>
    <form wire:submit.prevent="save" autocomplete="off">

        <div class="shadow overflow-hidden sm:rounded-md">
            <div class="px-4 py-6 bg-white sm:p-6">

                <div class="px-4 sm:px-0">
                    <h3 class="text-lg font-medium leading-6 text-gray-900">Origen de la Repartición</h3>
                </div>

                <div class="hidden sm:block" aria-hidden="true">
                    <div class="py-5">
                        <div class="border-t border-gray-200"></div>
                    </div>
                </div>

                <div class="grid grid-cols-6 gap-6 mb-4">
                    <div class="col-span-6 sm:col-span-6">
                        <x-custom.forms.input.group label="Nombre" for="nombre" :error="$errors->first('zona.nombre')">
                            <x-custom.forms.input.text wire:model.defer="zona.nombre" name="nombre" id="nombre"/>
                        </x-custom.forms.input.group>
                    </div>
                </div>

                <div class="grid grid-cols-6 gap-6 mb-4">

                    @foreach($origenes as $origen)
                        <div class="col-span-6 sm:col-span-1">
                            <x-custom.forms.input.group label="{{ $origen->descripcion }}" for="origenesAsociados" :error="false">
                                <input type="checkbox"
                                       wire:model="origenesSeteados.{{ $origen->id }}"
                                       name="origenesSeteados"
                                       value="1"
                                       class="border-gray-50"
                                       {{ $reparticionesPorOrigenes[$origen->id] ? 'disabled' : '' }}
                                />
                            </x-custom.forms.input.group>
                            @if ($reparticionesPorOrigenes[$origen->id])
                                <h6 class="text-xs">{{ $reparticionesPorOrigenes[$origen->id] }} Reparticion{{ ($reparticionesPorOrigenes[$origen->id] > 1) ? 'es' : '' }}</h6>
                            @endif
                        </div>
                    @endforeach

                </div>

                <div class="hidden sm:block" aria-hidden="true">
                    <div class="py-5">
                        <div class="border-t border-gray-200"></div>
                    </div>
                </div>

                <div class="text-right sm:px-6">
                    <a href="{{ url()->previous() }}" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-indigo hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Cancelar
                    </a>
                    <x-custom.forms.buttons.primary color="indigo">
                        Guardar
                    </x-custom.forms.buttons.primary>
                </div>

            </div>
        </div>

    </form>
</div>

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/pikaday.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script src="{{ asset('js/pikaday.js') }}"></script>
    <script src="{{ asset('js/imask.js') }}" type="text/javascript"></script>
    <script>

    </script>
@endpush
