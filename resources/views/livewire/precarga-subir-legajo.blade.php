<div class="shadow overflow-hidden sm:rounded-md z-0 w-full">
    <div class="px-4 py-6 bg-white sm:p-6">
        <!-- if ($propuestas->count() > 0) -->
            @if ($gestionador)
                <x-custom.table>
                    <x-slot name="head">
                    </x-slot>
                    <x-slot name="body">
                        <x-custom.table.row>
                            <x-custom.table.cell class="align-text-top p-4 w-3/12">
                                <div class="flex items-center">
                                    <div>
                                        <div class="text-base font-medium text-gray-900">
                                            <strong>Propuesta</strong>
                                        </div>
                                        <div class="text-xs text-gray-500">
                                            <select
                                                wire:model="propuesta_id"
                                                name="propuesta_id" id="propuesta_id"
                                                onkeydown="return false"
                                                class="select2 mt-1 block w-full py-2 px-3
                                                            border bg-white rounded-md shadow-sm
                                                            focus:outline-none focus:ring-indigo-500
                                                            focus:border-indigo-500 sm:text-sm">
                                                @if ($propuesta_id == 0)
                                                    <option wire:key="0" value="0" selected>Seleccione una opción</option>
                                                @endif
                                                @foreach($propuestas as $propuesta)
                                                    <option wire:key="{{ $propuesta->id }}" value="{{ $propuesta->id }}">{{ $propuesta->lineaCredito()->nombre  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </x-custom.table.cell>
                            <x-custom.table.cell class="align-text-top p-4 w-3/12">
                                <div class="flex items-center">
                                    <div>
                                        <div class="text-base font-medium text-gray-900">
                                            <strong>Tipo de Documento</strong>
                                        </div>
                                        <div class="text-xs text-gray-500">
                                            <select
                                                wire:model="tipo_documento"
                                                name="tipo_documento" id="tipo_documento"
                                                onkeydown="return false"
                                                class="select2 mt-1 block w-full py-2 px-3
                                                            border bg-white rounded-md shadow-sm
                                                            focus:outline-none focus:ring-indigo-500
                                                            focus:border-indigo-500 sm:text-sm">
                                                @if ($gestionador)
                                                    <option wire:key="1" value="1" selected>Legajo sin firmas</option>
                                                @elseif ($elem->user_id == auth()->user()->id)
                                                    <option wire:key="2" value="2" selected>Legajo con firmas</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </x-custom.table.cell>
                            <x-custom.table.cell class="align-text-top p-4 w-6/12">
                                @if ($subidaErrores != '')
                                    <div class="flex items-center">
                                        <div>
                                            <div class="text-base font-medium text-red-900">
                                                <strong>Error</strong>
                                            </div>
                                            <div class="text-xs text-red-500">
                                                {{ $subidaErrores }}
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </x-custom.table.cell>
                        </x-custom.table.row>
                        <x-custom.table.row>
                            <x-custom.table.cell class="align-text-top p-4" colspan="3">
                                <div class="container">
                                    <div>
                                        <x-custom.forms.input.filepond wire:model="filesLegajos" />
                                    </div>
                                </div>
                            </x-custom.table.cell>
                        </x-custom.table.row>
                    </x-slot>
                </x-custom.table>
                @if (count($filesLegajos) > 0)
                    <x-custom.hr />
                    <div class="container">
                        <div class="text-right sm:px-6">
                            <x-custom.forms.buttons.secondary wire:click.prevent="save()" color="green">
                                Guardar
                            </x-custom.forms.buttons.secondary>
                        </div>
                    </div>
                @endif
                @if ($pasarAAprobada)
                    <x-custom.hr />
                    <div class="shadow overflow-hidden sm:rounded-md z-30 bg-gray-50">
                        <div class="px-4 py-6 bg-white sm:p-6">
                            <div class="text-right sm:px-6">
                               <x-custom.forms.buttons.secondary wire:click.prevent="changeState('rechazada')" color="red">
                                    Rechazar
                                </x-custom.forms.buttons.secondary>
                                <x-custom.forms.buttons.secondary wire:click.prevent="changeState('aprobada')" color="green">
                                    Aprobar
                                </x-custom.forms.buttons.secondary>
                            </div>
                        </div>
                    </div>
                @endif
            @elseif (($estadoActualSlug == 'aprobada') && ($elem->user_id == auth()->user()->id))
                <x-custom.table>
                    <x-slot name="head">
                    </x-slot>
                    <x-slot name="body">
                        <x-custom.table.row>
                            <x-custom.table.cell class="align-text-top p-4 w-3/12">
                                <div class="flex items-center">
                                    <div>
                                        <div class="text-base font-medium text-gray-900">
                                            <strong>Propuesta</strong>
                                        </div>
                                        <div class="text-xs text-gray-500">
                                            <select
                                                wire:model="propuesta_id"
                                                name="propuesta_id" id="propuesta_id"
                                                onkeydown="return false"
                                                class="select2 mt-1 block w-full py-2 px-3
                                                            border bg-white rounded-md shadow-sm
                                                            focus:outline-none focus:ring-indigo-500
                                                            focus:border-indigo-500 sm:text-sm">
                                                @if ($propuesta_id == 0)
                                                    <option wire:key="0" value="0" selected>Seleccione una opción</option>
                                                @endif
                                                @foreach($propuestas as $propuesta)
                                                    <option wire:key="{{ $propuesta->id }}" value="{{ $propuesta->id }}">{{ $propuesta->lineaCredito()->nombre  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </x-custom.table.cell>
                            <x-custom.table.cell class="align-text-top p-4 w-3/12">
                                <div class="flex items-center">
                                    <div>
                                        <div class="text-base font-medium text-gray-900">
                                            <strong>Tipo de Documento</strong>
                                        </div>
                                        <div class="text-xs text-gray-500">
                                            <select
                                                wire:model="tipo_documento"
                                                name="tipo_documento" id="tipo_documento"
                                                onkeydown="return false"
                                                class="select2 mt-1 block w-full py-2 px-3
                                                            border bg-white rounded-md shadow-sm
                                                            focus:outline-none focus:ring-indigo-500
                                                            focus:border-indigo-500 sm:text-sm">
                                                @if ($gestionador)
                                                    <option wire:key="1" value="1" selected>Legajo sin firmas</option>
                                                @elseif ($elem->user_id == auth()->user()->id)
                                                    <option wire:key="2" value="2" selected>Legajo con firmas</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </x-custom.table.cell>
                            <x-custom.table.cell class="align-text-top p-4 w-6/12">
                                @if ($subidaErrores != '')
                                    <div class="flex items-center">
                                        <div>
                                            <div class="text-base font-medium text-red-900">
                                                <strong>Error</strong>
                                            </div>
                                            <div class="text-xs text-red-500">
                                                {{ $subidaErrores }}
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </x-custom.table.cell>
                        </x-custom.table.row>
                        <x-custom.table.row>
                            <x-custom.table.cell class="align-text-top p-4" colspan="3">
                                <div class="container">
                                    <div>
                                        <x-custom.forms.input.filepond wire:model="filesLegajos" />
                                    </div>
                                </div>
                            </x-custom.table.cell>
                        </x-custom.table.row>
                    </x-slot>
                </x-custom.table>
                @if (count($filesLegajos) > 0)
                    <x-custom.hr />
                    <div class="container">
                        <div class="text-right sm:px-6">
                            <x-custom.forms.buttons.secondary wire:click.prevent="save()" color="green">
                                Guardar
                            </x-custom.forms.buttons.secondary>
                        </div>
                    </div>
                @endif
                @if ($pasarAFirmada)
                    <x-custom.hr />
                    <div class="shadow overflow-hidden sm:rounded-md z-30 bg-gray-50">
                        <div class="px-4 py-6 bg-white sm:p-6">
                            <div class="text-right sm:px-6">
                                <x-custom.forms.buttons.secondary wire:click.prevent="changeState('rechazada')" color="red">
                                    Rechazar
                                </x-custom.forms.buttons.secondary>
                                <x-custom.forms.buttons.secondary wire:click.prevent="changeState('firmada')" color="green">
                                    Confirmar Legajo Firmado
                                </x-custom.forms.buttons.secondary>
                            </div>
                        </div>
                    </div>
                @endif
            @elseif ($estadoActualSlug == 'firmada')
                @php($roles = auth()->user()->getRoleNames())
                @if ($roles[0] == config('tools.constants.roles.tesorero'))
                    <div class="shadow overflow-hidden sm:rounded-md z-30 bg-gray-50">
                        <div class="px-4 py-6 bg-white sm:p-6">
                            <div class="text-right sm:px-6">
                                <x-custom.forms.buttons.secondary wire:click.prevent="changeState('rechazada')" color="red">
                                    Rechazar
                                </x-custom.forms.buttons.secondary>
                                <x-custom.forms.buttons.secondary wire:click.prevent="changeState('transferida')" color="green">
                                    Confirmar el pago
                                </x-custom.forms.buttons.secondary>
                            </div>
                        </div>
                    </div>
                @endif
            @endif
        <!-- endif -->
    </div>
</div>
