<header>
    <section class="m-b-30 m-t-30">
        <div class="row pager">
            <div class="col ">
                <p>Fecha:</p>
            </div>
            <div class="col text-right ">
                <p>ASOCIADO Nº</p>
            </div>
        </div>
    </section>
</header>

<div id="cuerpo">
    <h3 class="text-center">{{ $nombreArchivo }}</h3>

    <p>Señor Presidente de……………………………………………………………<br/>
        Presente</p>
    <p>El que suscribe, cuyos datos de identidad constan al pie y revisten carácter de Declaración jurada, solicita al Sr. Presidente y por su intermedio al H. Consejo de Administración de la Cooperativa, se le admita en carácter de SOCIO de la misma, a cuyo efecto declara reunir las condiciones establecidas en los estatutos, cuyo texto conoce y acepta, sometiéndose desde ya a la fiel observancia de las prescripciones estatutarias y reglamentarias vigentes en la Entidad.</p>
    <p>Por medio de la presente, SI/NO adhiero al servicio de asistencia médica y SI/NO al servicio de asistencia farmacéutica.</p>
    <p>Autorizo en forma expresa e irrevocable al departamento de liquidaciones de sueldos de mi lugar de trabajo a que se descuenten de mis haberes los importes que pudieran corresponder por cuotas de servicios sociales, membresía o créditos. En este acto se me notifica el valor de la cuota social vigente al día de la fecha, el cual podrá ser modificado por el Consejo de Administración.</p>
    <p>Solicita además, suscribir ………… (…......................................................................... ) acciones de valor nominal $ ……… (……...........................…… pesos) cada una, por un total de $ …………… (pesos).</p>

    <p><span style="width: 200px">Socio (Apellido y Nombre):</span><span> ...</span></p>
    <p>Documento (DNI – LE – LC):</p>
    <p>Fecha de nacimiento:</p>
    <p>Domicilio real:</p>
    <p>Ciudad:</p>
    <p>Provincia:</p>
    <p>Teléfono:</p>
    <p>Email:</p>
    <p>Actividad/Ramo:</p>
    <p>Observaciones:</p>

    <p>Firma del Solicitante

        RESOLUCIÓN DEL CONSEJO DE ADMINISTRACIÓN: Acta nº .............. de fecha .........../….…../.............
        Nº de nuevo socio asignado: ...............................



        Firma por Consejo de Administración</p>

</div>
