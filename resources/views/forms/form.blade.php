<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<style>
    @page {
        size: 21cm 29.7cm;
        margin: 25mm 15mm 25mm 15mm;
    }
    article, aside, figcaption, footer, header, main, nav, section {
        display: block;
    }
    .row {
        display: flex;
        flex-wrap: wrap;
        margin-right: -15px;
        margin-left: -15px;
    }
    .col {
        position: relative;
        width: 100%;
        padding-right: 15px;
        padding-left: 15px;
    }
    .col {
        flex-basis: 0;
        flex-grow: 1;
        max-width: 100%;
    }
    .text-right {
        text-align: right!important;
    }
    .text-center {
        text-align: center!important;
    }
    .m-t-30 {
        margin-top: 20px!important;
    }
    .m-r-30 {
        margin-right: 20px!important;
    }
    .m-b-30 {
        margin-bottom: 20px!important;
    }
    .m-l-30 {
        margin-left: 20px!important;
    }
</style>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
</head>

<body>

    @include('forms.variantes.solicitud-admr')

</body>

</html>
