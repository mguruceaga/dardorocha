<x-app-layout>

    <x-slot name="header">
        <div class="sm:flex-1 sm:flex sm:items-center sm:justify-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Grillas') }}
                <p class="mt-1 text-sm text-gray-600"> Listado de grillas </p>
            </h2>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-10 sm:mt-0">
                <div class="md:grid md:grid-cols-3 md:gap-6">
                    <div class="md:col-span-1">
                        <div class="overflow-hidden">
                            <div class="px-4 py-6 sm:p-6">
                                <div class="px-4 sm:px-0">
                                    <div class="px-4 sm:px-0">
                                        <h3 class="text-lg font-medium leading-6 text-gray-900">Datos de la Grilla</h3>
                                    </div>
                                    <div class="hidden sm:block" aria-hidden="true">
                                        <div class="py-5">
                                            <div class="border-t border-gray-200"></div>
                                        </div>
                                    </div>
                                    <div class="px-4 mb-4 sm:px-0">
                                        <p class="mt-1 text-sm text-gray-600">
                                            Las grillas que se corresponden con las líneas de crédito.
                                        </p>
                                    </div>
{{--                                    <div class="px-4 mb-4 sm:px-0">--}}
{{--                                        <p class="mt-1 text-md-left text-gray-600">Datos a cargar:</p>--}}
{{--                                    </div>--}}
{{--                                    <div class="px-4 mb-4 sm:px-0">--}}
{{--                                        <ul class="list-disc list-outside pl-2 md:list-inside text-sm text-gray-600">--}}
{{--                                            <li><span class="font-bold underline">Nombre:</span> ...</li>--}}
{{--                                            <li><span class="font-bold underline">Abreviatura:</span>: ...</li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-5 md:mt-0 md:col-span-2">
                        <div class="flex flex-col">
                            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">

                                        @livewire('grilla-edit', ['grillaId' => (isset($grillaId) ? $grillaId : 0)])

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
