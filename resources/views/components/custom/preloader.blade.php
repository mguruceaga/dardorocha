<div id="loading-screen" class="w-full h-full fixed block top-0 left-0 bg-white opacity-75 z-50">
    <x-custom.icon.small-refresh class="object-cover w-20 h-20 border-2 border-gray-300 rounded-full text-pink" />
</div>