@props([
    'data' => '',
])
{{ info($data) }}
<button type="submit" {{ $attributes }}
        class="inline-flex justify-center py-2 px-4 border
        border-transparent shadow-sm text-sm font-medium
        rounded-md bg-{{ $data['color'] }}-400 hover:bg-{{ $data['color'] }}-600 hover:text-white
        focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-{{ $data['color'] }}-600">
    {{ $data['nombre'] }}
</button>
