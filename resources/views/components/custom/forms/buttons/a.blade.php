@props([
   'color' => 'gray',
])
<a {{ $attributes }}
   class="inline-flex justify-center py-2 px-4 border
        border-transparent shadow-sm text-sm font-medium
        rounded-md bg-{{ $color }}-200 hover:bg-{{ $color }}-500 hover:text-white
        focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-{{ $color }}-500">
        {{ $slot }}
</a>