@props([
    'disabled' => false,
    'error' => false,
])

<div class="flex rounded-md">

    <input {{ $attributes }} autocomplete="nope" type="text"
           {{ $disabled ? ' disabled="disabled"' : '' }}
           class="rounded-none flex-1 form-input block w-full
           transition duration-150 ease-in-out sm:text-sm sm:leading -5"
            {{ $error ? ' border-red-500' : ($disabled ? ' border-gray-50' : '') }}" />
    <span class="inline-flex items-center px-1 rounded-r-md text-gray-500 text-xs">
        <x-custom.icon.small-search />
    </span>

</div>
