@props([
    'message' => '',
    'error' => false,
])

@if ($error)
    <p class="text-sm text-red-600 mt-2">{{ $message }}</p>
@endif
