@props([
    'mensaje' => 'Click aquí para subir documentos',
    'name' => 'pond',
])


<div
        wire:ignore
        x-init="
            FilePond.setOptions({
                allowMultiple: {{ isset($attributes['multiple']) ? 'true' : 'false' }},
                server: {
                    process: (fieldName, file, metadata, load, error, progress, abort, transfer, options) => {
                        @this.upload('{{ $attributes['wire:model'] }}', file, load, error, progress, abort, transfer, options)
                    },
                    revert: (filename, load) => {
                        @this.removeUpload('{{ $attributes['wire:model'] }}', filename, load)
                    },
                },
                // translate
                labelIdle: '{{ $mensaje }}',
                labelInvalidField: 'El campo contiene archivos inválidos',
                labelFileWaitingForSize: 'Esperando tamaño',
                labelFileSizeNotAvailable: 'Tamaño no disponible',
                labelFileLoading: 'Cargando',
                labelFileLoadError: 'Error durante la carga',
                labelFileProcessing: 'Cargando',
                labelFileProcessingComplete: 'Carga completa',
                labelFileProcessingAborted: 'Carga cancelada',
                labelFileProcessingError: 'Error durante la carga',
                labelFileProcessingRevertError: 'Error durante la reversión',
                labelFileRemoveError: 'Error durante la eliminación',
                labelTapToCancel: 'Toca para cancelar',
                labelTapToRetry: 'Tocar para volver a intentar',
                labelTapToUndo: 'Tocar para deshacer',
                labelButtonRemoveItem: 'Eliminar',
                labelButtonAbortItemLoad: 'Abortar',
                labelButtonRetryItemLoad: 'Reintentar',
                labelButtonAbortItemProcessing: 'Cancelar',
                labelButtonUndoItemProcessing: 'Deshacer',
                labelButtonRetryItemProcessing: 'Reintentar',
                labelButtonProcessItem: 'Cargar',
                labelMaxFileSizeExceeded: 'El archivo es demasiado grande',
                labelMaxFileSize: 'El tamaño máximo del archivo es {filesize}',
                labelMaxTotalFileSizeExceeded: 'Tamaño total máximo excedido',
                labelMaxTotalFileSize: 'El tamaño total máximo del archivo es {filesize}',
                labelFileTypeNotAllowed: 'Archivo de tipo no válido',
                fileValidateTypeLabelExpectedTypes: 'Espera {allButLastType} o {lastType}',
                imageValidateSizeLabelFormatError: 'Tipo de imagen no compatible',
                imageValidateSizeLabelImageSizeTooSmall: 'La imagen es demasiado pequeña',
                imageValidateSizeLabelImageSizeTooBig: 'La imagen es demasiado grande',
                imageValidateSizeLabelExpectedMinSize: 'El tamaño mínimo es {minWidth} × {minHeight}',
                imageValidateSizeLabelExpectedMaxSize: 'El tamaño máximo es {maxWidth} × {maxHeight}',
                imageValidateSizeLabelImageResolutionTooLow: 'La resolución es demasiado baja',
                imageValidateSizeLabelImageResolutionTooHigh: 'La resolución es demasiado alta',
                imageValidateSizeLabelExpectedMinResolution: 'La resolución mínima es {minResolution}',
                imageValidateSizeLabelExpectedMaxResolution: 'La resolución máxima es {maxResolution}',
            });
            FilePond.create($refs.input_legajo);
        "
        x-on:removeall.window="FilePond.removeFiles();"
    >

    <input type="file" name="docs" x-ref="input_legajo" />

</div>
