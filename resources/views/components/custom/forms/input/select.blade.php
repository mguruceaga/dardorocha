@props([
    'options',
    'disabled' => false,
    'error' => false,
    'nonzero' => false,
])

<select
        {{ $attributes }}
        {{ $disabled ? ' disabled="disabled"' : '' }}
        class="select2 mt-1 block w-full py-2 px-3
        border bg-white rounded-md shadow-sm
        focus:outline-none focus:ring-indigo-500
        focus:border-indigo-500 sm:text-sm
        {{ $error ? ' border-red-500' : ($disabled ? ' border-gray-50' : '') }}">
    @if (!$nonzero)
        <option wire:key="0" value="0">Seleccione una opción</option>
    @endif
    @foreach ($options as $k => $v)
        <option wire:key="{{ $k }}" value="{{ $k }}">{{ $v }}</option>
    @endforeach
</select>
