@props([
    'disabled' => false,
    'error' => false,
])

<div
    x-data
    x-init="
                new Pikaday({ field: $refs.input, format: 'DD-MM-YYYY' })
                IMask($refs.input, {mask: '00-00-0000' });
           "
    class="flex rounded-md shadow-sm mt-1"
>

    <span class="inline-flex items-center px-3 rounded-l-md border border-r-0
                    {{ ($disabled ? ' border-gray-50' : 'border-gray-300') }}
                    bg-gray-50 text-gray-500 text-sm">
        <x-custom.icon.small-calendar/>
    </span>

    <input
        {{ $attributes }}
        x-ref="input"
        autocomplete="nope"
        type="text"
        placeholder="dd-mm-aaaa"
        {{ $disabled ? ' disabled="disabled"' : '' }}
        class="text-right rounded-none rounded-r-md flex-1 form-input block w-full
                            transition duration-150 ease-in-out sm:text-sm sm:leading -5
                            {{ $error ? ' border-red-500' : ($disabled ? ' border-gray-50' : '') }}"

    />

</div>
