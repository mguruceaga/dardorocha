@props([
'currency' => false,
'calendar' => false,
'mask' => 'Number',
'scale' => 0,
'thousandsSeparator' => '',
'disabled' => false,
'error' => false,
])

<div
    x-data=""
    @if ($mask == 'Number')
    x-init="IMask($refs.input, {mask: Number, min: 0, scale: '{{ $scale }}', signed: false, thousandsSeparator: '{{ $thousandsSeparator }}'});"
    @else
    x-init="IMask($refs.input, {mask: '{!! $mask !!}', min: 0, scale: '{{ $scale }}', signed: false, thousandsSeparator: '{{ $thousandsSeparator }}'});"
    @endif
    class="flex rounded-md shadow-sm mt-1
               {{ $error ? ' border-red-500' : ($disabled ? ' border-gray-50' : '') }}">

        <span class="inline-flex items-center px-3 rounded-l-md border border-r-0
        {{ ($disabled ? ' border-gray-50' : 'border-gray-300') }}
            bg-gray-50
            text-gray-500 text-sm">
        <x-custom.icon.small-phone />
    </span>

    <input {{ $attributes }} autocomplete="nope" type="text"
           x-ref="input"
           {{ $disabled ? ' disabled="disabled"' : '' }}
           class="rounded-none rounded-r-md
               flex-1 form-input block w-full text-right
               sm:text-sm sm:leading -5
                {{ $error ? ' border-red-500' : ($disabled ? ' border-gray-50' : '') }}" />

</div>
