@props([
       'disabled' => false,
       'error' => false,
])

<input {{ $attributes }}
       type="text"
       {{ $disabled ? ' disabled="disabled"' : '' }}
       class="form-input rounded-md shadow-sm mt-1 block w-full
       {{ $error ? ' border-red-500' : ($disabled ? ' border-gray-50' : '') }}"
/>
