@props([
    'label',
    'for',
    'currency' => false,
    'error' => false,
])

<label class="block font-medium text-sm text-gray-700" for="{{ $for }}">
    {{ $label }}
</label>

{{ $slot }}

@if ($error)
    <p class="text-sm text-red-600 mt-2">{{ $error }}</p>
@endif