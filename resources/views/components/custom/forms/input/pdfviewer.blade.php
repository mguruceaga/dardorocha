<div id="documento{{ $documento->id }}"
     class="shadow overflow-hidden w-full px-4 pb-4" style="height: 700px">
    <div class="px-4 py-6 bg-white sm:p-6">

        <div class="px-4 sm:px-0">
            <h3 class="text-lg font-medium leading-6 text-gray-900">{{ $documento->nombre }}</h3>
            <p class="mt-1 text-sm text-gray-600"></p>
        </div>

        <x-custom.hr />

        <section class="flex items-center justify-center">
            <div class="shadow overflow-hidden border-b border-gray-200 w-full mb-12" >
                <object data='{{ $path }}'
                        type="application/pdf"
                        width="100%"
                        height="580px"
                >
                    <iframe
                            src='{{ $path }}'
                            width="100%"
                            height="580px"
                    >
                        <p>Este browser no soporta archivos PDF</p>
                    </iframe>

                </object>
            </div>
        </section>

    </div>
</div>
