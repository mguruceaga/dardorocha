@props([
       'disabled' => false,
       'value' => 0,
])

<input {{ $attributes }} type="checkbox"
       {{ $value ? 'checked="checked"' : '' }}
       {{ $disabled ? ' disabled="disabled"' : '' }}
       class="{{ $errors ? ' border-red-500' : ($disabled ? ' border-gray-50' : '') }}"
/>
