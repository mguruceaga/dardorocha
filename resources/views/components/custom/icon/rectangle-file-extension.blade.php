<div {{ $attributes->merge(['class' => 'object-cover w-14 h-18 p-1 border flex items-center rounded-md'])->only('class') }}>
    <x-custom.icon.small-document-text />
</div>