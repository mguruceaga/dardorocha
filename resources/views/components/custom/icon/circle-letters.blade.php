@props([
    'fillColor' => 'green',
    'fontColor' => 'white',
])

<div class="object-cover w-12 h-12 p-1 border border-{{ $fillColor }}-500 flex items-center rounded-full">
    <div class="object-cover w-10 h-10 flex items-center rounded-full bg-{{ $fillColor }}-300">
        <p class="w-full text-center text-lg font-weight-bold text-{{ $fontColor }}">
            {{ $slot }}
        </p>
    </div>
</div>