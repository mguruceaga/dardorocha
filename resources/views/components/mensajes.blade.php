@props([
    'precarga' => null,
])

<section class="flex items-center justify-center">
    <div class="container space-y-4 ">

        @foreach($precarga->mensajes as $mensaje)
            <x-mensajes.bloque :precarga="$precarga" :mensaje="$mensaje"/>
        @endforeach


        <div class="hidden sm:block" aria-hidden="true">
            <div class="py-5">
                <div class="border-t border-gray-200"></div>
            </div>
        </div>

        <div class="px-4 sm:px-0">
            <h3 class="text-lg font-medium leading-6 text-gray-900">Nuevo mensaje</h3>
        </div>

        <div class="flex-col w-full py-4 bg-white border border-gray-200 sm:px-4 sm:py-4 md:px-4 sm:rounded-lg sm:shadow-sm">
            <div class="flex flex-row md-10">
                <x-custom.icon.circle-letters fillColor="{{ auth()->user()->color() }}">
                    {{ auth()->user()->iniciales() }}
                </x-custom.icon.circle-letters>
                    <div class="flex-col mt-1 w-full">
                        <div class="flex items-center flex-1 px-4 font-bold leading-tight">
                            {{ auth()->user()->name }}
                        </div>
                        <livewire:mensaje-edit :precarga="$precarga" :mensajeId="0" :key="'mensaje_0'" />
                    </div>
            </div>
        </div>

    </div>
</section>