@props([
    'precarga' => null,
    'mensaje' => null,
])

<div class="flex-col w-full py-4 bg-white border border-gray-200 sm:px-4 sm:py-4 md:px-4 sm:rounded-lg sm:shadow-sm">

    <livewire:mensaje-show :mensaje="$mensaje" :key="'mensaje_show_'.$mensaje->id" />

    <hr class="my-2 ml-16 border-gray-200">
    @if ($mensaje->respuestas()->count() > 0)
        @foreach($mensaje->respuestas() as $respuesta)
            <livewire:mensaje-show :mensaje="$respuesta" :key="'mensaje_show_'.$respuesta->id" />
            <hr class="my-2 ml-16 border-gray-200">
        @endforeach
    @endif
    <div class="flex flex-row pt-1 md-10 md:ml-16">
        <x-custom.icon.circle-letters fillColor="{{ auth()->user()->color() }}">
            {{ auth()->user()->iniciales() }}
        </x-custom.icon.circle-letters>
        <div class="flex-col mt-1 w-full">
            <div class="flex items-center flex-1 px-4 font-bold leading-tight">
                {{ auth()->user()->name }}
            </div>
            <livewire:mensaje-edit :precarga="$precarga" :mensajeId="$mensaje->id" :key="'mensaje_'.$mensaje->id" />
        </div>
    </div>
</div>