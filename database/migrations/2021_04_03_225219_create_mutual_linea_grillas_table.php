<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMutualLineaGrillasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mutual_linea_grillas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('mutual_linea_id');
            $table->foreign('mutual_linea_id')->references('id')->on('mutual_lineas');
            $table->unsignedInteger('grilla_id');
            $table->foreign('grilla_id')->references('id')->on('grillas');
            $table->date('desde')->nullable();
            $table->date('hasta')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mutual_linea_grillas');
    }
}
