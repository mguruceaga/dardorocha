<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPrefijoMovilYPrefijoFijoToPrecargas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('precargas', function (Blueprint $table) {
            $table->string('prefijo_movil')->nullable()->after('email');
            $table->string('prefijo_fijo')->nullable()->after('telefono_movil');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('precargas', function (Blueprint $table) {
            $table->dropColumn('prefijo_movil');
            $table->dropColumn('prefijo_fijo');
        });
    }
}
