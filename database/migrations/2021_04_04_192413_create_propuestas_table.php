<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propuestas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('linea_credito_id');
            $table->foreign('linea_credito_id')->references('id')->on('mutual_lineas');
            $table->unsignedInteger('grilla_id');
            $table->foreign('grilla_id')->references('id')->on('grillas');
            $table->double('monto')->nullable();
            $table->unsignedInteger('cuotas')->nullable();
            $table->double('valor_cuota')->nullable();
            $table->unsignedInteger('debito_id')->nullable();
            $table->foreign('debito_id')->references('id')->on('debitos');
            $table->double('adicionales')->nullable();
            $table->double('valor_cuota_social')->nullable();
            $table->unsignedInteger('fondista_id')->nullable();
            $table->foreign('fondista_id')->references('id')->on('fondistas');
            $table->unsignedInteger('precarga_id')->nullable();
            $table->foreign('precarga_id')->references('id')->on('precargas');
            $table->boolean('aceptada')->default(false);
            $table->boolean('legajo_confirmado')->default(false);
            $table->string('path_documento')->nullable();
            $table->boolean('legajo_firmado')->default(false);
            $table->string('path_documento_firmado')->nullable();
            $table->string('nombre_original_documento_firmado')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propuestas');
    }
}
