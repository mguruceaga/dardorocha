<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListaNegraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lista_negra', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('documento_tipo_id');
            $table->foreign('documento_tipo_id')->references('id')->on('documento_tipos');
            $table->integer('numero_doc');
            $table->string('nombre');
            $table->string('apellido');
            $table->date('fecha');
            $table->text('observacion')->nullable();
            $table->unsignedInteger('lista_negra_motivo_id');
            $table->foreign('lista_negra_motivo_id')->references('id')->on('lista_negra_motivos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lista_negra');
    }
}
