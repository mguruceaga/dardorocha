<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentacionAdjuntasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documentacion_adjuntas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('precarga_id');
            $table->foreign('precarga_id')->references('id')->on('precargas')->onDelete('cascade');
            $table->string('nombre')->nullable();
            $table->string('path')->nullable();
            $table->unsignedInteger('documentacion_id')->nullable();
            $table->foreign('documentacion_id')->references('id')->on('documentaciones')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documentacion_adjuntas');
    }
}
