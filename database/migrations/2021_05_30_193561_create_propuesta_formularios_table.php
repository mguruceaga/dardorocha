<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropuestaFormulariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propuesta_formularios', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('propuesta_id');
            $table->foreign('propuesta_id')->references('id')->on('propuestas');
            $table->unsignedInteger('formulario_id');
            $table->foreign('formulario_id')->references('id')->on('formularios');
            $table->unsignedInteger('archivo_id');
            $table->foreign('archivo_id')->references('id')->on('archivos');
            $table->unsignedInteger('tipo_relacion_id');
            $table->foreign('tipo_relacion_id')->references('id')->on('tipo_relaciones');
            $table->boolean('original_bloqueado');
            $table->integer('copias_identicas');
            $table->integer('copias_blanco');
            $table->boolean('activo');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propuesta_formularios');
    }
}
