<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReparticionRestriccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reparticion_restricciones', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('reparticion_id');
            $table->foreign('reparticion_id')->references('id')->on('reparticiones')->onDelete('cascade');
            $table->string('descripcion');
            $table->string('slug');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reparticion_restricciones');
    }
}
