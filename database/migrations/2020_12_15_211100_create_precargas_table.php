<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrecargasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('precargas', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('documento_tipo_id');
            $table->foreign('documento_tipo_id')->references('id')->on('documento_tipos');
            $table->integer('numero_doc')->nullable();
            $table->string('cuil_cuit')->nullable();
            $table->string('nombre');
            $table->string('apellido');
            $table->date('fecha_nacimiento')->nullable();
            $table->unsignedInteger('genero_id');
            $table->foreign('genero_id')->references('id')->on('generos');
            $table->unsignedInteger('pais_id')->nullable();
            $table->foreign('pais_id')->references('id')->on('paises');

            $table->unsignedInteger('sector_id');
            $table->foreign('sector_id')->references('id')->on('sectores');
            $table->unsignedInteger('grupo_pago_id');
            $table->foreign('grupo_pago_id')->references('id')->on('grupos_pagos');
            $table->unsignedInteger('zona_id');
            $table->foreign('zona_id')->references('id')->on('zonas');
            $table->unsignedInteger('reparticion_id');
            $table->foreign('reparticion_id')->references('id')->on('reparticiones');
            $table->string('dependencia');

            $table->double('cuota_maxima')->nullable();
            $table->double('plazo_maximo')->nullable();
            $table->double('monto_maximo')->nullable();

            $table->unsignedInteger('contratacion_tipo_id')->nullable();
            $table->foreign('contratacion_tipo_id')->references('id')->on('contratacion_tipos');
            $table->double('ingreso_laboral')->nullable();
            $table->integer('antiguedad')->nullable();
            $table->unsignedInteger('banco_id')->nullable();
            $table->foreign('banco_id')->references('id')->on('bancos');
            $table->string('cbu')->nullable();

            $table->string('email')->nullable();
            $table->string('telefono_movil')->nullable();
            $table->string('telefono_fijo')->nullable();
            $table->string('calle')->nullable();
            $table->string('numero')->nullable();
            $table->string('piso')->nullable();
            $table->string('departamento')->nullable();
            $table->unsignedInteger('codigo_postal_id')->nullable();
            $table->foreign('codigo_postal_id')->references('id')->on('codigos_postales');

            $table->boolean('propuesta_multiple')->default(false);
            $table->boolean('fondeada')->default(false);

            $table->unsignedInteger('precarga_estado_id');
            $table->foreign('precarga_estado_id')->references('id')->on('precarga_estados');
            $table->date('fecha_estado')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('user_asignado_id')->nullable();
            $table->foreign('user_asignado_id')->references('id')->on('users');
            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('precargas');
    }
}
