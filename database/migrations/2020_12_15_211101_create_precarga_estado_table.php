<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrecargaEstadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('precarga_estado', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('precarga_id');
            $table->foreign('precarga_id')->references('id')->on('precargas');
            $table->unsignedInteger('precarga_estado_id');
            $table->foreign('precarga_estado_id')->references('id')->on('precarga_estados');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->text('mensaje')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('precarga_estado');
    }
}
