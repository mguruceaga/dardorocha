<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrecargaLegajoFormulariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('precarga_legajo_formularios', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('propuesta_id');
            $table->foreign('propuesta_id')->references('id')->on('propuestas');
            $table->unsignedInteger('propuesta_formulario_id');
            $table->foreign('propuesta_formulario_id')->references('id')->on('propuesta_formularios');
            $table->string('nombre');
            $table->text('path');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('precarga_legajo_formularios');
    }
}
