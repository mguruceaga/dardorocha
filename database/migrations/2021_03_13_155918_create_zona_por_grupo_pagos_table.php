<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZonaPorGrupoPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zona_por_grupo_pagos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('grupo_pago_id');
            $table->foreign('grupo_pago_id')->references('id')->on('grupos_pagos')->onDelete('cascade');
            $table->unsignedInteger('zona_id');
            $table->foreign('zona_id')->references('id')->on('zonas')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zona_por_grupo_pagos');
    }
}
