<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCondicionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('condiciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nullable();
            $table->boolean('haber')->nullable();
            $table->boolean('cbu')->nullable();
            $table->boolean('relacion_dependencia')->nullable();
            $table->integer('edad_max_hombre')->nullable();
            $table->integer('edad_max_mujer')->nullable();
            $table->integer('antiguedad')->nullable();
            $table->integer('cuotas_minimas_haber')->nullable();
            $table->integer('cuotas_maximas_haber')->nullable();
            $table->integer('cuotas_minimas_cbu')->nullable();
            $table->integer('cuotas_maximas_cbu')->nullable();
            $table->double('monto_minimo')->nullable();
            $table->double('monto_maximo')->nullable();
            $table->string('observacion')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('condiciones');
    }
}
