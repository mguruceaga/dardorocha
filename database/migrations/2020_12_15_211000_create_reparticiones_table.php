<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReparticionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reparticiones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('cuit')->nullable();
            $table->unsignedInteger('zona_id');
            $table->foreign('zona_id')->references('id')->on('zonas');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reparticiones');
    }
}
