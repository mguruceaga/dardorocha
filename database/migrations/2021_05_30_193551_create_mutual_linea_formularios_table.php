<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMutualLineaFormulariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mutual_linea_formularios', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('mutual_linea_id');
            $table->foreign('mutual_linea_id')->references('id')->on('mutual_lineas');
            $table->unsignedInteger('formulario_id');
            $table->foreign('formulario_id')->references('id')->on('formularios');
            $table->unsignedInteger('tipo_relacion_id');
            $table->foreign('tipo_relacion_id')->references('id')->on('tipo_relaciones');
            $table->boolean('original_bloqueado');
            $table->integer('copias_identicas');
            $table->integer('copias_blanco');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mutual_linea_formularios');
    }
}
