<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCondicionPorReparticionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('condicion_por_reparticiones', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('condicion_id');
            $table->foreign('condicion_id')->references('id')->on('condiciones')->onDelete('cascade');
            $table->unsignedInteger('reparticion_id');
            $table->foreign('reparticion_id')->references('id')->on('reparticiones')->onDelete('cascade');
            $table->unsignedInteger('zona_id');
            $table->foreign('zona_id')->references('id')->on('zonas')->onDelete('cascade');
            $table->unsignedInteger('grupo_pago_id');
            $table->foreign('grupo_pago_id')->references('id')->on('grupos_pagos')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('condicion_por_reparticiones');
    }
}
