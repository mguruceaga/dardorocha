<?php

namespace Database\Factories;

use App\Models\ListaNegraMotivo;
use Illuminate\Database\Eloquent\Factories\Factory;

class BlacklistMotivoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ListaNegraMotivo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
