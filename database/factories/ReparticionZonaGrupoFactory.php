<?php

namespace Database\Factories;

use App\Models\ReparticionZonaGrupo;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReparticionZonaGrupoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ReparticionZonaGrupo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
