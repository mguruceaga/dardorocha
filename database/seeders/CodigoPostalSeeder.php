<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CodigoPostalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('codigos_postales')->insert([
            [   'codigo' => '1000',
                'localidad' => 'CORREO CENTRAL',
                'provincia_abrev' =>'0000',
                'provincia_id' => '1',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [
                'codigo' => '1123',
                'localidad' => 'RECOLETA',
                'provincia_abrev' =>'0000',
                'provincia_id' => '1',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [
                'codigo' => '1900',
                'localidad' => 'LA PLATA',
                'provincia_abrev' =>'0000',
                'provincia_id' => '1',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [
                'codigo' => '1708',
                'localidad' => 'MORON',
                'provincia_abrev' =>'0000',
                'provincia_id' => '1',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [
                'codigo' => '1704',
                'localidad' => 'RAMOS MEJIA',
                'provincia_abrev' =>'0000',
                'provincia_id' => '1',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],

        ]);
    }
}
