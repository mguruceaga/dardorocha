<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Condicion;

class CondicionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $archivo = fopen(public_path('/resources/condiciones_x_reparticion.csv'), 'r');
        while (($info = fgetcsv($archivo)) !== false) {
            if ($info[0] != 'nombre') {
                Condicion::create([
                    'nombre' => $info[0],
                    'haber' => $info[1],
                    'cbu' => $info[2],
                    'relacion_dependencia' => $info[3],
                    'edad_max_hombre' => $info[4],
                    'edad_max_mujer' => $info[5],
                    'antiguedad' => $info[6],
                    'cuotas_minimas_haber' => $info[7],
                    'cuotas_maximas_haber' => $info[8],
                    'cuotas_minimas_cbu' => $info[9],
                    'cuotas_maximas_cbu' => $info[10],
                    'monto_minimo' => $info[11],
                    'monto_maximo' => $info[12],
                    'observacion' => $info[13],
                    // 'reparticion_id' => $info[14],
                    'deleted_at' => NULL,
                    'updated_at' => NULL,
                    'created_at' => Carbon::now(),
                ]);
            }
        }
        fclose($archivo);
    }
}
