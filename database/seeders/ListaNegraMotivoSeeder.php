<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ListaNegraMotivoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lista_negra_motivos')->insert([
            [
                'nombre' => 'Cred Pendiente',
                'slug' => 'cred_pendiente',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
        ]);

        DB::table('lista_negra_motivos')->insert([
            [
                'nombre' => 'Quiebra',
                'slug' => 'quiebra',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
        ]);

        DB::table('lista_negra_motivos')->insert([
            [
                'nombre' => 'Sin Destino',
                'slug' => 'sin_destino',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
        ]);

        DB::table('lista_negra_motivos')->insert([
            [
                'nombre' => 'Sin motivo',
                'slug' => 'sin_motivo',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
        ]);
    }
}
