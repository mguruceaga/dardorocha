<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Grilla;

class GrillaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $archivo = fopen(public_path('/resources/grillas.csv'), 'r');
        while (($info = fgetcsv($archivo)) !== false) {
            if ($info[0] != 'nombre') {
                $grilla = Grilla::create([
                    'nombre' => $info[0],
                    'abreviatura' => $info[1],
                    'deleted_at' => NULL,
                    'updated_at' => NULL,
                    'created_at' => Carbon::now(),
                ]);
            }
        }
        fclose($archivo);
        
        
        /*DB::table('grillas')->insert([
            [   'nombre' => 'Grilla 1',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Grilla 2',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Grilla 3',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Grilla 4',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Grilla 5',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Grilla 6',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Grilla 7',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Grilla 8',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Grilla 9',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Grilla 10',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Grilla 11',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Grilla 12',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Grilla 13',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Grilla 14',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Grilla 15',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Grilla 16',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Grilla 17',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Grilla 18',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
        ]);*/
    }
}
