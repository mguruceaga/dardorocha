<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ReparticionZonaGrupo;
use Carbon\Carbon;

class ReparticionZonaGrupoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $archivo = fopen(public_path('/resources/reparticionZonaGrupo.csv'), 'r');
        while (($info = fgetcsv($archivo)) !== false) {
            if ($info[0] != 'alias') {
                $reparticionZonaGrupo = ReparticionZonaGrupo::create([
                    'alias' => $info[0],
                    'reparticion_id' => $info[1],
                    'zona_id' => $info[2],
                    'grupo_pago_id' => $info[3],
                    'deleted_at' => NULL,
                    'updated_at' => NULL,
                    'created_at' => Carbon::now(),
                ]);
            }
        }
        fclose($archivo);
    }
 
}
