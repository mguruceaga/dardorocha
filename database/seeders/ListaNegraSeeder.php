<?php

namespace Database\Seeders;

use App\Models\Archivo;
use App\Models\ListaNegra;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ListaNegraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $archivo = fopen(public_path('/resources/listanegra.csv'), 'r');
        while (($info = fgetcsv($archivo)) !== false) {
            if ($info[0] != 'documento_tipo_id') {
                $file = ListaNegra::create([
                    'documento_tipo_id' => $info[0],
                    'numero_doc' => $info[1],
                    'nombre' => $info[2],
                    'apellido' => $info[3],
                    'fecha' => $info[4],
                    'observacion' => $info[5],
                    'lista_negra_motivo_id' => $info[6],
                    'created_at' => Carbon::now(),
                    'updated_at' => NULL
                ]);
            }
        }
        fclose($archivo);
    }
}
