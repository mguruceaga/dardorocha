<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $administrador = Role::create(
            [
                'id' => 1,
                'name' => config('tools.constants.roles.administrador'),
                'guard_name' => 'web'
            ]);
        $empleado = Role::create(
            [
                'id' => 2,
                'name' => config('tools.constants.roles.empleado'),
                'guard_name' => 'web'
            ]);
        $comercial = Role::create(
            [
                'id' => 3,
                'name' => config('tools.constants.roles.comercial'),
                'guard_name' => 'web'
            ]);
        $tesorero = Role::create(
            [
                'id' => 4,
                'name' => config('tools.constants.roles.tesorero'),
                'guard_name' => 'web'
            ]);

        $user = User::create([
            'name' => 'Administrador',
            'email' => 'administrador@gmail.com',
            'email_verified_at' => NULL,
            'password' => bcrypt(12121212),
            'two_factor_secret' => NULL,
            'two_factor_recovery_codes' => NULL,
            'remember_token' => NULL,
            'current_team_id' => '1',
            'profile_photo_path' => NULL,
            'created_at' =>  Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $user->assignRole(config('tools.constants.roles.administrador'));

        $user = User::create([
            'name' => 'Operador 1',
            'email' => 'operador1@gmail.com',
            'email_verified_at' => NULL,
            'password' => bcrypt(12121212),
            'two_factor_secret' => NULL,
            'two_factor_recovery_codes' => NULL,
            'remember_token' => NULL,
            'current_team_id' => '2',
            'profile_photo_path' => NULL,
            'recibe_precargas' => true,
            'created_at' =>  Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $user->assignRole(config('tools.constants.roles.empleado'));

        $user = User::create([
            'name' => 'Operador 2',
            'email' => 'operador2@gmail.com',
            'email_verified_at' => NULL,
            'password' => bcrypt(12121212),
            'two_factor_secret' => NULL,
            'two_factor_recovery_codes' => NULL,
            'remember_token' => NULL,
            'current_team_id' => '2',
            'profile_photo_path' => NULL,
            'recibe_precargas' => true,
            'created_at' =>  Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $user->assignRole(config('tools.constants.roles.empleado'));

        $user = User::create([
            'name' => 'Comercial 1',
            'email' => 'comercial1@gmail.com',
            'email_verified_at' => NULL,
            'password' => bcrypt(12121212),
            'two_factor_secret' => NULL,
            'two_factor_recovery_codes' => NULL,
            'remember_token' => NULL,
            'current_team_id' => '3',
            'profile_photo_path' => NULL,
            'recibe_precargas' => false,
            'created_at' =>  Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $user->assignRole(config('tools.constants.roles.comercial'));

        $user = User::create([
            'name' => 'Comercial 2',
            'email' => 'comercial2@gmail.com',
            'email_verified_at' => NULL,
            'password' => bcrypt(12121212),
            'two_factor_secret' => NULL,
            'two_factor_recovery_codes' => NULL,
            'remember_token' => NULL,
            'current_team_id' => '3',
            'profile_photo_path' => NULL,
            'recibe_precargas' => false,
            'created_at' =>  Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $user->assignRole(config('tools.constants.roles.comercial'));

        $user = User::create([
            'name' => 'Tesorero',
            'email' => 'tesorero@gmail.com',
            'email_verified_at' => NULL,
            'password' => bcrypt(12121212),
            'two_factor_secret' => NULL,
            'two_factor_recovery_codes' => NULL,
            'remember_token' => NULL,
            'current_team_id' => '3',
            'profile_photo_path' => NULL,
            'recibe_precargas' => false,
            'created_at' =>  Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $user->assignRole(config('tools.constants.roles.tesorero'));

    }
}
