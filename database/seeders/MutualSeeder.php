<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Mutual;

class MutualSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $archivo = fopen(public_path('/resources/mutuales.csv'), 'r');
        while (($info = fgetcsv($archivo)) !== false) {
            if ($info[0] != 'nombre') {
                $mutual = Mutual::create([
                    'nombre' => $info[0],
                    'abreviatura' => $info[1],
                    'deleted_at' => NULL,
                    'updated_at' => NULL,
                    'created_at' => Carbon::now(),
                ]);
            }
        }
        fclose($archivo);

        /*DB::table('mutuales')->insert([
            [   'nombre' => 'Asociación Mutual Dardo Rocha',
                'abreviatura' => 'AMDR',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Sistema Integral Mutual Argentino',
                'abreviatura' => 'SIMA',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Sin nombre',
                'abreviatura' => 'ADM',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Asociación Mutual de Empleados de Ministerios y Municipales de la Provincia de Bs As',
                'abreviatura' => 'AMUPROBA',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Cooperativo de Crédito y consumo',
                'abreviatura' => 'DAP',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
        ]);*/
    }
}
