<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GeneroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('generos')->insert([
            [   'nombre' => 'Femenino',
                'abreviatura' => 'F',
                'codigo' => NULL,
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Masculino',
                'abreviatura' => 'M',
                'codigo' => NULL,
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            /*[   'nombre' => 'Prefiero no mencionarlo',
                'abreviatura' => 'PNM',
                'codigo' => NULL,
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],*/
        ]);
    }
}
