<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DebitoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('debitos')->insert([
            [
                'nombre' => 'CBU',
            ],
            [
                'nombre' => 'Haberes',
            ],
        ]);
    }
}
