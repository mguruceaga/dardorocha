<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paises')->insert(
            [
                ['nombre' => 'Argentina'],
                ['nombre' => 'Brasil'],
                ['nombre' => 'Bolivia'],
                ['nombre' => 'Chile'],
                ['nombre' => 'Colombia'],
                ['nombre' => 'Ecuador'],
                ['nombre' => 'Guyana'],
                ['nombre' => 'Guayana Francesa'],
                ['nombre' => 'Paraguay'],
                ['nombre' => 'Perú'],
                ['nombre' => 'Suriname'],
                ['nombre' => 'Uruguay'],
                ['nombre' => 'Venezuela'],
        ]);

    }
}
