<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Archivo;
use Carbon\Carbon;

class ArchivoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $archivo = fopen(public_path('/resources/archivos.csv'), 'r');
        while (($info = fgetcsv($archivo)) !== false) {
            if ($info[0] != 'nombre') {
                $file = Archivo::create([
                    'nombre' => $info[0],
                    'path' => $info[1],
                    'formulario_id' => $info[2],
                    'fondista_id' => ($info[3] != '') ? $info[3] : null,
                    'reparticion_id' => ($info[4] != '') ? $info[4] : null,
                    'activo' => false,
                    'deleted_at' => NULL,
                    'updated_at' => NULL,
                    'created_at' => Carbon::now(),
                ]);
            }
        }
        fclose($archivo);
    }
}
