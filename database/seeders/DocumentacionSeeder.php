<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DocumentacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('documentaciones')->insert([
            [
                'nombre' => 'Resumen de Movimientos Bancarios',
                'abreviatura' => 'RMB',
                'slug' => 'resumen_de_movimientos_bancarios',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [
                'nombre' => 'Recibo de Sueldo',
                'abreviatura' => 'REC',
                'slug' => 'recibo_de_sueldo',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [
                'nombre' => 'Documento de Identidad',
                'abreviatura' => 'DNI',
                'slug' => 'documento_de_identidad',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [
                'nombre' => 'Servicio',
                'abreviatura' => 'SER',
                'slug' => 'servicio',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [
                'nombre' => 'Constancia de CBU',
                'abreviatura' => 'CBU',
                'slug' => 'constancia_de_cbu',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [
                'nombre' => 'Certificado de Afectación',
                'abreviatura' => 'CAF',
                'slug' => 'certificado_de_afectacion',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],

        ]);
    }
}
