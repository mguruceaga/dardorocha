<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teams')->insert([
            [   'user_id' => '1',
                'name' => 'Administrador\'s Team',
                'personal_team' => '1',
                'created_at' =>  Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [   'user_id' => '2',
                'name' => 'Empleado\'s Team',
                'personal_team' => '1',
                'created_at' =>  Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [   'user_id' => '3',
                'name' => 'Comercial\'s Team',
                'personal_team' => '1',
                'created_at' =>  Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
    }
}
