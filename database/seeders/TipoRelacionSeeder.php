<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoRelacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_relaciones')->insert([
            [
                'id' => 1,
                'nombre' => 'Obligatorio',
                'slug' => 'obligatorio',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [
                'id' => 2,
                'nombre' => 'Opcional',
                'slug' => 'opcional',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [
                'id' => 3,
                'nombre' => 'Dependiente',
                'slug' => 'dependiente',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
        ]);

    }
}
