<?php

namespace Database\Seeders;

use App\Models\ReparticionZonaGrupo;
use Illuminate\Database\Seeder;
use App\Models\Reparticion;
use Carbon\Carbon;

class ReparticionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $archivo = fopen(public_path('/resources/reparticiones.csv'), 'r');
        while (($info = fgetcsv($archivo)) !== false) {
            if ($info[0] != 'nombre') {
                $reparticion = Reparticion::create([
                    'nombre' => $info[0],
                    'cuit' => $info[1],
                    'zona_id' => $info[2],
                    'deleted_at' => NULL,
                    'updated_at' => NULL,
                    'created_at' => Carbon::now(),
                ]);/*
                switch ($info[1]) {
                    case '30-66190374-7':
                    case '30-62069801-2':
                    case '33-63317800-9':
                        ReparticionZonaGrupo::create([
                            'alias' => $info[0] . '(JyP)',
                            'reparticion_id' => $reparticion->id,
                            'zona_id' => 1,
                            'grupo_pago_id' => 2,
                            'deleted_at' => NULL,
                            'updated_at' => NULL,
                            'created_at' => Carbon::now(),
                        ]);
                        break;
                }
                ReparticionZonaGrupo::create([
                    'alias' => $info[0],
                    'reparticion_id' => $reparticion->id,
                    'zona_id' => 1,
                    'grupo_pago_id' => 1,
                    'deleted_at' => NULL,
                    'updated_at' => NULL,
                    'created_at' => Carbon::now(),
                ]);*/

            }
        }
        fclose($archivo);
    }
}