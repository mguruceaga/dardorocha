<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DocumentoTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('documento_tipos')->insert([
            [   'nombre' => 'Documento Nacional de Identidad',
                'abreviatura' => 'DNI',
                'codigo' => 'DNI',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            /*[
                'nombre' => 'Documento Nacional Único',
                'abreviatura' => 'DNU',
                'codigo' => 'DNU',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],*/
            [
                'nombre' => 'Cédula de Identidad',
                'abreviatura' => 'CI',
                'codigo' => 'CI',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [
                'nombre' => 'Libreta de Enrolamiento',
                'abreviatura' => 'LE',
                'codigo' => 'LE',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [
                'nombre' => 'Libreta Cívica',
                'abreviatura' => 'LC',
                'codigo' => 'LC',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [
                'nombre' => 'Pasaporte',
                'abreviatura' => 'PAS',
                'codigo' => 'PAS',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],

        ]);
    }
}
