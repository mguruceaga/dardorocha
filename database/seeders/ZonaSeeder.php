<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ZonaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('zonas')->insert([
            [
                'nombre' => 'Buenos Aires',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],

        ]);
        
        DB::table('zonas')->insert([
            [
                'nombre' => 'Nación',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],

        ]);
        
        DB::table('zonas')->insert([
            [
                'nombre' => 'San Luis',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],

        ]);
        
        DB::table('zonas')->insert([
            [
                'nombre' => 'Misiones',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],

        ]);
        
        DB::table('zonas')->insert([
            [
                'nombre' => 'Salta',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],

        ]);
    }
}
