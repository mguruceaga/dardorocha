<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ZonaPorGrupoPagoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('zona_por_grupo_pagos')->insert([
            [
                'grupo_pago_id' => '1',
                'zona_id' => '1',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],

        ]);
        
        DB::table('zona_por_grupo_pagos')->insert([
            [
                'grupo_pago_id' => '1',
                'zona_id' => '2',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],

        ]);

        DB::table('zona_por_grupo_pagos')->insert([
            [
                'grupo_pago_id' => '2',
                'zona_id' => '1',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],

        ]);
        
        DB::table('zona_por_grupo_pagos')->insert([
            [
                'grupo_pago_id' => '2',
                'zona_id' => '2',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],

        ]);
    }
}
