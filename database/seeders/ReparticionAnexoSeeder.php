<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReparticionAnexoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */	
    
    public function run()
    {
        DB::table('reparticion_anexos')->insert([
            [   'nombre' => 'Anexo 1',
                'reparticion_id' => '1',
                'archivo' => 'anexo1.docx',
                'hasta' => NULL,
                'desde' => NULL,
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Anexo 2',
                'reparticion_id' => '1',
                'archivo' => 'anex2.docx',
                'hasta' => NULL,
                'desde' => NULL,
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Anexo 3',
                'reparticion_id' => '2',
                'archivo' => 'anexo11.docx',
                'hasta' => NULL,
                'desde' => NULL,
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Anexo 4',
                'reparticion_id' => '3',
                'archivo' => '',
                'hasta' => NULL,
                'desde' => NULL,
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
        ]);
    }
}
