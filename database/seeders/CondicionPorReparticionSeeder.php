<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CondicionPorReparticion;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CondicionPorReparticionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            $archivo = fopen(public_path('/resources/condicionPorReparticiones.csv'), 'r');
            while (($info = fgetcsv($archivo)) !== false) {
                if ($info[0] != 'condicion_id') {
                    CondicionPorReparticion::create([
                        'condicion_id' => $info[0],
                        'reparticion_id' => $info[1],
                        'zona_id' => $info[2],
                        'grupo_pago_id' => $info[3],
                        'deleted_at' => NULL,
                        'updated_at' => NULL,
                        'created_at' => Carbon::now(),
                    ]);
                }
            }
            fclose($archivo);
        }
    }
}
