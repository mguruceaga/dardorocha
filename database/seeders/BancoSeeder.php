<?php

namespace Database\Seeders;

use App\Models\Banco;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class BancoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('bancos')->truncate();

        {
            $archivo = fopen(public_path('/resources/bancosBCRA.csv'), 'r');
            while (($info = fgetcsv($archivo)) !== false) {
                if ($info[0] != 'codigo') {
                    Banco::create([
                        'codigo' => $info[0],
                        'nombre' => $info[1],
                        'deleted_at' => NULL,
                        'updated_at' => NULL,
                        'created_at' => Carbon::now(),
                    ]);
                }
            }
            fclose($archivo);
        }
    }
}
