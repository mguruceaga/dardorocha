<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->truncateTables([
            // 'lineas_credito',
            'debitos',
            'archivos',
            'condicion_por_reparticiones',
            'condiciones',
            'reparticion_zona_grupos',
            'zona_por_grupo_pagos',
            'model_has_permissions',
            'permissions',
            'model_has_roles',
            'roles',
            'reparticion_restricciones',
            'documentacion_adjuntas',
            'mensajes',
            'precarga_usuario',
            'precarga_estado',
            'precargas',
            'motivos_rechazos',
            'mutual_linea_formularios',
            'tipo_relaciones',
            'mutual_linea_grillas',
            'mutual_lineas',
            'grillas',
            'mutuales',
            'grupos_pagos',
            'reparticiones',
            'documentaciones',
            'precarga_estados',
            'prestamo_tipos',
            'codigos_postales',
            'bancos',
            'zonas',
            'contratacion_tipos',
            'paises',
            'generos',
            'documento_tipos',
            'sectores',
            'failed_jobs',
            'users'
        ]);

        $this->call(FondistasSeeder::class);
        $this->call(BancoSeeder::class);
        $this->call(CodigoPostalSeeder::class);
        $this->call(ContratacionTipoSeeder::class);
        $this->call(DocumentacionSeeder::class);
        $this->call(DocumentoTipoSeeder::class);
        $this->call(GeneroSeeder::class);
        $this->call(PaisSeeder::class);
        $this->call(GrillaSeeder::class);
        $this->call(PrecargaEstadoSeeder::class);
        $this->call(PrestamoTipoSeeder::class);
        $this->call(SectorSeeder::class);
        $this->call(GrupoPagoSeeder::class);
        $this->call(ZonaSeeder::class);
        $this->call(ZonaPorGrupoPagoSeeder::class);
        $this->call(ReparticionSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(TeamSeeder::class);
        $this->call(MotivosRechazoSeeder::class);
        $this->call(MutualSeeder::class);
        $this->call(MutualLineaSeeder::class);
        $this->call(CondicionesSeeder::class);
        $this->call(DebitoSeeder::class);
        // $this->call(LineaCreditoSeeder::class);
        $this->call(CondicionPorReparticionSeeder::class);
        $this->call(ReparticionZonaGrupoSeeder::class);
        $this->call(MutualLineaGrillaSeeder::class);
        $this->call(ReparticionAnexoSeeder::class);
        $this->call(FormularioSeeder::class);
        $this->call(ArchivoSeeder::class);
        $this->call(TipoRelacionSeeder::class);
        $this->call(MutualLineaFormularioSeeder::class);
        $this->call(ListaNegraMotivoSeeder::class);
        $this->call(ListaNegraSeeder::class);
    }

    protected function truncateTables(array $tables)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        foreach ($tables as $table) {
            DB::table($table)->truncate();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }

}
