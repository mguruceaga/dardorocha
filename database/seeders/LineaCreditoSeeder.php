<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LineaCreditoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lineas_credito')->insert([
            [
                'nombre' => 'Línea 1',
            ],
            [
                'nombre' => 'Línea 2',
            ],
        ]);
    }
}
