<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\MutualLineaGrilla;

class MutualLineaGrillaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $archivo = fopen(public_path('/resources/mutual_linea_grillas.csv'), 'r');
        while (($info = fgetcsv($archivo)) !== false) {
            if ($info[0] != 'mutual_linea_id') {
                $mutualLineaGrilla = MutualLineaGrilla::create([
                    'mutual_linea_id' => $info[0],
                    'grilla_id' => $info[1],
                    'desde' => NULL,
                    'hasta' => NULL,
                    'deleted_at' => NULL,
                    'updated_at' => NULL,
                    'created_at' => Carbon::now(),
                ]);
            }
        }
        fclose($archivo);

        /*DB::table('mutual_linea_grillas')->insert([
            [   'mutual_linea_id' => '1',
                'grilla_id' => '1',
                'desde' => NULL,
                'hasta' => NULL,
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'mutual_linea_id' => '1',
                'grilla_id' => '9',
                'desde' => NULL,
                'hasta' => NULL,
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'mutual_linea_id' => '17',
                'grilla_id' => '1',
                'desde' => NULL,
                'hasta' => NULL,
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'mutual_linea_id' => '17',
                'grilla_id' => '10',
                'desde' => NULL,
                'hasta' => NULL,
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'mutual_linea_id' => '17',
                'grilla_id' => '11',
                'desde' => NULL,
                'hasta' => NULL,
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
        ]);*/
    }
}
