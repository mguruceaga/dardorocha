<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Formulario;
use Carbon\Carbon;

class FormularioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $archivo = fopen(public_path('/resources/formularios.csv'), 'r');
        while (($info = fgetcsv($archivo)) !== false) {
            if ($info[0] != 'nombre') {
                $formulario = Formulario::create([
                    'nombre' => $info[0],
                    'opcional' => $info[1],
                    'extra' => $info[2],
                    'deleted_at' => NULL,
                    'updated_at' => NULL,
                    'created_at' => Carbon::now(),
                ]);
            }
        }
        fclose($archivo);
    }
}
