<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MutualLineaFormulario;
use Carbon\Carbon;

class MutualLineaFormularioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $archivo = fopen(public_path('/resources/mutual_linea_formularios.csv'), 'r');
        while (($info = fgetcsv($archivo)) !== false) {
            if ($info[0] != 'mutual_linea_id') {
                $mutualLineaFormulario = MutualLineaFormulario::create([
                    'mutual_linea_id' => $info[0],
                    'formulario_id' => $info[1],
                    'tipo_relacion_id' => $info[2],
                    'original_bloqueado' => $info[3],
                    'copias_identicas' => $info[4],
                    'copias_blanco' => $info[5],
                    'deleted_at' => NULL,
                    'updated_at' => NULL,
                    'created_at' => Carbon::now(),
                ]);
            }
        }
        fclose($archivo);
    }
}
