<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Mutual;

class FondistasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('fondistas')->insert([
            [
                'nombre' => 'DAP',
                'abreviatura' => 'DAP',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'nombre' => 'Videgar',
                'abreviatura' => 'Videgar',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
        ]);
    }
}
