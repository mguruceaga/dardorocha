<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContratacionTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contratacion_tipos')->insert([
            [   'descripcion' => 'Planta Permanente',
                'slug' => 'planta_permanente',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [
                'descripcion' => 'Planta Permanente sin Estabilidad',
                'slug' => 'planta_permanente_sin_estabilidad',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [
                'descripcion' => 'Planta Temporaria',
                'slug' => 'planta_temporaria',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [
                'descripcion' => 'Becarios',
                'slug' => 'becarios',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [
                'descripcion' => 'Contrato de Locación de Servicio',
                'slug' => 'contrato_de_locacion_de_servicio',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],

        ]);
    }
}
