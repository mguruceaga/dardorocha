<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GrupoPagoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('grupos_pagos')->insert([
            [   'descripcion' => 'Activo',
                'slug' => 'activo',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ],
            [   'descripcion' => 'Pasivo',
                'slug' => 'pasivo',
                'deleted_at' => NULL,
                'updated_at' => NULL,
                'created_at' =>  Carbon::now(),
            ]
        ]);
    }
}
